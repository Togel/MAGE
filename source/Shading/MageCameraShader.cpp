/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageCameraShader.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageCameraShader::MageCameraShader(GLuint iCameraUniformBufferID)
  : m_iCameraUniformBufferID(iCameraUniformBufferID)
{
}


MageCameraShader::~MageCameraShader()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

ShaderClass MageCameraShader::GetClass()
{
  return ShaderClass::CameraShader;
}


void MageCameraShader::MapUniforms()
{
  // Clear outdated Results.
  m_mapUniforms.clear();

  // Find Uniforms of the Shader Program.
  int iNumOfUniforms, iMaxNameLength;

  glGetProgramiv(m_iProgram, GL_ACTIVE_UNIFORMS, &iNumOfUniforms);
  glGetProgramiv(m_iProgram, GL_ACTIVE_UNIFORM_MAX_LENGTH, &iMaxNameLength);

  if (iNumOfUniforms == 0 || iMaxNameLength == 0)
    return;

  // Put Uniforms in Map.
  GLchar* sName = new GLchar[iMaxNameLength];
  GLsizei iLength;
  GLint   iSize;
  GLenum  iType;

  for (int i = 0; i < iNumOfUniforms; ++i)
  {
    glGetActiveUniform(m_iProgram, static_cast<GLuint>(i), iMaxNameLength, &iLength, &iSize, &iType, sName);

    std::string sNameClean = "";
    std::string::size_type iArrayPos = std::string(sName).rfind('[');

    if (iArrayPos != std::string::npos)
      sNameClean = std::string(sName).substr(0, iArrayPos);
    else
      sNameClean = std::string(sName);

    const GLint iLoc = glGetUniformLocation(m_iProgram, sNameClean.c_str());
    m_mapUniforms[sNameClean] = iLoc;
  }

  delete[] sName;

  // Initialize Uniform Buffer for the Camera (View and Projection Matrix)
  GLuint iCameraBufferIndex = glGetUniformBlockIndex(m_iProgram, "Camera");
  if (iCameraBufferIndex != -1)
  {
    const GLchar* aUniformNames[3] =
    {
      "mat4View",
      "mat4Projection",
      "mat4InvTransView"
    };

    int iUniformBlockDataSize;
    glGetActiveUniformBlockiv(m_iProgram, iCameraBufferIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &iUniformBlockDataSize);

    glBindBuffer(GL_UNIFORM_BUFFER, m_iCameraUniformBufferID);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, m_iCameraUniformBufferID);
    glUniformBlockBinding(m_iProgram, iCameraBufferIndex, 0);

    GLuint aIndex[3];
    glGetUniformIndices(m_iProgram, 3, aUniformNames, aIndex);
    m_iMatViewIndex = aIndex[0];
    m_iMatProjectionIndex = aIndex[1];
    m_iMatInvTransViewIndex = aIndex[2];

    GLint aOffset[3];
    glGetActiveUniformsiv(m_iProgram, 3, aIndex, GL_UNIFORM_OFFSET, aOffset);
    m_iMatViewOffset = aOffset[0];
    m_iMatProjectionOffset = aOffset[1];
    m_iMatIntTransViewOffset = aOffset[2];
  }

  glGetError();
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/