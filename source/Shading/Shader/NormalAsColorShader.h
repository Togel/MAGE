#pragma once
#include <string>


static const std::string NORMAL_AS_COLOR_CAM_VS = R"(#version 330 core

// [Input Data].
layout(location = 0) in vec3 v3Vertex;
layout(location = 1) in vec3 v3Normal;
layout(location = 2) in vec3 v3Color;

// [Output Data].
out vec3 v3FragColor;

// [Uniform Data].
uniform mat4 mat4Model;
uniform mat4 mat4InvTransModel;

layout (std140) uniform Camera
{
  mat4 mat4View;
  mat4 mat4Projection;
  mat4 mat4InvTransView;
};

// [Main Program].
void main()
{
  // Output position of the vertex, in clip space : MVP * position
  gl_Position = mat4Projection * mat4View * mat4Model * vec4(v3Vertex, 1);

  // Use the Normal as Color Value.
  v3FragColor = (v3Normal + vec3(1.0, 1.0, 1.0)) * 0.5;
})";


static const std::string NORMAL_AS_COLOR_CAM_FS = R"(#version 330 core

// [Input Data].
in vec3 v3FragColor;

// [Output Data].
out vec3 v3Color;

// [Main Program].
void main()
{
  // Simply use the Color provided by the Vertex Shader.
  v3Color = v3FragColor;
})";