#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../Core/Timer/MageTimer.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageScene
{
public:
  /*
  * Constructor.
  */
  MageScene();


  /*
  * Destructor.
  */
  ~MageScene();


  /*
  * Traverse the Scene Data Structure and Draws the Scene.
  * Needs to be implemented in Sub-Class.
  */
  virtual void Draw() = 0;


  /*
  * Traverse the Scene Data Structure and executes Updates.
  * Needs to be implemented in Sub-Class.
  * @param 'pTimer' Global Timer used by the Geometry Thread.
  */
  virtual void Update(MageTimer* pTimer) = 0;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/