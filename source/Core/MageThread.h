#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageCallback.h"

#include "Timer/MageTimerWithUpdate.h"

#include "../SceneRepresentation/MageScene.h"

#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageThread
{

public:
  /*
  * Constructor.
  */
  MageThread();


  /*
  * Destructor.
  */
  virtual ~MageThread();


  /*
  * Adds another Callback to the Thread.
  * The Callback is called in each Loop of the Thread.
  */
  unsigned int AddCallback(MageCallback* pCallback);


  /*
  * Removes a Callback from the System.
  * @param 'iID' the unique Identification Number (ID) of the Callback.
  * @return Boolean representing whether the Deletion has been successful.
  */
  bool DeleteCallback(unsigned int iID);


  /*
  * Returns the Timer of the Thread.
  * @return Pointer to the Timer.
  */
  MageTimer* GetTimer();


  /*
  * Checks whether the Thread has a Callback with a specific ID.
  * @param 'iID' ID of the Callback.
  * @return 'true' if the Thread administrates the Callback.
  */
  bool IsOwnerOfCallback(unsigned int iID);


  /*
  * Runs the Thread.
  * Behavior needs to be defined in Sub-Class.
  */
  virtual void _Run() = 0;


  /*
  * Switches to a new Scene.
  * @param 'pScene' Pointer to the new Scene.
  */
  void SwitchScene(MageScene* pScene);


  /*
  * Callback Function which updates the Timer.
  * Is automatically called once per Loop Run.
  */
  void _UpdateTimer();


protected:
  // Each Thread has its own Timer.
  // Sharing the same Timer would lead to Bugs as the Threads are not synchronized.
  MageTimerWithUpdate* m_pThreadTimer = nullptr;

  // Stores a Pointer to the active Scene.
  MageScene* m_pScene = nullptr; 
     
  // Stores all Callbacks which are performed each Loop.
  std::map<unsigned int, MageCallback*> m_mapCallbacks; 
  typedef std::map<unsigned int, MageCallback*>::iterator it_CB;
  // Stores the next Callback ID.
  unsigned int m_iNextCallbackID = 1;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/