#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <iostream>
#include <sstream>
#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/
#define inf inf()
#define war war()
#define err err()
#define crs crs()

/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

// (M)AGE (I)nput and (O)utput.
namespace mio
{
  // Contains internal Functions and Structures Information.
  namespace internals
  {
    /*
    * Stores Configuration Information in Form of Key and Value.
    */
    static std::map<std::string, std::string> m_mapConfig;


    /*
    * Struct storing Information about the Color Value on Windows and Linux.
    */
    struct ConsoleColor
    {
      ConsoleColor(int iWinColor, std::string sLinColor)
      {
        m_iWinColor = iWinColor;
        m_sLinColor = sLinColor;
      }

      int m_iWinColor = 0;
      std::string m_sLinColor = "";
    };


    /*
    * Color Definitions.
    */
    static ConsoleColor ColorBlackBlue   = ConsoleColor(9, "\033[34m");
    static ConsoleColor ColorBlackWhite  = ConsoleColor(07, "\033[0m");
    static ConsoleColor ColorBlackGreen  = ConsoleColor(10, "\033[32m");
    static ConsoleColor ColorBlackRed    = ConsoleColor(12, "\033[31m");
    static ConsoleColor ColorBlackYellow = ConsoleColor(14, "\033[33m");
    static ConsoleColor ColorRedYellow   = ConsoleColor(206, "\033[33;31m");
    static ConsoleColor ColorYellowRed   = ConsoleColor(236, "\033[31;33m");


    /*
    * Set the Console Color (Background and Foreground) to a given value.
    * @param 'sColor' Struct defining the Color for Windows and Linux.
    */
    void SetConsoleColor(ConsoleColor sColor);


    /*
    * Logs a Message in the specified Log-File.
    * @param 'sPath' defines the Path to the Log-File.
    * @param 'sMessage' defines the Log-Message.
    */
    void LogMessage(char* sPath, char* sMessage);


    /*
    * Defining a new Output Type with a Green Font.
    */
    struct IOInfo
    {
      template<typename T>
      IOInfo& operator << (T&& x)
      {
        SetConsoleColor(ColorBlackGreen);
        std::cout << x;
        SetConsoleColor(ColorBlackWhite);

        return *this;
      }
    };


    /*
    * Defining a new Output Type with a Yellow Font.
    */
    struct IOWarning
    {
      template<typename T>
      IOWarning& operator << (T&& x)
      {
        SetConsoleColor(ColorBlackYellow);
        std::cout << x;
        SetConsoleColor(ColorBlackWhite);

        return *this;
      }
    };


    /*
    * Defining a new Output Type with a Red Font and a Log-File.
    */
    struct IOError
    {
      template<typename T>
      IOError& operator << (T&& x)
      {
        SetConsoleColor(ColorBlackRed);
        std::cout << x;
        SetConsoleColor(ColorBlackWhite);

        return *this;
      };
    };


    /*
    * Defining a new Output Type with a Red Font, a Yellow Background and a Log-File.
    */
    struct IOCrash
    {
      template<typename T>
      IOCrash& operator << (T&& x)
      {
        SetConsoleColor(ColorYellowRed);
        std::cout << x;
        SetConsoleColor(ColorBlackWhite);

        return *this;
      };
    };
  };

  
  /*
  * Adds another Configuration to the System, which can later be accessed.
  * @param 'sKey' defines the Key via which the Value can be read later.
  * @param 'sValue' defines the actual Value of the Configuration.
  * @return Boolean representing whether the Configuration has been added successfully.
  */
  bool AddConfiguration(std::string sKey, std::string sValue);


  /*
  * Gets the Value of a specific Configuration.
  * @param 'sKey' specifies the Key of the Configuration.
  * @param 'sValue' Value gets stored in this Reference.
  * @return 'true' if the Key exists.
  */
  bool GetConfiguration(std::string sKey, std::string& sValue);


  /*
  * Loads a Configuration File and stores the Contents.
  * @param 'sPath' defines the Path to the Configuration File.
  * @return Boolean representing whether the File has been loaded successfully.
  */
  bool LoadConfigFile(std::string sPath);


  /*
  * Print an Information Message.
  */
  internals::IOInfo inf;


  /*
  * Print a Warning Message.
  */
  internals::IOWarning war;


  /*
  * Print an Error Message.
  * In addition, the Error is stored in a Log-File.
  */
  internals::IOError err;


  /*
  * Print a Crash Message.
  * In addition, the Crash is stored in a Log-File.
  */
  internals::IOCrash crs;


  /*
  * Prints an End of Line.
  */
  static const std::string endl = "\n";
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/