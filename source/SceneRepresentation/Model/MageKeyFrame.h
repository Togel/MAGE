#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTransformation.h"

#include <string>
#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

struct MageKeyframe
{
  /*
  * Constructor.
  */
  MageKeyframe(double dTimeStamp, std::map<std::string, MageTransformation*> mapPose)
  {
    m_dTimeStamp = dTimeStamp;
    m_mapPose    = mapPose;
  }
  
  // The Time in Seconds when the Keyframe occurs.
  double m_dTimeStamp;

  // Map storing for each Joint, referenced by Name, its Transformation at the current Keyframe.
  std::map<std::string, MageTransformation*> m_mapPose;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/