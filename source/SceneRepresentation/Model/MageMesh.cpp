/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageMesh.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageMesh::MageMesh(std::vector<glm::vec3>& vecVertices, 
  std::vector<glm::vec3>& vecNormals, 
  std::vector<glm::vec3>& vecColors, 
  std::vector<glm::vec2>& vecUVs, 
  std::vector<unsigned int>& vecFaceIndices, 
  std::vector<glm::vec3> vecTangents,
  std::vector<glm::vec3> vecBiTangents,
  std::vector<glm::ivec4> vecJointIndices, 
  std::vector<glm::vec4> vecJointWeights)
  : m_vecVertices(vecVertices)
  , m_vecNormals(vecNormals)
  , m_vecColors(vecColors)
  , m_vecUVs(vecUVs)
  , m_vecFaceIndices(vecFaceIndices)
  , m_vecTangents(vecTangents)
  , m_vecBiTangents(vecBiTangents)
  , m_vecJointIndices(vecJointIndices)
  , m_vecJointWeights(vecJointWeights)
{
  if (m_vecFaceIndices.size() > 0)
    m_iSize = static_cast<GLsizei>(m_vecFaceIndices.size());
  else
    m_iSize = static_cast<GLsizei>(m_vecVertices.size());
}


MageMesh::~MageMesh()
{
  m_vecVertices.clear();
  m_vecNormals.clear();
  m_vecColors.clear();
  m_vecUVs.clear();
  m_vecFaceIndices.clear();
  m_vecTangents.clear();
  m_vecBiTangents.clear();
  m_vecJointIndices.clear();
  m_vecJointWeights.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

const std::vector<glm::vec3>& MageMesh::GetBiTangents()
{
  return m_vecBiTangents;
}


const std::vector<glm::vec3>& MageMesh::GetColors()
{
  return m_vecColors;
}


void MageMesh::GetData(std::vector<glm::vec3>& vecVertices,
  std::vector<glm::vec3>& vecNormals,
  std::vector<glm::vec3>& vecColors,
  std::vector<glm::vec2>& vecUVs,
  std::vector<unsigned int>& vecFaceIndices,
  std::vector<glm::vec3>& vecTangents,
  std::vector<glm::vec3>& vecBiTangents,
  std::vector<glm::ivec4>& vecJointIndices,
  std::vector<glm::vec4>& vecJointWeights)
{
  vecVertices     = m_vecVertices;
  vecNormals      = m_vecNormals;
  vecColors       = m_vecColors;
  vecUVs          = m_vecUVs;
  vecFaceIndices  = m_vecFaceIndices;
  vecTangents     = m_vecTangents;
  vecBiTangents   = m_vecBiTangents;
  vecJointIndices = m_vecJointIndices;
  vecJointWeights = m_vecJointWeights;
}


const std::vector<unsigned int>& MageMesh::GetFaceIndices()
{
  return m_vecFaceIndices;
}


const std::vector<glm::ivec4>& MageMesh::GetJointIndices()
{
  return m_vecJointIndices;
}


const std::vector<glm::vec4>& MageMesh::GetJointWeights()
{
  return m_vecJointWeights;
}


GLenum MageMesh::GetMode()
{
  return m_eMode;
}


const std::vector<glm::vec3>& MageMesh::GetNormals()
{
  return m_vecNormals;
}


GLenum MageMesh::GetPolygonMode()
{
  return m_eMode;
}


GLsizei MageMesh::GetSize()
{
  return m_iSize;
}


const std::vector<glm::vec3>& MageMesh::GetTangents()
{
  return m_vecTangents;
}


const std::vector<glm::vec2>& MageMesh::GetUVs()
{
  return m_vecUVs;
}


const std::vector<glm::vec3>& MageMesh::GetVertices()
{
  return m_vecVertices;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/