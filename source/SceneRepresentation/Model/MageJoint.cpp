/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageJoint.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageJoint::MageJoint(int iIndex, std::string sName, glm::mat4 m4LocalBindTransform)
  : m_iIndex(iIndex)
  , m_sName(sName)
  , m_m4LocalBindTransform(m4LocalBindTransform)
{
}


MageJoint::~MageJoint()
{
  m_vecChildren.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageJoint::AddChild(MageJoint* pChild)
{
  m_vecChildren.push_back(pChild);
}


void MageJoint::CalculateInverseBindTransform(glm::mat4& m4ParentTransformation)
{
  // Calculates the combination of the local Joint Transformation with its parent local Transformations.
  glm::mat4 m4CombinedBindTransform = m4ParentTransformation * m_m4LocalBindTransform;
  
  // Calculate the Inverse of the combined Transformation.
  m_m4InverseBindTransform = glm::inverse(m4CombinedBindTransform);

  // Do the same for the whole Hierarchy (all Children).
  for(MageJoint* pChild : m_vecChildren)
  {
    pChild->CalculateInverseBindTransform(m4CombinedBindTransform);
  }
}


std::vector<MageJoint*> MageJoint::GetChildren()
{
  return m_vecChildren;
}


glm::mat4 MageJoint::GetCurrentTransformation()
{
  return m_m4CurrentTransform;
}


int MageJoint::GetID()
{
  return m_iIndex;
}


glm::mat4 MageJoint::GetInverseBindTransform()
{
  return m_m4InverseBindTransform;
}


std::string MageJoint::GetName()
{
  return m_sName;
}


void MageJoint::GetSortedJoints(std::map<unsigned int, MageJoint*>& mapSkeleton)
{
  // Include the Joint itself in the Map.
  mapSkeleton[m_iIndex] = this;
  
  // Include Children.
  for (MageJoint* pChild : m_vecChildren)
  {
    pChild->GetSortedJoints(mapSkeleton);
  }
}


void MageJoint::ResetToBindingTransformation()
{
  SetCurrentTransformation(glm::mat4(1.0f));

  for (MageJoint* pChild : m_vecChildren)
  {
    pChild->ResetToBindingTransformation();
  }
}


void MageJoint::SetCurrentTransformation(glm::mat4& m4CurrentTransform)
{
  m_m4CurrentTransform = m4CurrentTransform;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/