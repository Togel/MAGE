#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageVBO.h"
#include "MageMesh.h"

#include "GL/glew.h"

#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum VaoState
{
  Free,
  Uploading,
  Uploaded
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageVAO
{
public:
  /*
  * Constructor.
  */
  MageVAO(MageMesh* pMesh);


  /*
  * Destructor.
  */
  virtual ~MageVAO();


  /*
  * Draws the Mesh.
  * @return true if the Drawing Process has been successful.
  */
  bool Draw();


  /*
  * Uploads the contained Vertex Buffer Objects.
  * Initializes the Vertex Attribute Pointer.
  * Vertex Attribute Pointer:
  * 0: Vertex
  * 1: Normal
  * 2: UV Coordinate or Color
  * 3: Joint Indices
  * 4: Joint Weights
  * 5: Tangent
  * 6: Bi-Tangent
  * @param 'eUsage' contains Performance Hints. See 'glBufferData' Documentation for more Information. Common Choices are GL_STATIC_DRAW and GL_DYNAMIC_DRAW.
  */
  bool Upload(GLenum eUsage);


private:
  // Stores the unique ID of the VAO.
  GLuint m_iBufferID = 0;

  // Stores the State of the VAO.
  VaoState m_eState = VaoState::Free;

  // Stores the VBO's contain in this VAO together with their Types.
  // See 'VboType' Definition for more Details.
  std::map<VboType, MageVBO*> m_mapVBOs;

  // Stores the Mesh this VAO represents.
  MageMesh* m_pMesh = nullptr;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/