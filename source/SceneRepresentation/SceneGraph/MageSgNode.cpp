/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgNode.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSgNode::MageSgNode()
{
}


MageSgNode::~MageSgNode()
{
  // Remove the Reference from the Parent Node to this Node.
  if (m_pParent)
    m_pParent->RemoveChild(this);

  // Remove all Children.
  // As the Child Nodes are removed during the Loop the Iterator would produce a Bug.
  // Hence, a Copy of the Vector is created.
  std::vector<MageSgNode*> vecChildren = m_vecChildren;

  for (MageSgNode* pChild : vecChildren)
  {
    delete pChild;
  }

  m_vecChildren.clear();
  vecChildren.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageSgNode::AddChild(MageSgNode* pChild)
{
  m_vecChildren.push_back(pChild);
  pChild->SetParent(this);
}


const std::vector<MageSgNode*>& MageSgNode::GetChildren()
{
  return m_vecChildren;
}


unsigned int MageSgNode::GetNumOfChildren()
{
  return static_cast<unsigned int>(m_vecChildren.size());
}


MageSgNode* MageSgNode::GetParent()
{
  return m_pParent;
}


NodeType MageSgNode::GetType()
{
  return NodeType::Node;
}


bool MageSgNode::IsActive()
{
  return m_bActive;
}


bool MageSgNode::RemoveChild(MageSgNode* pChild)
{
  // Search the Child and delete it.
  std::vector<MageSgNode*>::iterator it = std::find(m_vecChildren.begin(), m_vecChildren.end(), pChild);

  if (it != m_vecChildren.end())
  {
    pChild->SetParent(nullptr);
    m_vecChildren.erase(it);
    return true;
  }
  else
  {
    return false;
  }
}


void MageSgNode::SetActive(bool bActive)
{
  m_bActive = bActive;
}


void MageSgNode::Update(MageTimer* pTimer)
{
  // Nothing is done by default.
  // Custom Implementation in Sub-Classes.
}


void MageSgNode::SetParent(MageSgNode* pParent)
{
  m_pParent = pParent;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/