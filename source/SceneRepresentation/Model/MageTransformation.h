#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"
#include "gtx/quaternion.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageTransformation
{
public:
  /*
  * Constructor.
  */
  MageTransformation(glm::vec3 v3Translation, glm::quat qRotation, glm::vec3 v3Scaling);


  /*
  * Destructor.
  */
  virtual ~MageTransformation();


  /*
  * Returns the Rotation of the Transformation.
  * @return A Quaternion storing the Rotation.
  */
  glm::quat GetRotation();


  /*
  * Returns the Scaling of the Transformation.
  * @return A Vector storing the Scaling.
  */
  glm::vec3 GetScaling();


  /*
  * Calculates a matrix storing the Transformation.
  * @return A Matrix storing the Transformation.
  */
  glm::mat4x4 GetTransformation();


  /*
  * Returns the Translation of the Transformation.
  * @return A Vector storing the Translation.
  */
  glm::vec3 GetTranslation();


  /*
  * Interpolates between this and another Joint Transformation.
  * @param 'pOtherTransform' Pointer to another Joint Transformation.
  * @param 'dProcess' Number in the Interval [0, 1] representing the Process between two Keyframes.
  * @return Pointer to the interpolated Joint Transformation.
  */
  MageTransformation* Interpolate(MageTransformation* pOtherTransform, double dProcess);


private:
  glm::vec3 m_v3Translation;
  glm::quat m_qRotation;
  glm::vec3 m_v3Scaling;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/