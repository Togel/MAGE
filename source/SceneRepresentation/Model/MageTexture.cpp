/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTexture.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageTexture::MageTexture(GLuint iTexID, GLenum eType)
  : m_iTexID(iTexID)
  , m_eType(eType)
{
  // The Texture Information are created in the Model Factory.
  // Otherwise a Sub-Class would be needed for each Data Type.
}


MageTexture::~MageTexture()
{
  glDeleteTextures(1, &m_iTexID);
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageTexture::Bind(GLenum eTexture)
{
  glActiveTexture(eTexture);
  glBindTexture(m_eType, m_iTexID);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/