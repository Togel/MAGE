set( RelativeDir "source/SceneRepresentation/SceneGraph" )
set( RelativeSourceGroup "SourceFiles\\SceneRepresentation\\SceneGraph" )

set(Directories

)

set(DirFiles
  MageSceneGraph.cpp
  MageSceneGraph.h
  MageSgCameraNode.cpp
  MageSgCameraNode.h
  MageSgNoClipCamera.cpp
  MageSgNoClipCamera.h
  MageSgNode.cpp
  MageSgNode.h
  MageSgRenderNode.cpp
  MageSgRenderNode.h
  MageSgTransformNode.cpp
  MageSgTransformNode.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source/SceneRepresentation")