/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageDrawContext.h"

#include "../IO/mio.h"
#include "../IO/MageInputDeviceHandler.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageDrawContext::MageDrawContext()
{
  if (!Init())
  {
    glfwTerminate();
    mio::err << "MageDrawContext::MageDrawContext() -> Window Creation failed." << mio::endl;
    exit(-1);
  }

  // Initialize GLEW.
  glewExperimental = true; // Needs to be set to true to enable the Deactivation of VSync.
  if (glewInit() != GLEW_OK)
  {
    mio::err << "MageDrawContext::MageDrawContext() -> GLEW could not be initialized!" << mio::endl;
    exit(-1);
  }

  // Specify OpenGL State.
  glClearColor(0.0f, 0.0f, 1.0f, 0.0f); // Blue Background.
  glEnable(GL_DEPTH_TEST);

  // Update the Keyboard and Mouse Handlers.
  MageInputDeviceHandler::GetInstance()->SwitchDrawContext(this);
}


MageDrawContext::~MageDrawContext()
{
  glfwDestroyWindow(m_pWindow);
  glfwTerminate();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

GLFWwindow* MageDrawContext::GetWindow()
{
  return m_pWindow;
}


void MageDrawContext::SetSubTitle(std::string sSubTitle)
{
  std::string sFullTitle = m_sTitle;
  sFullTitle.append(" - ");
  sFullTitle.append(sSubTitle);

  if (m_pWindow != nullptr)
    glfwSetWindowTitle(m_pWindow, sFullTitle.c_str());
}


void MageDrawContext::SetTitle(std::string sTitle)
{
  m_sTitle = sTitle;

  if (m_pWindow != nullptr)
    glfwSetWindowTitle(m_pWindow, sTitle.c_str());
}


void MageDrawContext::SetVSync(bool bEnable)
{
  if (bEnable)
    glfwSwapInterval(1);
  else
    glfwSwapInterval(0);
}


void MageDrawContext::SetWindowSize(int iWidth, int iHeight)
{
  if(m_pWindow != nullptr)
    glfwSetWindowSize(m_pWindow, iWidth, iHeight);
}


void MageDrawContext::UpdateWindow()
{
  if (m_pWindow != nullptr)
  {
    glfwSwapBuffers(m_pWindow);
    glfwPollEvents();
  }
}


bool MageDrawContext::Init()
{
  // Initialize Window Properties.
  int iWindowWidth = 1024;
  int iWindowHeight = 768;
  std::string sWindowWidth;
  std::string sWindowHeight;

  if (mio::GetConfiguration("WindowWidth", sWindowWidth))
    iWindowWidth = std::atoi(sWindowWidth.c_str());

  if (mio::GetConfiguration("WindowHeight", sWindowHeight))
    iWindowHeight = std::atoi(sWindowHeight.c_str());

  std::string sTitle = "MAGE";
  std::string sMode = "WINDOWED";

  mio::GetConfiguration("WindowTitle", sTitle);
  mio::GetConfiguration("WindowMode", sMode);

  bool bVSync = true;
  std::string m_sVSync = "true";
  if (mio::GetConfiguration("VSync", m_sVSync))
    if (m_sVSync == "false" || m_sVSync == "FALSE")
      bVSync = false;

  // Initialize GLFW
  if (!glfwInit())
  {
    mio::err << "MageDrawContext::Init() -> GLFW Initialization failed." << mio::endl;
    return false;
  }

  // Edit Window Hints.
#ifdef __APPLE__
#if (OPENGL_VERSION >= 30)
  // request OpenGL 3.2, will return a 4.1 context on Mavericks
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4 - 1);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4 - 2);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
#else
  // non-apple
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4 - 2);
#endif

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  // Create a new Window.
  if (sMode == "WINDOWED")
    m_pWindow = glfwCreateWindow(iWindowWidth, iWindowHeight, sTitle.c_str(), NULL, NULL);
  else if (sMode == "FULLSCREEN")
    m_pWindow = glfwCreateWindow(iWindowWidth, iWindowHeight, sTitle.c_str(), glfwGetPrimaryMonitor(), NULL);
  else if (sMode == "WINDOWED_FULLSCREEN")
  {
    GLFWmonitor* pMonitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* pMode = glfwGetVideoMode(pMonitor);

    glfwWindowHint(GLFW_RED_BITS, pMode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, pMode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, pMode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, pMode->refreshRate);

    m_pWindow = glfwCreateWindow(iWindowWidth, iWindowHeight, sTitle.c_str(), pMonitor, NULL);
  }

  // Error Handling.
  if (m_pWindow == nullptr)
  {
    mio::err << "MageDrawContext::Init() -> Could not create a new Window." << mio::endl;
    glfwTerminate();
    return false;
  }

  // Create the Window Context.
  glfwMakeContextCurrent(m_pWindow);

  // Set VSync active or inactive.
  if (bVSync)
    glfwSwapInterval(1);
  else
    glfwSwapInterval(0);

  // Enable Keyboard Input Mode.
  glfwSetInputMode(m_pWindow, GLFW_STICKY_KEYS, GL_TRUE);

  return true;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/