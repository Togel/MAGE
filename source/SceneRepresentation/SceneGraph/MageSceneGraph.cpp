/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSceneGraph.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSceneGraph::MageSceneGraph()
  : m_pRoot(new MageSgNode())
  , m_pCameraNode(new MageSgCameraNode())
{
}


MageSceneGraph::~MageSceneGraph()
{
  // This will Call the Destructors of all Scene-Graph Nodes including the Camera Node.
  delete m_pRoot;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageSceneGraph::AddRenderNode(MageSgRenderNode* pRenderNode, MageSgNode* pPartent)
{
  pPartent->AddChild(pRenderNode);
}


void MageSceneGraph::AddTransformNode(MageSgTransformNode* pTransformNode, MageSgNode* pPartent)
{
  pPartent->AddChild(pTransformNode);
}


MageSgTransformNode* MageSceneGraph::CreateTransformationNode(MageSgNode* pParent)
{
  MageSgTransformNode* pTransformNode = new MageSgTransformNode();
  pParent->AddChild(pTransformNode);
  return pTransformNode;
}


MageSgCameraNode* MageSceneGraph::GetCameraNode()
{
  return m_pCameraNode;
}


MageSgNode* MageSceneGraph::GetRoot()
{
  return m_pRoot;
}


void MageSceneGraph::SetCameraNode(MageSgCameraNode* pCameraNode)
{
  m_pCameraNode = pCameraNode;
}


void MageSceneGraph::Draw()
{
  // Fill the Uniform Buffer with the Camera Transformations.
  m_pCameraNode->FillBuffer();

  // Draw opaque Models.
  DrawTraversal(m_pRoot, glm::mat4(), false);

  // Draw transparent Models.
  DrawTraversal(m_pRoot, glm::mat4(), true);
}


void MageSceneGraph::Update(MageTimer* pTimer)
{
  UpdateTraversal(m_pRoot, pTimer);
}


void MageSceneGraph::DrawTraversal(MageSgNode* pNode, glm::mat4 mat4Transformation, bool bTransparent)
{
  // Only process active Nodes.
  if (!pNode->IsActive())
    return;

  // Combine the local Transformation of the Node with the Transformations of the Parents.
  if (pNode->GetType() == NodeType::Transform)
    mat4Transformation = mat4Transformation * dynamic_cast<MageSgTransformNode*>(pNode)->GetTransformation();

  // Call the Draw Function if possible.
  else if (pNode->GetType() == NodeType::Render)
  {
    MageSgRenderNode* pRenderNode = dynamic_cast<MageSgRenderNode*>(pNode);

    // Transparent Models are drawn after opaque Models. Makes sure only Models of one Type are drawn at once.
    if (pRenderNode->IsTransparent() == bTransparent)
      pRenderNode->Draw(mat4Transformation);
  }
    
  // Continue with the Children.
  for (MageSgNode* pNode : pNode->GetChildren())
  {
    DrawTraversal(pNode, mat4Transformation, bTransparent);
  }
}


void MageSceneGraph::UpdateTraversal(MageSgNode* pNode, MageTimer* pTimer)
{
  // Only process active Nodes.
  if (!pNode->IsActive())
    return;

  // Call the Update Function.
  pNode->Update(pTimer);

  // Continue with the Children.
  for (MageSgNode* pNode : pNode->GetChildren())
  {
    UpdateTraversal(pNode, pTimer);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/