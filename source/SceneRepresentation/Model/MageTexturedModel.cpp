/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTexturedModel.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageTexturedModel::MageTexturedModel(MageVAO* pVAO, std::vector<MageTexture*> vecTextures)
  : MageModel(pVAO)
  , m_vecTextures(vecTextures)
{
}

MageTexturedModel::~MageTexturedModel()
{
  for (MageTexture* pTexture : m_vecTextures)
  {
    delete pTexture;
  }
  m_vecTextures.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageTexturedModel::BindTextures()
{
  // Get maximum number of bindable Textures.
  GLint iMaxTexPos;
  glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &iMaxTexPos);

  // Bind the Textures.
  for (GLint iTexPos = 0; iTexPos < m_vecTextures.size(); ++iTexPos)
  {
    // Too many Textures.
    if (iTexPos == iMaxTexPos)
      break;

    m_vecTextures.at(iTexPos)->Bind(GL_TEXTURE0 + iTexPos);
  }
}


bool MageTexturedModel::Draw()
{
  // Bind Textures.
  BindTextures();

  // Draw the Model.
  return m_pVAO->Draw();
}


ModelType MageTexturedModel::GetType()
{
  return ModelType::Textured;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/