/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageMesh.h"
#include "MageSceneLoader.h"

#include "../IO/mio.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageMesh::MageMesh(std::string sPath)
  : m_sPath(sPath)
{
}


MageMesh::~MageMesh()
{
  FreeVRam();
  
  m_eState = MeshState::Free;

  m_vecVertices.clear();
  m_vecNormals.clear();
  m_vecUVs.clear();

  m_vecFaceIndices.clear();
  m_vecTangents.clear();
  m_vecBitangents.clear();

  m_sPath = "";
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool MageMesh::Load()
{
  if (m_eState != MeshState::Free)
    return false;

  m_eState = MeshState::Loading;

  // Try to load the Mesh.
  if (MageSceneLoader::GetSingleton()->LoadMeshObj(m_sPath, m_vecVertices, m_vecNormals, m_vecUVs, m_vecFaceIndices))
  {
    // Compute Tangent Basis if possible.
    ComputeTangentBasis();
    m_eState = MeshState::Loaded;
    return true;
  }
    
  m_eState = MeshState::Free;
  return false;
}


void MageMesh::ComputeTangentBasis()
{
  // The Tangent Basis can only be commuted for Meshes with indexed Faces.
  if (m_vecFaceIndices.size() == 0 || m_vecVertices.size() == 0 || m_vecVertices.size() != m_vecUVs.size())
    return;

  // Prepare Vectors storing Tangents and Bi-Tangents.
  m_vecTangents.clear();
  m_vecBitangents.clear();

  m_vecTangents.resize(m_vecVertices.size(), glm::vec3(0.0, 0.0, 0.0));
  m_vecBitangents.resize(m_vecVertices.size(), glm::vec3(0.0, 0.0, 0.0));

  // Compute the Tangents for each Face consisting of 3 Points.
  for (size_t i = 0; i < m_vecFaceIndices.size(); i += 3)
  {
    // Get the Reference Vertices and UV Coordinates.
    glm::vec3 v3Vertex1 = m_vecVertices[m_vecFaceIndices[i + 0]];
    glm::vec3 v3Vertex2 = m_vecVertices[m_vecFaceIndices[i + 1]];
    glm::vec3 v3Vertex3 = m_vecVertices[m_vecFaceIndices[i + 2]];

    glm::vec2 v2UV1 = m_vecUVs[m_vecFaceIndices[i + 0]];
    glm::vec2 v2UV2 = m_vecUVs[m_vecFaceIndices[i + 1]];
    glm::vec2 v2UV3 = m_vecUVs[m_vecFaceIndices[i + 2]];


    // Calculate Edges of the Triangle.
    glm::vec3 v3Edge1To2 = v3Vertex2 - v3Vertex1;
    glm::vec3 v3Edge1To3 = v3Vertex3 - v3Vertex1;

    glm::vec2 v2UV1To2 = v2UV2 - v2UV1;
    glm::vec2 v2UV1To3 = v2UV3 - v2UV1;

    // Compute Tangent and Bi-Tangent.
    float fScale = 1.0f / (v2UV1To2.x * v2UV1To3.y - v2UV1To2.y * v2UV1To3.x);

    glm::vec3 v3Tangent = (v3Edge1To2 * v2UV1To3.y - v3Edge1To3 * v2UV1To2.y) * fScale;
    glm::vec3 v3Bitangent = (v3Edge1To3 * v2UV1To2.x - v3Edge1To2 * v2UV1To3.x) * fScale;

    // Store Tangent and Bi-Tangent.
    m_vecTangents[m_vecFaceIndices[i + 0]] += v3Tangent;
    m_vecTangents[m_vecFaceIndices[i + 1]] += v3Tangent;
    m_vecTangents[m_vecFaceIndices[i + 2]] += v3Tangent;

    m_vecBitangents[m_vecFaceIndices[i + 0]] += v3Bitangent;
    m_vecBitangents[m_vecFaceIndices[i + 1]] += v3Bitangent;
    m_vecBitangents[m_vecFaceIndices[i + 2]] += v3Bitangent;
  }

  // Normalize the Tangent and Bi-Tangent.
  for (size_t i = 0; i < m_vecTangents.size(); ++i)
  {
    glm::vec3 v3Tangent = m_vecTangents[i];
    glm::vec3 v3Bitangent = m_vecBitangents[i];

    // Make Tangents orthogonal to Normals.
    if (i < m_vecNormals.size())
    {
      glm::vec3 v3Normal = m_vecNormals[i];
      glm::vec3 v3Right;

      v3Right = glm::cross(v3Tangent, v3Normal);
      v3Tangent = glm::cross(v3Normal, v3Right);
      v3Right = glm::cross(v3Bitangent, v3Normal);
      v3Bitangent = glm::cross(v3Normal, v3Right);
    }

    // Store Normalized Results.
    m_vecTangents[i] = glm::normalize(v3Tangent);
    m_vecBitangents[i] = glm::normalize(v3Bitangent);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/