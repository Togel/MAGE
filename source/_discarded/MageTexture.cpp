#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <algorithm>

#include "MageTexture.h"
#include "MageSceneLoader.h"

#include "../IO/mio.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageTexture::MageTexture(std::string sPath, TextureType eType)
  : m_sPath(sPath)
{
  // Get Data Format.
  std::string sFormat = sPath.substr(sPath.size() - 3);
  transform(sFormat.begin(), sFormat.end(), sFormat.begin(), ::toupper);

  // Select the specific Loader for the given Type.
  if (sFormat == "DDS")
    m_eFormat = TextureFormat::DDS;

  // Specify at which Texture Position the Texture should be uploaded.
  switch (eType)
  {
  case TextureType::Normal:
    m_eUnit = GL_TEXTURE0;
    break;
  case TextureType::ShadowMap:
    m_eUnit = GL_TEXTURE1;
    break;
  default:
    m_eUnit = GL_TEXTURE0;
  }
}


MageTexture::~MageTexture()
{
  FreeVRam();

  m_eState = TextureState::Free;

  std::fill(std::begin(m_aHeader), std::end(m_aHeader), 0);

  m_sPath = "";
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageTexture::Bind()
{
  if (m_eState != TextureState::Uploaded)
    return;

  glActiveTexture(m_eUnit);
  glBindTexture(m_eType, m_iID);
}


bool MageTexture::FreeVRam()
{
  if (m_eState != TextureState::Uploaded)
    return false;

  glDeleteTextures(1, &m_iID);
  m_eState = TextureState::Loaded;

  return true;
}


bool MageTexture::Load()
{
  if (m_eState != TextureState::Free)
    return false;

  m_eState = TextureState::Loading;

  // Try to load the Mesh.
  if (MageSceneLoader::GetSingleton()->LoadTextureDDS(m_sPath, m_aHeader, m_sBuffer))
  {
    m_eState = TextureState::Loaded;
    return true;
  }

  m_eState = TextureState::Free;
  return false;
}


bool MageTexture::Upload()
{
  if (m_eState != TextureState::Loaded)
    return false;

  m_eState = TextureState::Uploading;

  if (m_eFormat == TextureFormat::DDS)
  {
    UploadDDS();

    m_eState = TextureState::Uploaded;

    return true;
  }

  mio::err << "Can not upload invalid Texture Format." << mio::endl;
  m_eState = TextureState::Loaded;

  return false;
}


void MageTexture::UploadDDS()
{
  // Load the Surface Description.
  unsigned int iHeight      = *(unsigned int*)&(m_aHeader[8]);
  unsigned int iWidth       = *(unsigned int*)&(m_aHeader[12]);
  unsigned int iLinearSize  = *(unsigned int*)&(m_aHeader[16]);
  unsigned int iMipMapCount = *(unsigned int*)&(m_aHeader[24]);
  unsigned int iFourCC      = *(unsigned int*)&(m_aHeader[80]);

  // Read the Compression Format.
  unsigned int iFormat;
  switch (iFourCC)
  {
  case FOURCC_DXT1:
    iFormat = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
    break;
  case FOURCC_DXT3:
    iFormat = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
    break;
  case FOURCC_DXT5:
    iFormat = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
    break;
  default:
    mio::err << "Unknown Texture Compression." << mio::endl;
  }

  // Create the actual Texture.
  glGenTextures(1, &m_iID);

  // Bind the Texture.
  glBindTexture(GL_TEXTURE_2D, m_iID);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  unsigned int iBlockSize = (iFormat == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
  unsigned int iOffset = 0;

  // Load Mipmaps.
  for (unsigned int iLevel = 0; iLevel < iMipMapCount && (iWidth || iHeight); ++iLevel)
  {
    unsigned int iSize = ((iWidth + 3) / 4) * ((iHeight + 3) / 4) * iBlockSize;
    glCompressedTexImage2D(GL_TEXTURE_2D, iLevel, iFormat, iWidth, iHeight, 0, iSize, m_sBuffer + iOffset);

    iOffset += iSize;
    iWidth /= 2;
    iHeight /= 2;

    // Deal with Non-Power-Of-Two Textures.
    if (iWidth < 1)
      iWidth = 1;
    if (iHeight < 1)
      iHeight = 1;
  }

  // Define Filtering Operation on the Mipmaps.
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  // Let OpenGL generate Mipmaps.
  if (iMipMapCount <= 1)
    glGenerateMipmap(GL_TEXTURE_2D);

  // Unbind Texture.
  glBindTexture(GL_TEXTURE_2D, 0);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/