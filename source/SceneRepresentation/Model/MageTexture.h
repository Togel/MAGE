#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageTexture
{
public:
  /*
  * Constructor.
  */
  MageTexture(GLuint iTexID, GLenum eType = GL_TEXTURE_2D);


  /*
  * Destructor.
  */
  ~MageTexture();


  /*
  * Binds the Texture to a specific Texture Unit.
  * @param 'eTexture' defines the Texture Unit i.e. GL_TEXTURE0, GL_TEXTURE1, ...
  */
  void Bind(GLenum eTexture);


private:
  // Stores the unique Texture ID.
  GLuint m_iTexID = 0;

  // Stores the Texture Type.
  // See 'glBindTexture' 'target'-Documentation for more Information.
  GLenum m_eType = GL_TEXTURE_2D;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/