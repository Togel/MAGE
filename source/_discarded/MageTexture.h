#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <string>

#include "GL/glew.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum TextureFormat
{
  INVALID,
  DDS
};


enum TextureType
{
  Normal,
  ShadowMap
};


enum TextureState
{
  Free,
  Loading,
  Loaded,
  Uploading,
  Uploaded
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageTexture
{
public:
  /*
  * Constructor.
  */
  MageTexture(std::string sPath, TextureType eType);


  /*
  * Destructor.
  */
  virtual ~MageTexture();


  /*
  * Bind the Texture.
  */
  void Bind();


  /*
  * Remove the Texture from the Graphics Card.
  * @return true if the Texture has been removed from the Graphics Card.
  */
  bool FreeVRam();


  /*
  * Loads the Texture an store it in the Memory.
  * Loading the Texture will not upload it to the Graphics Card!
  * However, before uploading the Texture it needs to be loaded.
  * @return true if the Texture has been loaded.
  */
  bool Load();


  /*
  * Uploads the Texture to the Graphics Card.
  * @return true if the Texture has been uploaded successfully.
  */
  bool Upload();


protected:
  std::string m_sPath;

  unsigned char  m_aHeader[124];
  unsigned char* m_sBuffer;

  TextureState  m_eState  = TextureState::Free;
  TextureFormat m_eFormat = TextureFormat::INVALID;

  GLuint m_iID = 0;
  GLenum m_eType = GL_TEXTURE_2D;
  GLenum m_eUnit = GL_TEXTURE0;

  /*
  * Uploads a DDS-Texture to the Graphics Card.
  */
  void UploadDDS();
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/