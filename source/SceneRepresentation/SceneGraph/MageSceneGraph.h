#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../MageScene.h"

#include "MageSgNode.h"
#include "MageSgCameraNode.h"
#include "MageSgRenderNode.h"
#include "MageSgTransformNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageSceneGraph : public MageScene
{
public:
  /*
  * Constructor.
  */
  MageSceneGraph();


  /*
  * Destructor.
  */
  virtual ~MageSceneGraph();


  /*
  * Adds another Render Node to the Scene Graph.
  * @param 'pRenderNode' defines the new Render Node.
  * @param 'pPartent' specifies the Parent of the Render Node.
  */
  void AddRenderNode(MageSgRenderNode* pRenderNode, MageSgNode* pPartent);


  /*
  * Adds another Transformation Node to the Scene Graph.
  * @param 'pTransformNode' defines the new Transformation Node.
  * @param 'pPartent' specifies the Parent of the Transform Node.
  */
  void AddTransformNode(MageSgTransformNode* pTransformNode, MageSgNode* pPartent);


  /*
  * Creates another Transformation Node and adds it to the Scene Graph.
  * @param 'pPartent' specifies the Parent of the Render Node.
  * @return Pointer to the new Transformation Node.
  */
  MageSgTransformNode* CreateTransformationNode(MageSgNode* pParent);


  /*
  * Returns the Camera Node.
  * @return Pointer to the Camera Node.
  */
  MageSgCameraNode* GetCameraNode();


  /*
  * Returns the Root Node.
  * @return Pointer to the Root Node.
  */
  MageSgNode* GetRoot();


  /*
  * Resets the Camera Node.
  * @param 'pCameraNode' defines the new Camera Node.
  */
  void SetCameraNode(MageSgCameraNode* pCameraNode);


  /*
  * Traverse the Scene Data Structure and Draws the Scene.
  */
  void Draw() override;


  /*
  * Traverse the Scene Data Structure and executes Updates.
  * @param 'pTimer' Global Timer used by the Thread.
  */
  void Update(MageTimer* pTimer) override;


private:
  // Stores the Root Node of the Scene.
  MageSgNode* m_pRoot = nullptr;

  // Stores the Camera Node.
  MageSgCameraNode* m_pCameraNode = nullptr;


  /*
  * Executes the Draw Traversal.
  * Running through all Nodes calling the Draw Function if the current Node is a Render Node.
  * @param 'pNode' defines the currently handled Node.
  * @param 'mat4Transformation' stores the Transformation of all Parent Nodes.
  * @param 'bTransparent' defines whether all Transparent (true) or all opaque (false) Models should be drawn.
  */
  void DrawTraversal(MageSgNode* pNode, glm::mat4 mat4Transformation, bool bTransparent);


  /*
  * Executes the Update Traversal.
  * Running through all Nodes calling the Update Function if the current Node is capable of it.
  * @param 'pNode' defines the currently handled Node.
  * @param 'pTimer' Shared Pointer to the Timer used by the Thread.
  */
  void UpdateTraversal(MageSgNode* pNode, MageTimer* pTimer);

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/