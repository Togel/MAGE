#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"

#include "GL/glew.h"

#include <vector>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageMesh
{
public:
  /*
  * Constructor.
  */
  MageMesh(std::vector<glm::vec3>& vecVertices,
    std::vector<glm::vec3>& vecNormals,
    std::vector<glm::vec3>& vecColors,
    std::vector<glm::vec2>& vecUVs,
    std::vector<unsigned int>& vecFaceIndices,
    std::vector<glm::vec3> vecTangents,
    std::vector<glm::vec3> vecBiTangents,
    std::vector<glm::ivec4> vecJointIndices,
    std::vector<glm::vec4> vecJointWeights);


  /*
  * Destructor.
  */
  virtual ~MageMesh();


  /*
  * Returns the Bi-Tangents of the Mesh.
  * @return The Bi-Tangents of the Mesh.
  */
  const std::vector<glm::vec3>& GetBiTangents();


  /*
  * Returns the Colors of the Mesh.
  * @return The Colors of the Mesh.
  */
  const std::vector<glm::vec3>& GetColors();


  /*
  * Returns References to all Data of the Mesh.
  */
  void GetData(std::vector<glm::vec3>& vecVertices,
    std::vector<glm::vec3>& vecNormals,
    std::vector<glm::vec3>& vecColors,
    std::vector<glm::vec2>& vecUVs,
    std::vector<unsigned int>& vecFaceIndices,
    std::vector<glm::vec3>& vecTangents,
    std::vector<glm::vec3>& vecBiTangents,
    std::vector<glm::ivec4>& vecJointIndices,
    std::vector<glm::vec4>& vecJointWeights);


  /*
  * Returns the Face Indices of the Mesh.
  * @return The Face Indices of the Mesh.
  */
  const std::vector<unsigned int>& GetFaceIndices();


  /*
  * Returns the Joint Indices of the Mesh.
  * @return The Joint Indices of the Mesh.
  */
  const std::vector<glm::ivec4>& GetJointIndices();


  /*
  * Returns the Joint Weights of the Mesh.
  * @return The Joint Weights of the Mesh.
  */
  const std::vector<glm::vec4>& GetJointWeights();


  /*
  * Returns the Type of Primitives which are rendered.
  * @return The Type of Primitives which are rendered.
  */
  GLenum GetMode();


  /*
  * Returns the Normals of the Mesh.
  * @return The Normals of the Mesh.
  */
  const std::vector<glm::vec3>& GetNormals();


  /*
  * Returns the Polygon Mode i.e. Triangles, Quads, or something else.
  * See 'glDrawArrays' Documentation for more Information.
  * @return The Polygon Mode.
  */
  GLenum GetPolygonMode();


  /*
  * Returns the Size of the Mesh.
  * The Size equals the Number of Face Indices if they are available.
  * Otherwise the Size equals the Number of Vertices.
  * @return The Size of the Mesh.
  */
  GLsizei GetSize();


  /*
  * Returns the Tangents of the Mesh.
  * @return The Tangents of the Mesh.
  */
  const std::vector<glm::vec3>& GetTangents();

  
  /*
  * Returns the UV Coordinates of the Mesh.
  * @return The UV Coordinates of the Mesh.
  */
  const std::vector<glm::vec2>& GetUVs();


  /*
  * Returns the Vertices of the Mesh.
  * @return The Vertices of the Mesh.
  */
  const std::vector<glm::vec3>& GetVertices();


protected:
  // Stores the Vertices of the Mesh.
  std::vector<glm::vec3> m_vecVertices;

  // Stores the Normals of the Mesh.
  std::vector<glm::vec3> m_vecNormals;

  // Stores the Colors of the Mesh.
  std::vector<glm::vec3> m_vecColors;

  // Stores the UV Coordinates of the Mesh.
  std::vector<glm::vec2> m_vecUVs;

  // Stores the Face Indices of the Mesh.
  std::vector<unsigned int> m_vecFaceIndices;

  // Stores the Tangents of the Mesh.
  std::vector<glm::vec3> m_vecTangents;

  // Stores the Bi-Tangents of the Mesh.
  std::vector<glm::vec3> m_vecBiTangents;

  // Stores the Joint Indices of the Mesh.
  std::vector<glm::ivec4> m_vecJointIndices;

  // Stores the Joint Weights of the Mesh.
  std::vector<glm::vec4> m_vecJointWeights;  

  // Stores the Size of the Mesh.
  // Depends on whether Face Indices exist.
  // Otherwise the Number of Vertices is stored.
  // Needed to Draw the Mesh.
  GLsizei m_iSize = 0;

  // Stores the Polygon Mode of the Mesh.
  // See 'glDrawArrays' Documentation for more Information.
  GLenum m_eMode = GL_TRIANGLES;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/