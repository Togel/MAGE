#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../Core/MageSingleton.h"

#include "glm.hpp"

#include <vector>
#include <string>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageSceneLoader : public MageSingleton<MageSceneLoader>
{
  friend class MageSingleton<MageSceneLoader>;


public:
  /*
  * Destructor.
  */
  ~MageSceneLoader();


  /*
  * Loads a Obj Mesh.
  * @param 'sPath' defines the Path to the Mesh.
  * @param 'vecVertices' stores the Vertices if the Loading Process has been successful.
  * @param 'vecNormals' stores the Normals if the Loading Process has been successful.
  * @param 'vecUVs' stores the UV Coordinates if the Loading Process has been successful.
  * @param 'vecFacesIndices' stores the Indices of the Faces if the Loading Process has been successful.
  * @return true if the Mesh has been loaded successfully.
  */
  bool LoadMeshObj(const std::string sPath, std::vector<glm::vec3>& vecVertices, std::vector<glm::vec3>& vecNormals, std::vector<glm::vec2>& vecUVs, std::vector<unsigned int>& vecFacesIndices);


  /*
  * Loads a DDS Texture.
  * @param 'sPath' defines the Path to the Texture.
  * @param 'aHeader' stores the Texture Information.
  * @param 'sBuffer' contains the Mipmap Information.
  */
  bool LoadTextureDDS(const std::string sPath, unsigned char (&aHeader)[124], unsigned char* sBuffer);


protected:
  MageSceneLoader();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/