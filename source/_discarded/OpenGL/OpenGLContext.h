#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../MageDrawContext.h"

#include "GL/glew.h"
#include "glfw3.h"

#include <memory>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class OpenGLDrawContext : public MageDrawContext
{
public:
  /*
  * Constructor.
  */
  OpenGLDrawContext();


  /*
  * Destructor.
  */
  virtual ~OpenGLDrawContext();


  /*
  * Sets the Title of the Window.
  * @param 'sTitle' new Title of the Window.
  */
  virtual void SetTitle(std::string sTitle) = 0;


  /*
  * Sets VSync on or off.
  * VSync synchronizes the Framerate to the Refreshrate of the Monitor.
  * @param 'bEnable' specifies whether VSync should be enable or disabled.
  */
  virtual void SetVSync(bool bEnable) = 0;


  /*
  * Sets the Size of the Window.
  * @param 'iWidth' define the Width of the Window.
  * @param 'iHeight' define the Width of the Window.
  */
  virtual void SetWindowSize(int iWidth, int iHeight) = 0;


private:
  GLFWwindow* m_pWindow = nullptr;


  /*
  * Initializes the OpenGl Context.
  */
  bool Init();
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/