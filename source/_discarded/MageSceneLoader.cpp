/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSceneLoader.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSceneLoader::MageSceneLoader()
{
}

MageSceneLoader::~MageSceneLoader()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool MageSceneLoader::LoadMeshObj(const std::string sPath, std::vector<glm::vec3>& vecVertices, std::vector<glm::vec3>& vecNormals, std::vector<glm::vec2>& vecUVs, std::vector<unsigned int>& vecFacesIndices)
{
  return false;
}


bool MageSceneLoader::LoadTextureDDS(const std::string sPath, unsigned char(&aHeader)[124], unsigned char* sBuffer)
{
  return false;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/