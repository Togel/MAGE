cmake_minimum_required(VERSION 2.8)
# Project Name
set(PROJECT_NAME MultiApiGameEngine)
PROJECT(${PROJECT_NAME})


###############################################################
# Compile external dependencies                               #
###############################################################
get_filename_component(PARENT_DIR ${CMAKE_SOURCE_DIR} PATH)
set(BinaryDir ${PARENT_DIR}/../Binaries/${PROJECT_NAME})
#set(EXECUTABLE_OUTPUT_PATH ${BinaryDir} CACHE PATH "Build directory" FORCE)
set(LIBRARY_OUTPUT_PATH ${BinaryDir} CACHE PATH "Build directory" FORCE)


find_package(OpenGL REQUIRED)

add_subdirectory(external)

include_directories(
  external/glfw-3.0.3/include/GLFW/
  external/glm-0.9.4.0/glm
  external/glew-1.9.0/include/
  external/assimp-3.3.1/include/
  external/soil-1.16/src/
)


## Assimp and SOIL## 
# Find Release Library.
find_library(SOIL_LIBRARY_RELEASE NAMES Soil HINTS ${BinaryDir}/Release)
find_library(ZLIB_LIBRARY_RELEASE NAMES zlibstatic HINTS ${BinaryDir}/Release)
find_library(ASSIMP_LIBRARY_RELEASE NAMES assimp-vc140-mt HINTS ${BinaryDir}/Release)
 
# Find Debug Library.
find_library(SOIL_LIBRARY_DEBUG NAMES Soil HINTS ${BinaryDir}/Debug)
find_library(ZLIB_LIBRARY_DEBUG NAMES zlibstaticd HINTS ${BinaryDir}/Debug)
find_library(ASSIMP_LIBRARY_DEBUG NAMES assimp-vc140-mt HINTS ${BinaryDir}/Debug)
    
# Set Library to Debug or Release depending on Compiling Mode.
set(SOIL_LIBRARIES "")
if(SOIL_LIBRARY_RELEASE AND SOIL_LIBRARY_DEBUG)
    set(SOIL_LIBRARIES 
        optimized   ${SOIL_LIBRARY_RELEASE}
        debug       ${SOIL_LIBRARY_DEBUG})
elseif(SOIL_LIBRARY_RELEASE)
    set(SOIL_LIBRARIES ${SOIL_LIBRARY_RELEASE})
elseif(SOIL_LIBRARY_DEBUG)
    set(SOIL_LIBRARIES ${SOIL_LIBRARY_DEBUG})
endif()
    
# Set Library to Debug or Release depending on Compiling Mode.
set(ZLIB_LIBRARIES "")
if(ZLIB_LIBRARY_RELEASE AND ZLIB_LIBRARY_DEBUG)
    set(ZLIB_LIBRARIES 
        optimized   ${ZLIB_LIBRARY_RELEASE}
        debug       ${ZLIB_LIBRARY_DEBUG})
elseif(ZLIB_LIBRARY_RELEASE)
    set(ZLIB_LIBRARIES ${ZLIB_LIBRARY_RELEASE})
elseif(ZLIB_LIBRARY_DEBUG)
    set(ZLIB_LIBRARIES ${ZLIB_LIBRARY_DEBUG})
endif()
    
# Set Library to Debug or Release depending on Compiling Mode.
set(ASSIMP_LIBRARIES "")
if(ASSIMP_LIBRARY_RELEASE AND ASSIMP_LIBRARY_DEBUG)
    set(ASSIMP_LIBRARIES 
        optimized   ${ASSIMP_LIBRARY_RELEASE}
        debug       ${ASSIMP_LIBRARY_DEBUG})
elseif(ASSIMP_LIBRARY_RELEASE)
    set(ASSIMP_LIBRARIES ${ASSIMP_LIBRARY_RELEASE})
elseif(ASSIMP_LIBRARY_DEBUG)
    set(ASSIMP_LIBRARIES ${ASSIMP_LIBRARY_DEBUG})
endif()



set(ALL_LIBS
  ${OPENGL_LIBRARY}
	GLFW_303
	GLEW_190
  ${SOIL_LIBRARIES}
  ${ZLIB_LIBRARIES}
  ${ASSIMP_LIBRARIES}
)


add_definitions(
	-DTW_STATIC
	-DTW_NO_LIB_PRAGMA
	-DTW_NO_DIRECT3D
	-DGLEW_STATIC
	-D_CRT_SECURE_NO_WARNINGS
)

# set to c++11
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# Include Source Files
include("source/_SourceFiles.cmake")
add_library(${PROJECT_NAME} STATIC ${ProjectSources})

target_link_libraries(${PROJECT_NAME} ${ALL_LIBS})