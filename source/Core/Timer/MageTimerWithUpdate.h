#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTimer.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageTimerWithUpdate : public MageTimer
{
public:
  /*
  * Constructor.
  * NOTE: 
  * YOU PROBALBLY WANT TO USE THE TIMER OF A THREAD.
  * IN THIS WAY YOU DO NOT NEED TO UPDATE THE TIMER BY HAND.
  * ONLY USE THIS CONSTRUCTOR IF YOU KNOW WHAT YOU ARE DOING.
  */
  MageTimerWithUpdate();


  /*
  * Destructor.
  */
  ~MageTimerWithUpdate();


  /*
  * Updates the Timer resetting its Member-Variables storing the Loop Time and Time since Initialization.
  */
  void _Update();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/