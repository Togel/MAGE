#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include <chrono>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/
typedef std::chrono::high_resolution_clock MageClock;


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageTimer
{
public:
  /*
  * Destructor.
  */
  ~MageTimer();


  /*
  * Returns the Time since the Timer has been updated for the last Time in Milliseconds.
  * Update takes place automatically once per Loop of the Thread.
  * @return The Time since the last Timer Update in Milliseconds.
  */
  double GetLoopTime();


  /*
  * Returns the Time since the Timer has been initialized in Seconds.
  * @return The Time since the Timer has been initialized in Seconds.
  */
  double GetInitTime();


protected:
  // Timestamp of the Initialization Time.
  MageClock::time_point m_tInitTime;
  // Timestamp of the Time of the last Update.
  MageClock::time_point m_tLastTime;

  // Stores the Time elapsed since the last Update.
  double m_dLoopTime = 0.0;
  // Stores the Time elapsed since the Initialization.
  double m_dInitTime  = 0.0;


  /*
  * Constructor.
  */
  MageTimer();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/