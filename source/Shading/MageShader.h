#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"

#include <string>
#include <vector>
#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum ShaderClass
{
  PrimitiveShader,
  CameraShader
};


enum ShaderType
{
  Vertex = GL_VERTEX_SHADER,
  Fragment = GL_FRAGMENT_SHADER,
  TessellationControl = GL_TESS_CONTROL_SHADER,
  TessellationEvaluation = GL_TESS_EVALUATION_SHADER,
  Geometry = GL_GEOMETRY_SHADER,
  Compute = GL_COMPUTE_SHADER
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageShader
{
public:
  /*
  * Constructor.
  */
  MageShader();


  /*
  * Destructor.
  */
  ~MageShader();


  /*
  * Activate the Shader.
  */
  void Bind();


  /*
  * Returns the Class of the Shader.
  * @return Enumeration Entry defining the Class of the Shader.
  */
  virtual ShaderClass GetClass();


  /*
  * Returns the Location Index of the specific Uniform Variable.
  * @param 'sName' The Name of the Uniform Variable.
  * @return The Location Index of the specific Uniform Variable.
  */
  GLuint GetUniformLocation(const std::string& sName) const;


  /*
  * Initializes a new Shader from a given File.
  * @param 'eType' defines the Shader Type.
  * @param 'sShader' defines the Path to the Shader.
  */
  bool InitShaderFromFile(ShaderType eType, const std::string& sShader);


  /*
  * Initializes a new Shader from a given String.
  * @param 'eType' defines the Shader Type.
  * @param 'sShader' Defines the Shader.
  */
  bool InitShaderFromString(ShaderType eType, const std::string& sShader);


  /*
  * Returns whether the Shader is usable.
  * @return 'true' if the Shader is usable.
  */
  bool IsUsable();


  /*
  * Links the Shader Program to make it executable.
  */
  bool Link();


  /*
  * Deactivate the Shader.
  */
  void Release();


  /*
  * Modifies the Value of a Uniform Variable.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'pValue' Specifies a Pointer to an Array that will be used to update the specified Uniform Variable.
  */
  void SetUniform(int iLoc, const GLfloat* pValue);


  /*
  * Modifies the Value of a Uniform Variable.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'pValue' Specifies a Pointer to an Array that will be used to update the specified Uniform Variable.
  */
  void SetUniform(int iLoc, int iArraySize, const GLfloat* pValue);


  /*
  * Modifies the Value of a Uniform Variable.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'iUniform1' Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, int iUniform1);


  /*
  * Modifies the Value of a Uniform Variable.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'fUniform1' Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1);


  /*
  * Modifies the Value of a Uniform Variable Array.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'fUniform1' Specifies a new Value to be used for the specified Uniform Variable.
  * @param 'fUniform2' Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1, float fUniform2);


  /*
  * Modifies the Value of a Uniform Variable Array.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'fUniform1' Specifies a new Value to be used for the specified Uniform Variable.
  * @param 'fUniform2' Specifies a new Value to be used for the specified Uniform Variable.
  * @param 'fUniform3' Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3);


  /*
  * Modifies the Value of a Uniform Variable Array.
  * @param 'iLoc' Specifies the Location of the Uniform Variable to be modified.
  * @param 'fUniform1' Specifies a new Value to be used for the specified Uniform Variable.
  * @param 'fUniform2' Specifies a new Value to be used for the specified Uniform Variable.
  * @param 'fUniform3' Specifies a new Value to be used for the specified Uniform Variable.
  * @param 'fUniform4' Specifies a new Value to be used for the specified Uniform Variable.
  */
  void SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3, float fUniform4);  


protected:
  // Stores the Handle to the Shader Program.
  GLuint m_iProgram = 0;

  // Stores whether the Shader Program is usable, yet.
  bool m_bUsable = false;

  // Vector storing the Vertex Shaders linked to the Program.
  std::vector<GLuint> m_vecVertexShader;
  // Vector storing the Fragment Shaders linked to the Program.
  std::vector<GLuint> m_vecFragmentShader;
  // Vector storing the Tesselation Control Shaders linked to the Program.
  std::vector<GLuint> m_vecTesselationControlShader;
  // Vector storing the Tesselation Evaluation Shaders linked to the Program.
  std::vector<GLuint> m_vecTesselationEvaluationShader;
  // Vector storing the Geometry Shaders linked to the Program.
  std::vector<GLuint> m_vecGeometryShader;
  // Vector storing the Compute Shaders linked to the Program.
  std::vector<GLuint> m_vecComputeShader;

  // Maps the Uniform Names of the linked Shaders to their Handles.
  std::map<std::string, GLint> m_mapUniforms;


  /*
  * Creates the Mapping between the Uniform Names and their Handles.
  */
  virtual void MapUniforms();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/