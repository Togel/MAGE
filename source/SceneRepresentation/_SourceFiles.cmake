set( RelativeDir "source/SceneRepresentation" )
set( RelativeSourceGroup "SourceFiles\\SceneRepresentation" )

set(Directories
  Quadtree
  SceneGraph
  Model
)

set(DirFiles
  _SourceFiles.cmake
  MageScene.cpp
  MageScene.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")