#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"

#include <string>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
struct MageTexture
{
  MageTexture(std::string sPath, GLuint iTextureID, GLenum eTextureType, GLenum eTexturePosition)
  {
    m_sPath = sPath;
    m_iTextureID = iTextureID;
    m_eTextureType = eTextureType;
    m_eTexturePosition = eTexturePosition;
  }

  std::string m_sPath;
  GLuint m_iTextureID = 0;
  GLenum m_eTextureType = GL_TEXTURE_2D;
  GLenum m_eTexturePosition = GL_TEXTURE0;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/