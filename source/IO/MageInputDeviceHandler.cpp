/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageInputDeviceHandler.h"

#include <algorithm>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageInputDeviceHandler::MageInputDeviceHandler()
{
  // Create the Mapping between GLFW Keys and Strings.
  CreateMapping();

  // Fill Status Map for the Keyboard. In the Beginning all Key are not pushed (idle).
  std::map<std::string, int>::iterator it_Mapping;
  for (it_Mapping = m_mapKeyMappings.begin(); it_Mapping != m_mapKeyMappings.end(); ++it_Mapping)
  {
    m_mapKeyStatus.insert(std::make_pair(it_Mapping->first, KeyStatus::KeyIdle));
  }

  // Fill Status Map for the Mouse. In the Beginning all Key are not pushed (idle).
  for (it_Mapping = m_mapMouseMappings.begin(); it_Mapping != m_mapMouseMappings.end(); ++it_Mapping)
  {
    m_mapKeyStatus.insert(std::make_pair(it_Mapping->first, KeyStatus::KeyIdle));
  }
}


MageInputDeviceHandler::~MageInputDeviceHandler()
{
  m_mapKeyStatus.clear();
  m_mapKeyMappings.clear();
  m_mapMouseMappings.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageInputDeviceHandler::CenterMouse()
{
  GLFWwindow* pWindow = m_pDrawContext->GetWindow();

  // Error Handling.
  if (pWindow)
  {
    // Get the Window Size.
    int iWidth, iHeight;
    glfwGetWindowSize(pWindow, &iWidth, &iHeight);

    // Reposition the Cursor.
    glfwSetCursorPos(pWindow, static_cast<double>(iWidth) / 2.0, static_cast<double>(iHeight) / 2.0);
  }
}


KeyStatus MageInputDeviceHandler::GetKeyStatus(std::string sKey)
{
  // Transform the Key into Caps only.
  std::transform(sKey.begin(), sKey.end(), sKey.begin(), ::toupper);

  // Check whether the Key is tracked by the Handler.
  if(m_mapKeyStatus.find(sKey) != m_mapKeyStatus.end())
    return m_mapKeyStatus[sKey];

  // The Key is not tracked by the Handler.
  return KeyStatus::KeyIdle;
}


glm::vec2 MageInputDeviceHandler::GetMouseDistanceFromCenter()
{
  // Error Handling.
  if (m_pDrawContext->GetWindow())
  {
    // Get the current Mouse Position.
    glm::vec2 v2MousePos = GetMousePosition();

    // Get the Window Size.
    int iWidth, iHeight;
    glfwGetWindowSize(m_pDrawContext->GetWindow(), &iWidth, &iHeight);

    // Calculate Center Position.
    glm::vec2 v2Center = glm::vec2(iWidth / 2.0f, iHeight / 2.0f);

    // Window Origin is in Top-Left Corner. Flip y-Component to increase y-Result when when is above Window Center.
    v2MousePos.y = iHeight - v2MousePos.y;

    return (v2MousePos - v2Center);
  }

  return glm::vec2(0.0f, 0.0f);
}


glm::vec2 MageInputDeviceHandler::GetMousePosition()
{
  // Error Handling.
  if (m_pDrawContext->GetWindow())
  {
    // Get the Cursor Position.
    double dX, dY;
    glfwGetCursorPos(m_pDrawContext->GetWindow(), &dX, &dY);

    return glm::vec2(dX, dY);
  }

  return glm::vec2(0.0f, 0.0f);
}


void MageInputDeviceHandler::SwitchDrawContext(MageDrawContext* pDrawContext)
{
  m_pDrawContext = pDrawContext;
}


void MageInputDeviceHandler::ToggleMouseVisibility(bool bVisible)
{
  // Error Handling.
  if (m_pDrawContext->GetWindow())
  {
    if (bVisible)
      glfwSetInputMode(m_pDrawContext->GetWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    else
      glfwSetInputMode(m_pDrawContext->GetWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
  }
}


void MageInputDeviceHandler::_Update()
{
  GLFWwindow* pWindow = m_pDrawContext->GetWindow();
  
  // The Key Status can only be updated if there is a Window.
  if (pWindow == nullptr)
    return;

  // Update Status of Keyboard Keys.
  std::map<std::string, int>::iterator it_Mapping;
  for (it_Mapping = m_mapKeyMappings.begin(); it_Mapping != m_mapKeyMappings.end(); ++it_Mapping)
  {
    // Button is pushed.
    if (glfwGetKey(pWindow, it_Mapping->second) == GLFW_PRESS)
    {
      m_mapKeyStatus[it_Mapping->first] = KeyStatus::KeyPushed;
    }
    // Button is not pushed.
    else
    {
      // Button has been released.
      if (m_mapKeyStatus[it_Mapping->first] == KeyStatus::KeyPushed)
        m_mapKeyStatus[it_Mapping->first] = KeyStatus::KeyReleased;
      // Button is still idle.
      else
        m_mapKeyStatus[it_Mapping->first] = KeyStatus::KeyIdle;
    }
  }

  // Update Status of Mouse Keys.
  for (it_Mapping = m_mapMouseMappings.begin(); it_Mapping != m_mapMouseMappings.end(); ++it_Mapping)
  {
    // Button is pushed.
    if (glfwGetMouseButton(pWindow, it_Mapping->second) == GLFW_PRESS)
    {
      m_mapKeyStatus[it_Mapping->first] = KeyStatus::KeyPushed;
    }
    // Button is not pushed.
    else
    {
      // Button has been released.
      if (m_mapKeyStatus[it_Mapping->first] == KeyStatus::KeyPushed)
        m_mapKeyStatus[it_Mapping->first] = KeyStatus::KeyReleased;
      // Button is still idle.
      else
        m_mapKeyStatus[it_Mapping->first] = KeyStatus::KeyIdle;
    }
  }
}


void MageInputDeviceHandler::CreateMapping()
{
  // Keyboard Mappings.
  m_mapKeyMappings["SPACE"] = GLFW_KEY_SPACE;
  m_mapKeyMappings["'"] = GLFW_KEY_APOSTROPHE;
  m_mapKeyMappings[","] = GLFW_KEY_COMMA;
  m_mapKeyMappings["-"] = GLFW_KEY_MINUS;
  m_mapKeyMappings["."] = GLFW_KEY_PERIOD;
  m_mapKeyMappings["/"] = GLFW_KEY_SLASH;
  m_mapKeyMappings["0"] = GLFW_KEY_0;
  m_mapKeyMappings["1"] = GLFW_KEY_1;
  m_mapKeyMappings["2"] = GLFW_KEY_2;
  m_mapKeyMappings["3"] = GLFW_KEY_3;
  m_mapKeyMappings["4"] = GLFW_KEY_4;
  m_mapKeyMappings["5"] = GLFW_KEY_5;
  m_mapKeyMappings["6"] = GLFW_KEY_6;
  m_mapKeyMappings["7"] = GLFW_KEY_7;
  m_mapKeyMappings["8"] = GLFW_KEY_8;
  m_mapKeyMappings["9"] = GLFW_KEY_9;
  m_mapKeyMappings[";"] = GLFW_KEY_SEMICOLON;
  m_mapKeyMappings["="] = GLFW_KEY_EQUAL;
  m_mapKeyMappings["A"] = GLFW_KEY_A;
  m_mapKeyMappings["B"] = GLFW_KEY_B;
  m_mapKeyMappings["C"] = GLFW_KEY_C;
  m_mapKeyMappings["D"] = GLFW_KEY_D;
  m_mapKeyMappings["E"] = GLFW_KEY_E;
  m_mapKeyMappings["F"] = GLFW_KEY_F;
  m_mapKeyMappings["G"] = GLFW_KEY_G;
  m_mapKeyMappings["H"] = GLFW_KEY_H;
  m_mapKeyMappings["I"] = GLFW_KEY_I;
  m_mapKeyMappings["J"] = GLFW_KEY_J;
  m_mapKeyMappings["K"] = GLFW_KEY_K;
  m_mapKeyMappings["L"] = GLFW_KEY_L;
  m_mapKeyMappings["M"] = GLFW_KEY_M;
  m_mapKeyMappings["N"] = GLFW_KEY_N;
  m_mapKeyMappings["O"] = GLFW_KEY_O;
  m_mapKeyMappings["P"] = GLFW_KEY_P;
  m_mapKeyMappings["Q"] = GLFW_KEY_Q;
  m_mapKeyMappings["R"] = GLFW_KEY_R;
  m_mapKeyMappings["S"] = GLFW_KEY_S;
  m_mapKeyMappings["T"] = GLFW_KEY_T;
  m_mapKeyMappings["U"] = GLFW_KEY_U;
  m_mapKeyMappings["V"] = GLFW_KEY_V;
  m_mapKeyMappings["W"] = GLFW_KEY_W;
  m_mapKeyMappings["X"] = GLFW_KEY_X;
  m_mapKeyMappings["Y"] = GLFW_KEY_Y;
  m_mapKeyMappings["Z"] = GLFW_KEY_Z;
  m_mapKeyMappings["["] = GLFW_KEY_LEFT_BRACKET;
  m_mapKeyMappings["\\"] = GLFW_KEY_BACKSLASH;
  m_mapKeyMappings["]"] = GLFW_KEY_RIGHT_BRACKET;
  m_mapKeyMappings["`"] = GLFW_KEY_GRAVE_ACCENT;
  m_mapKeyMappings["ESCAPE"] = GLFW_KEY_ESCAPE;
  m_mapKeyMappings["RETURN"] = GLFW_KEY_ENTER;
  m_mapKeyMappings["TAB"] = GLFW_KEY_TAB;
  m_mapKeyMappings["BACKSPACE"] = GLFW_KEY_BACKSPACE;
  m_mapKeyMappings["INSERT"] = GLFW_KEY_INSERT;
  m_mapKeyMappings["DELETE"] = GLFW_KEY_DELETE;
  m_mapKeyMappings["RIGHT"] = GLFW_KEY_RIGHT;
  m_mapKeyMappings["LEFT"] = GLFW_KEY_LEFT;
  m_mapKeyMappings["DOWN"] = GLFW_KEY_DOWN;
  m_mapKeyMappings["UP"] = GLFW_KEY_UP;
  m_mapKeyMappings["PAGEUP"] = GLFW_KEY_PAGE_UP;
  m_mapKeyMappings["PAGEDOWN"] = GLFW_KEY_PAGE_DOWN;
  m_mapKeyMappings["HOME"] = GLFW_KEY_HOME;
  m_mapKeyMappings["END"] = GLFW_KEY_END;
  m_mapKeyMappings["CAPSLOCK"] = GLFW_KEY_CAPS_LOCK;
  m_mapKeyMappings["SCROLL"] = GLFW_KEY_SCROLL_LOCK;
  m_mapKeyMappings["NUMLOCK"] = GLFW_KEY_NUM_LOCK;
  m_mapKeyMappings["PRINT"] = GLFW_KEY_PRINT_SCREEN;
  m_mapKeyMappings["PAUSE"] = GLFW_KEY_PAUSE;
  m_mapKeyMappings["F1"] = GLFW_KEY_F1;
  m_mapKeyMappings["F2"] = GLFW_KEY_F2;
  m_mapKeyMappings["F3"] = GLFW_KEY_F3;
  m_mapKeyMappings["F4"] = GLFW_KEY_F4;
  m_mapKeyMappings["F5"] = GLFW_KEY_F5;
  m_mapKeyMappings["F6"] = GLFW_KEY_F6;
  m_mapKeyMappings["F7"] = GLFW_KEY_F7;
  m_mapKeyMappings["F8"] = GLFW_KEY_F8;
  m_mapKeyMappings["F9"] = GLFW_KEY_F9;
  m_mapKeyMappings["F10"] = GLFW_KEY_F10;
  m_mapKeyMappings["F11"] = GLFW_KEY_F11;
  m_mapKeyMappings["F12"] = GLFW_KEY_F12;
  m_mapKeyMappings["NP0"] = GLFW_KEY_KP_0;
  m_mapKeyMappings["NP1"] = GLFW_KEY_KP_1;
  m_mapKeyMappings["NP2"] = GLFW_KEY_KP_2;
  m_mapKeyMappings["NP3"] = GLFW_KEY_KP_3;
  m_mapKeyMappings["NP4"] = GLFW_KEY_KP_4;
  m_mapKeyMappings["NP5"] = GLFW_KEY_KP_5;
  m_mapKeyMappings["NP6"] = GLFW_KEY_KP_6;
  m_mapKeyMappings["NP7"] = GLFW_KEY_KP_7;
  m_mapKeyMappings["NP8"] = GLFW_KEY_KP_8;
  m_mapKeyMappings["NP9"] = GLFW_KEY_KP_9;
  m_mapKeyMappings["NP,"] = GLFW_KEY_KP_DECIMAL;
  m_mapKeyMappings["NP/"] = GLFW_KEY_KP_DIVIDE;
  m_mapKeyMappings["NP*"] = GLFW_KEY_KP_MULTIPLY;
  m_mapKeyMappings["NP-"] = GLFW_KEY_KP_SUBTRACT;
  m_mapKeyMappings["NP+"] = GLFW_KEY_KP_ADD;
  m_mapKeyMappings["NPENTER"] = GLFW_KEY_KP_ENTER;
  m_mapKeyMappings["LEFTSHIFT"] = GLFW_KEY_LEFT_SHIFT;
  m_mapKeyMappings["LEFTCTRL"] = GLFW_KEY_LEFT_CONTROL;
  m_mapKeyMappings["LEFTALT"] = GLFW_KEY_LEFT_ALT;
  m_mapKeyMappings["RIGHTSHIFT"] = GLFW_KEY_RIGHT_SHIFT;
  m_mapKeyMappings["RIGHTCTRL"] = GLFW_KEY_RIGHT_CONTROL;
  m_mapKeyMappings["RIGHTALT"] = GLFW_KEY_RIGHT_ALT;

  // Mouse Mappings.
  m_mapMouseMappings["MBT1"] = GLFW_MOUSE_BUTTON_1;
  m_mapMouseMappings["MBT2"] = GLFW_MOUSE_BUTTON_2;
  m_mapMouseMappings["MBT3"] = GLFW_MOUSE_BUTTON_3;
  m_mapMouseMappings["MBT4"] = GLFW_MOUSE_BUTTON_4;
  m_mapMouseMappings["MBT5"] = GLFW_MOUSE_BUTTON_5;
  m_mapMouseMappings["MBT6"] = GLFW_MOUSE_BUTTON_6;
  m_mapMouseMappings["MBT7"] = GLFW_MOUSE_BUTTON_7;
  m_mapMouseMappings["MBT8"] = GLFW_MOUSE_BUTTON_8;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/