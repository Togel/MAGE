set( RelativeDir "src" )
set( RelativeSourceGroup "SourceFiles" )

set(Directories
)

set(DirFiles
  image_DXT.c
  image_DXT.h
  image_helper.c
  image_helper.h
  SOIL.c
  SOIL.h
  stb_image_aug.c
  stb_image_aug.h
  stbi_DDS_aug.h
  stbi_DDS_aug_c.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )
	
set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()

source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "")