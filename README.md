MAGE is the second generation of my self-developed graphics engine.
The purpose of the engine is to learn and implement rendering techniques and games.
Note, that thee first generation of this engine (CAGE) has already been used to create the game "Thief's Creed".<br />
In order to build the engine, you need to create the project files using cmake.
This works not only on Windows, but also on Linux.
It should work on Mac as well, but this has not been tested, yet.
Building the project will create a library file.
This library can be included into other projects in order to create e.g. games.<br />

Features:<br />
* Custom colorized Console Output
* Configuration Loader
* Shader Management Tool
* Scenegraph
* Model Loading via Assimp Library
* Skeleton Animation
* Thread Management
* OpenGL Context Management

Planed features of next generation:<br />
* Editor
* Even more modularity without getting inefficient
