/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTimerWithUpdate.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageTimerWithUpdate::MageTimerWithUpdate()
  : MageTimer()
{
}

MageTimerWithUpdate::~MageTimerWithUpdate()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageTimerWithUpdate::_Update()
{
  // Get the current Time.
  MageClock::time_point tCurTime = MageClock::now();

  // Compute the Time Differences between the last Update and now as well as the Initialization Time and now.
  std::chrono::microseconds tMicroSecsInit  = std::chrono::duration_cast<std::chrono::microseconds>(tCurTime - m_tInitTime);
  std::chrono::microseconds tMicroSecsFrame = std::chrono::duration_cast<std::chrono::microseconds>(tCurTime - m_tLastTime);

  // Cast the calculated Times to doubles.
  m_dInitTime = static_cast<double>(tMicroSecsInit.count()) / 1000000.0;
  m_dLoopTime = static_cast<double>(tMicroSecsFrame.count()) / 1000.0;

  // Update the last Update Time.
  m_tLastTime = tCurTime;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/