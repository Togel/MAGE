set( RelativeDir "source/Core/OpenGL" )
set( RelativeSourceGroup "SourceFiles\\Core\\OpenGL" )

set(Directories

)

set(DirFiles
  _SourceFiles.cmake
  OpenGLContext.cpp
  OpenGLContext.h
  OpenGLMesh.cpp
  OpenGLMesh.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source/Core")