#pragma once
#include <string>


static const std::string TEXTURE_CAM_VS = R"(#version 330 core

// [Input Data].
layout(location = 0) in vec3 v3Vertex;
layout(location = 1) in vec3 v3Normal;
layout(location = 2) in vec2 v2UV;

// [Output Data].
out vec2 v2TexUV;

// [Uniform Data].
uniform mat4 mat4Model;
uniform mat4 mat4InvTransModel;

layout (std140) uniform Camera
{
  mat4 mat4View;
  mat4 mat4Projection;
  mat4 mat4InvTransView;
};

// [Main Program].
void main()
{
  // Output position of the vertex, in clip space : MVP * position
  gl_Position = mat4Projection * mat4View * mat4Model * vec4(v3Vertex, 1);

  // Process the UV Coordinate to the Fragment Shader.
  v2TexUV = v2UV;
})";


static const std::string TEXTURE_CAM_FS = R"(#version 330 core

// [Input Data].
in vec2 v2TexUV;

// [Output Data].
out vec3 v3Color;

// [Uniform Data].
uniform sampler2D texSampler;

// [Main Program].
void main()
{
  // Map the Texture to the Mesh.
  v3Color = texture(texSampler, v2TexUV).rgb;
})";