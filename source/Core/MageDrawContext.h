#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "GL/glew.h"
#include "glfw3.h"

#include <string>


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageDrawContext
{
public:
  /*
  * Constructor.
  */
  MageDrawContext();


  /*
  * Destructor.
  */
  virtual ~MageDrawContext();


  /*
  * Returns the GLFW Window.
  * @return Pointer to the Window.
  */
  GLFWwindow* GetWindow();


  /*
  * Sets the Sub-Title of the Window.
  * The Sub-Title is added behind the Title.
  * @param 'sSubTitle' new Sub-Title of the Window.
  */
  void SetSubTitle(std::string sSubTitle);


  /*
  * Sets the Title of the Window.
  * @param 'sTitle' new Title of the Window.
  */
  void SetTitle(std::string sTitle);


  /*
  * Sets VSync to 'on' or 'off'.
  * VSync synchronizes the Framerate to the Refreshrate of the Monitor.
  * @param 'bEnable' specifies whether VSync should be enable (true) or disabled (false).
  */
  void SetVSync(bool bEnable);


  /*
  * Sets the Size of the Window.
  * @param 'iWidth' define the Width of the Window.
  * @param 'iHeight' define the Height of the Window.
  */
  void SetWindowSize(int iWidth, int iHeight);


  /*
  * Swap the Image Buffers such that one of them can be drawn and the other can be overwritten.
  * Polling Window Events.
  */
  void UpdateWindow();


private:
  // Pointer to the GLFW Window.
  GLFWwindow* m_pWindow = nullptr;

  // Stores the Title of the GLFW Window.
  std::string m_sTitle = "";


  /*
  * Initializes the OpenGl Context.
  */
  bool Init();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/