set( RelativeDir "source/Core" )
set( RelativeSourceGroup "SourceFiles\\Core" )

set(Directories
  Timer
)

set(DirFiles
  _SourceFiles.cmake
  MageCallback.h
  MageCallback.cpp
  MageDrawContext.h
  MageDrawContext.cpp
  MageSingleton.h
  MageSystem.cpp
  MageSystem.h
  MageThread.cpp
  MageThread.h
  MageThreadDraw.cpp
  MageTHreadDraw.h
  MageThreadGeometry.cpp
  MageThreadGeometry.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")