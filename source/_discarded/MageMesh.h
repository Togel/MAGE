#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"

#include <vector>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum MeshState
{
  Free,
  Loading,
  Loaded,
  Uploading,
  Uploaded
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageMesh
{
public:
  /*
  * Constructor.
  */
  MageMesh(std::string sPath);


  /*
  * Destructor.
  */
  virtual ~MageMesh();


  /*
  * Draws the Mesh.
  */
  virtual void Draw() = 0;


  /*
  * Remove the Mesh from the Graphics Card.
  * @return true if the Mesh has been removed from the Graphics Card.
  */
  virtual bool FreeVRam() = 0;


  /*
  * Loads the Mesh an store it in the Memory.
  * Loading the Mesh will not upload it to the Graphics Card!
  * However, before uploading the Mesh it needs to be loaded.
  * @return true if the Mesh has been loaded.
  */
  bool Load();


  /*
  * Uploads the Mesh to the Graphics Card.
  * @return true if the Mesh has been uploaded to the Graphics Card.
  */
  virtual bool Upload() = 0;


protected:
  std::string m_sPath;

  std::vector<glm::vec3> m_vecVertices;
  std::vector<glm::vec3> m_vecNormals;
  std::vector<glm::vec2> m_vecUVs;

  std::vector<unsigned int> m_vecFaceIndices;
  std::vector<glm::vec3> m_vecTangents;
  std::vector<glm::vec3> m_vecBitangents;

  MeshState m_eState = MeshState::Free;

private:
  /*
  * Computes the Tangent Basis.
  * Only works for indexed Meshes.
  */
  void ComputeTangentBasis();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/