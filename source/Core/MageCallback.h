#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "Timer/MageTimer.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

/*
* Defines when the Callback is called.
* Pre-Callbacks are called right before the Main-Function of the Thread like the Draw Calls and the Geometry Updates,
* while Post-Callbacks are called afterwards.
*/
enum CallbackType
{
  PreDraw,
  PostDraw,
  PreGeometry,
  PostGeometry
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageCallback
{
public:
  /*
  * Constructor.
  */
  MageCallback(CallbackType eType);


  /*
  * Destructor.
  */
  virtual ~MageCallback();


  /*
  *Returns the Type of the Callback
  *@return Entry from the CallbackType Enum.
  */
  CallbackType GetType();


  /*
  * Overwrite this Function to define the Behavior of the Callback.
  * @param 'pTimer' Global Timer used by the Threads.
  */
  virtual void Do(MageTimer* pTimer) = 0;


protected:
  // Stores the Type of the Callback. See CallbackType Enum for more Details.
  CallbackType m_eType;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/