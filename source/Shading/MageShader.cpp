/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageShader.h"

#include "../IO/mio.h"

#include <fstream>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageShader::MageShader()
{
}


MageShader::~MageShader()
{
  for (unsigned int i = 0; i < m_vecVertexShader.size(); ++i)
    glDeleteShader(m_vecVertexShader[i]);
  for (unsigned int i = 0; i < m_vecFragmentShader.size(); ++i)
    glDeleteShader(m_vecFragmentShader[i]);
  for (unsigned int i = 0; i < m_vecTesselationControlShader.size(); ++i)
    glDeleteShader(m_vecTesselationControlShader[i]);
  for (unsigned int i = 0; i < m_vecTesselationEvaluationShader.size(); ++i)
    glDeleteShader(m_vecTesselationEvaluationShader[i]);
  for (unsigned int i = 0; i < m_vecGeometryShader.size(); ++i)
    glDeleteShader(m_vecGeometryShader[i]);
  for (unsigned int i = 0; i < m_vecComputeShader.size(); ++i)
    glDeleteShader(m_vecComputeShader[i]);

  glDeleteProgram(m_iProgram);

  m_mapUniforms.clear();
  m_vecVertexShader.clear();
  m_vecFragmentShader.clear();
  m_vecTesselationControlShader.clear();
  m_vecTesselationEvaluationShader.clear();
  m_vecGeometryShader.clear();
  m_vecComputeShader.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageShader::Bind()
{
  if (!m_bUsable)
    mio::err << "MageShader::Bind() -> Shader is not linked. Bind has no Effect." << mio::endl;
  else
    glUseProgram(m_iProgram);
}


ShaderClass MageShader::GetClass()
{
  return ShaderClass::PrimitiveShader;
}


GLuint MageShader::GetUniformLocation(const std::string& sName) const
{
  std::map<std::string, GLint>::const_iterator it = m_mapUniforms.find(sName);

  if (it != m_mapUniforms.end())
    return (*it).second;
  else
    return -1;
}


bool MageShader::InitShaderFromFile(ShaderType eType, const std::string& sShader)
{
  // Open the File
  std::fstream file(sShader.c_str(), std::ios_base::in | std::ios_base::binary);

  if (file.is_open())
  {
    // Determine File size.
    file.seekg(0, file.end);
    std::size_t stProgramLength = static_cast<std::size_t>(file.tellg());
    file.seekg(0, file.beg);

    // Store File Context.
    char* sProgramStr = new char[stProgramLength + 1];
    file.read(sProgramStr, stProgramLength);
    file.close();
    sProgramStr[stProgramLength] = '\0';

    // Create the Shader
    if (InitShaderFromString(eType, sProgramStr))
    {
      delete[] sProgramStr;
      return true;
    }
    else
    {
      delete[] sProgramStr;
      return false;
    }

  }
  else
  {
    mio::err << "MageShader::InitShaderFromFile() -> Could not open Shader File." << mio::endl;
    mio::err << sShader << mio::endl;
    return false;
  }

  return true;
}


bool MageShader::InitShaderFromString(ShaderType eType, const std::string& sShader)
{
  // Create a Shader Program if non exist.
  if (!m_iProgram)
    m_iProgram = glCreateProgram();

  // Create and store the Shader.
  GLuint uiShader = glCreateShader(eType);

  switch (eType)
  {
  case GL_VERTEX_SHADER:
    m_vecVertexShader.push_back(uiShader);
    break;
  case GL_FRAGMENT_SHADER:
    m_vecFragmentShader.push_back(uiShader);
    break;
  case GL_TESS_CONTROL_SHADER:
    m_vecTesselationControlShader.push_back(uiShader);
    break;
  case GL_TESS_EVALUATION_SHADER:
    m_vecTesselationEvaluationShader.push_back(uiShader);
    break;
  case GL_GEOMETRY_SHADER:
    m_vecGeometryShader.push_back(uiShader);
    break;
  case GL_COMPUTE_SHADER:
    m_vecComputeShader.push_back(uiShader);
  default:
    mio::err << "Unknown Shader Type." << mio::endl;
    return false;
  }

  // Compile and check the Shader.
  const char* pSrc = sShader.c_str();
  glShaderSource(uiShader, 1, &pSrc, NULL);
  glCompileShader(uiShader);

  GLint iCompileStatus = GL_FALSE;
  glGetShaderiv(uiShader, GL_COMPILE_STATUS, &iCompileStatus);
  if (iCompileStatus != GL_TRUE)
  {
    mio::err << "MageShader::InitShaderFromString() -> Failed to compile Shader Program." << mio::endl;

    int iLogLength = 0;
    glGetShaderiv(uiShader, GL_INFO_LOG_LENGTH, &iLogLength);

    if (iLogLength > 0)
    {
      char* sLog = new char[iLogLength];
      glGetShaderInfoLog(uiShader, iLogLength, NULL, sLog);

      mio::err << "MageShader::InitShaderFromString() -> Shader Compile Log: " << sLog << mio::endl;

      delete[] sLog;
    }
  }

  // Attach the Shader
  glAttachShader(m_iProgram, uiShader);

  return true;
}


bool MageShader::IsUsable()
{
  return m_bUsable;
}


bool MageShader::Link()
{
  m_bUsable = false;

  if (!m_iProgram)
  {
    mio::err << "MageShader::Link() -> Shader Program not created yet." << mio::endl;
    return false;
  }

  // Link Program.
  glLinkProgram(m_iProgram);

  GLint iLinkStatus = GL_FALSE;
  glGetProgramiv(m_iProgram, GL_LINK_STATUS, &iLinkStatus);

  // Error Checking.
  if (iLinkStatus == GL_FALSE)
  {

    GLint maxLength = 0;
    glGetProgramiv(m_iProgram, GL_INFO_LOG_LENGTH, &maxLength);

    //The maxLength includes the NULL character
    std::vector<GLchar> infoLog(maxLength);
    glGetProgramInfoLog(m_iProgram, maxLength, &maxLength, &infoLog[0]);


    std::string str;
    for (GLchar c : infoLog)
    {
      str.push_back(c);
    }

    mio::err << "Shader Error Log: " << str << mio::endl;
    return false;
  }
  else
  {
    m_bUsable = true;

    MapUniforms();

    return true;
  }
}


void MageShader::MapUniforms()
{
  // Clear outdated Results.
  m_mapUniforms.clear();

  // Find Uniforms of the Shader Program.
  int iNumOfUniforms, iMaxNameLength;

  glGetProgramiv(m_iProgram, GL_ACTIVE_UNIFORMS, &iNumOfUniforms);
  glGetProgramiv(m_iProgram, GL_ACTIVE_UNIFORM_MAX_LENGTH, &iMaxNameLength);

  if (iNumOfUniforms == 0 || iMaxNameLength == 0)
    return;

  // Put Uniforms in Map.
  GLchar* sName = new GLchar[iMaxNameLength];
  GLsizei iLength;
  GLint   iSize;
  GLenum  iType;

  for (int i = 0; i < iNumOfUniforms; ++i)
  {
    glGetActiveUniform(m_iProgram, static_cast<GLuint>(i), iMaxNameLength, &iLength, &iSize, &iType, sName);

    std::string sNameClean = "";
    std::string::size_type iArrayPos = std::string(sName).rfind('[');

    if (iArrayPos != std::string::npos)
      sNameClean = std::string(sName).substr(0, iArrayPos);
    else
      sNameClean = std::string(sName);

    const GLint iLoc = glGetUniformLocation(m_iProgram, sNameClean.c_str());
    m_mapUniforms[sNameClean] = iLoc;
  }

  delete[] sName;

  glGetError();
}


void MageShader::Release()
{
  glUseProgram(0);
}


void MageShader::SetUniform(int iLoc, const GLfloat* pValue)
{
  glUniformMatrix4fv(iLoc, 1, GL_FALSE, pValue);
}


void MageShader::SetUniform(int iLoc, int iArraySize, const GLfloat* pValue)
{
  glUniformMatrix4fv(iLoc, iArraySize, GL_FALSE, pValue);
}


void MageShader::SetUniform(int iLoc, int iUniform1)
{
  glUniform1i(iLoc, iUniform1);
}


void MageShader::SetUniform(int iLoc, float fUniform1)
{
  glUniform1f(iLoc, fUniform1);
}


void MageShader::SetUniform(int iLoc, float fUniform1, float fUniform2)
{
  glUniform2f(iLoc, fUniform1, fUniform2);
}


void MageShader::SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3)
{
  glUniform3f(iLoc, fUniform1, fUniform2, fUniform3);
}


void MageShader::SetUniform(int iLoc, float fUniform1, float fUniform2, float fUniform3, float fUniform4)
{
  glUniform4f(iLoc, fUniform1, fUniform2, fUniform3, fUniform4);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/