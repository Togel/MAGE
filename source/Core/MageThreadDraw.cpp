/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageThreadDraw.h"

#include "../IO/MageInputDeviceHandler.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
MageThreadDraw::MageThreadDraw()
  : m_pDrawContext(new MageDrawContext())
{
}


MageThreadDraw::~MageThreadDraw()
{
  delete m_pDrawContext;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageThreadDraw::_Run()
{
  // Update Keyboard and Mouse Inputs.
  MageInputDeviceHandler::GetInstance()->_Update();

  // Call Pre-Draw Callbacks.
  for (it_CB it = m_mapCallbacks.begin(); it != m_mapCallbacks.end(); ++it)
  {
    if (it->second->GetType() == CallbackType::PreDraw)
      it->second->Do(m_pThreadTimer);
  }

  // Clear the Frame-Buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Draw the Scene.
  if (m_pScene)
    m_pScene->Draw();

  // Swap Buffers and poll Events.
  if(m_pDrawContext)
    m_pDrawContext->UpdateWindow();

  // Call Post-Draw Callbacks.
  for (it_CB it = m_mapCallbacks.begin(); it != m_mapCallbacks.end(); ++it)
  {
    if (it->second->GetType() == CallbackType::PostDraw)
      it->second->Do(m_pThreadTimer);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/