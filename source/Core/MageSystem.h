#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSingleton.h"
#include "MageCallback.h"
#include "MageThreadDraw.h"
#include "MageThreadGeometry.h"

#include "./Timer/MageTimerWithUpdate.h"

#include <vector>
#include <map>
#include <thread>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageSystem : public MageSingleton<MageSystem>
{
  friend class MageSingleton<MageSystem>;

public:
  /*
  * Destructor.
  */
  ~MageSystem();


  /*
  * Returns a Pointer to the Draw Thread.
  * @return Pointer to the Draw Thread.
  */
  MageThreadDraw* GetDrawThread();


  /*
  * Returns a Pointer to the Geometry Thread.
  * @return Pointer to the Geometry Thread.
  */
  MageThreadGeometry* GetGeometryThread();


  /*
  * Adds another Thread and adds it to the System.
  * Note! This Thread should perform periodic Tasks each Frame.
  * If you want to calculate something using multiple Threads create them by Hand and not by this Function.
  * @param 'pThread' Pointer to the new Thread. Should be a Sub-Class of the MageThread Class.
  */
  //void AddThread(MageThread* pThread);
  // TODO: Adds this Functionality.


  /*
  * Kick off the System.
  */
  void Run();


  /*
  * Switches to a new Scene.
  * All Threads will change the Scene.
  * @param 'pScene' Pointer to the new Scene.
  */
  void SwitchScene(MageScene* pScene);


  /*
  * Stop the Drawing Process and shuts down the System.
  */
  void Terminate();


private:
  // Stores all generated Threads (not including Main-Thread).
  std::vector<std::thread> m_vecThreads; 

  // Owns OpenGL Context and draws Scenery.
  MageThreadDraw*     m_pDrawThread = nullptr; 
  // Updates Geometry.
  MageThreadGeometry* m_pGeoThread  = nullptr;  

  // Stores whether the System is running or not.
  bool m_bRun = false; 


protected:
  /*
  * Constructor.
  */
  MageSystem();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/