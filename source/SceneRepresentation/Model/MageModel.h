#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageVAO.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum ModelType
{
  Colorized,
  Textured,
  Animated
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageModel
{
public:
  /*
  * Constructor.
  */
  MageModel(MageVAO* pVAO);


  /*
  * Destructor.
  */
  virtual ~MageModel();


  /*
  * Binds the Vertex Buffer Objects and draws its Contents.
  * @return true if the Drawing Process has been successful.
  */
  virtual bool Draw();


  /*
  * Returns the Type of the Model.
  * @return Enum Entry defining the Type of the Model.
  */
  virtual ModelType GetType();


  /*
  * Uploads the contained Vertex Buffer Objects.
  * @param 'eUsage' contains Performance Hints. See 'glBufferData' Documentation for more Information. Common Choices are GL_STATIC_DRAW and GL_DYNAMIC_DRAW.
  * @return 'true' if the Data has been uploaded successfully. 'false' otherwise.
  */
  bool Upload(GLenum eUsage);


protected:
  // Stores the VAO of the Model, containing all Information needed to draw the Model.
  MageVAO* m_pVAO = nullptr;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/