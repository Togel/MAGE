set( RelativeDir "source/Core/Timer" )
set( RelativeSourceGroup "SourceFiles\\Core\\Timer" )

set(Directories

)

set(DirFiles
  _SourceFiles.cmake
  MageTimer.h
  MageTimer.cpp
  MageTimerWithUpdate.h
  MageTimerWithUpdate.cpp
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source/Core")