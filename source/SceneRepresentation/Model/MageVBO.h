#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"

#include "GL/glew.h"

#include <vector>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum VboType
{
  VertexBuffer,
  NormalBuffer,
  UvBuffer,
  ColorBuffer,
  FaceIndexBuffer,
  TangentBuffer,
  BiTangentBuffer,
  JointIndexBuffer,
  JointWeightBuffer
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageVBO
{
public:
  /*
  * Constructor.
  */
  MageVBO(GLenum eType);


  /*
  * Destructor.
  */
  virtual ~MageVBO();


  /*
  * Returns the unique Vertex Buffer ID.
  * @return The Buffer ID:
  */
  GLuint GetBufferID();


  /*
  * Uploads the Mesh Data to the Graphics Card.
  * @param 'eUsage' contains Performance Hints. See 'glBufferData' Documentation for more Information. Common Choices are GL_STATIC_DRAW and GL_DYNAMIC_DRAW.
  * @param 'vecData' Vector storing the Data which should be uploaded.
  */
  void Upload(GLenum eUsage, std::vector<float>& vecData);
  void Upload(GLenum eUsage, std::vector<unsigned int>& vecData);
  void Upload(GLenum eUsage, std::vector<glm::vec2>& vecData);
  void Upload(GLenum eUsage, std::vector<glm::vec3>& vecData);
  void Upload(GLenum eUsage, std::vector<glm::vec4>& vecData);
  void Upload(GLenum eUsage, std::vector<glm::ivec4>& vecData);


private:
  // Stores the Unique Buffer ID of the VBO.
  GLuint m_iBufferID = 0;

  // Stores the Type of the VBO.
  // See 'glBindBuffer' 'target'-Documentation for more Information.
  GLenum m_eType;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/