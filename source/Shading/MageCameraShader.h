#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageShader.h"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageCameraShader : public MageShader
{
public:
  /*
  * Constructor.
  */
  MageCameraShader(GLuint iCameraUniformBufferID);


  /*
  * Destructor.
  */
  ~MageCameraShader();


  /*
  * Returns the Class of the Shader.
  * @return Enumeration Entry defining the Class of the Shader.
  */
  ShaderClass GetClass() override;


protected:
  // Stores the Handle to the Camera Uniform Buffer.
  GLuint m_iCameraUniformBufferID = 0;

  // Stores the Index Position of the View Matrix inside the Camera Uniform Buffer.
  GLuint m_iMatViewIndex = 0;
  // Stores the Offset of the View Matrix inside the Camera Uniform Buffer.
  GLint  m_iMatViewOffset = 0;
  // Stores the Index Position of the Projection Matrix inside the Camera Uniform Buffer.
  GLuint m_iMatProjectionIndex = 0;
  // Stores the Offset of the Projection Matrix inside the Camera Uniform Buffer.
  GLint  m_iMatProjectionOffset = 0;
  // Stores the Index Position of the inverse transposed View Matrix inside the Camera Uniform Buffer. Needed for Normals.
  GLuint m_iMatInvTransViewIndex = 0;
  // Stores the Offset of the inverse transposed View Matrix inside the Camera Uniform Buffer.
  GLint  m_iMatIntTransViewOffset = 0;


  /*
  * Creates the Mapping between the Uniform Names and their Handles.
  */
  void MapUniforms() override;

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/