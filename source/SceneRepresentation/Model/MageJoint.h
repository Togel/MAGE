#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "glm.hpp"

#include <string>
#include <vector>
#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageJoint
{
public:
  /*
  * Constructor.
  */
  MageJoint(int iIndex, std::string sName, glm::mat4 m4LocalBindTransform);


  /*
  * Destructor.
  */
  virtual ~MageJoint();


  /*
  * Adds another Joint as a Child of this Joint to the Hierarchy.
  * @param 'pChild' defines the new Child which is added.
  */
  void AddChild(MageJoint* pChild);


  /*
  * The Binding Transformations of the Joint and its Parents get combined into a new Transformation Matrix.
  * This combined Transformation Matrix gets inverted and is required to calculate the current Transformation.
  * Function is called after the Joint Hierarchy has been fully set up.
  * @param 'm4ParentTransformation' Stores the combined Transformation from the Parents.
  */
  void CalculateInverseBindTransform(glm::mat4& m4ParentTransformation);


  /*
  * Returns a Vector storing Pointers to the Children-Joints.
  * @return The Children of this Joint.
  */
  std::vector<MageJoint*> GetChildren();


  /*
  * Returns the current Transformation of the Joint.
  * @return A Matrix storing the current Transformation of the Joint.
  */
  glm::mat4 GetCurrentTransformation();


  /*
  * Returns the unique Identification Number of the Joint.
  * @return The ID of the Joint.
  */
  int GetID();


  /*
  * The Binding Transformations of the Joint and its Parents get combined into a new Transformation Matrix.
  * The inverse of this Matrix gets returned.
  * Is used to calculate the current Transformation of the Joint.
  * @return The combined inverse Matrix of the Joint's and Parent's Binding Transformations.
  */
  glm::mat4 GetInverseBindTransform();


  /*
  * Returns the Name of the Joint
  * @return Name of the Joint.
  */
  std::string GetName();


  /*
  * Returns a Map with the Joint Indices of the Joint itself and its Children as Keys and Pointers to them as Values.
  * @return Map (<Index, Joint>) storing the Joint and its Children.
  */
  void GetSortedJoints(std::map<unsigned int, MageJoint*>& mapSkeleton);


  /*
  * Resets the Joint to its Binding Transformation
  */
  void ResetToBindingTransformation();


  /*
  * Redefines the current Transformation of the Joint.
  * This is used to realize the Animation.
  * @param 'm4CurrentTransform' Matrix storing the new Transformation of the Joint.
  */
  void SetCurrentTransformation(glm::mat4& m4CurrentTransform);


private:
  // Unique Index of the Joint inside a Skeleton.
  int m_iIndex = 0;

  // Name of the Joint.
  std::string m_sName;

  // Stores Hierarchy (Children).
  std::vector<MageJoint*> m_vecChildren;

  // The Transformation of the Joint changes as an Animation is applied.
  // Defines the Transformation from the Binding Pose to the current Pose.
  glm::mat4 m_m4CurrentTransform;

  // The local Transformation of the Joint.
  // Defines the Transformation from the Parent Joint.
  // No Animation applied (Default Binding Transformation).
  glm::mat4 m_m4LocalBindTransform; 

  // The Binding Transformations of the Joint and its Parents get combined into a new Transformation Matrix.
  // The combined Transformation Matrix gets inverted and stored in this Member.
  glm::mat4 m_m4InverseBindTransform;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/