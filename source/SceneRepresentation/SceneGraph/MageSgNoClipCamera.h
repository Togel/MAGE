#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgTransformNode.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageSgNoClipCamera : public MageSgTransformNode
{
public:
  /*
  * Constructor.
  */
  MageSgNoClipCamera();


  /*
  * Constructor.
  */
  virtual ~MageSgNoClipCamera();


  /*
  * Updates the Position of the Camera according to the Keyboard and Mouse Input.
  * @param 'pTimer' Pointer to Geometry Thread Timer.
  */
  void Update(MageTimer* pTimer) override;


private:
  // Stores the Viewing Direction of the Camera.
  glm::vec3 m_v3ViewDirection = glm::vec3(0.0f, 0.0f, -1.0f);

  // Stores the maximal Translation Speed.
  const float m_fTranslationSpeed = 0.01f;
  // Stores the maximal Rotation Speed.
  const float m_fRotationSpeed    = 0.5f;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/