/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgRenderNode.h"

#include "../Model/MageAnimatedModel.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSgRenderNode::MageSgRenderNode(MageShader* pShader, MageModel* pModel)
  : m_pShader(pShader)
  , m_pModel(pModel)
{
}


MageSgRenderNode::~MageSgRenderNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageSgRenderNode::Draw(glm::mat4 mat4Transformation)
{
  if (m_pShader)
  {
    // Bind the Shader.
    m_pShader->Bind();

    // Upload Uniforms.
    InitCustomUniforms();

    GLuint iAlpha = m_pShader->GetUniformLocation("fAlpha");
    m_pShader->SetUniform(iAlpha, m_fAlpha);

    if (m_pShader->GetClass() == ShaderClass::CameraShader) // If Camera Shader
    {
      GLuint iModelMatrix = m_pShader->GetUniformLocation("mat4Model");
      m_pShader->SetUniform(iModelMatrix, &mat4Transformation[0][0]);

      glm::mat4 matInvTransModel = glm::transpose(glm::inverse(mat4Transformation));

      GLuint iInvTransModel = m_pShader->GetUniformLocation("mat4InvTransModel");
      m_pShader->SetUniform(iInvTransModel, &matInvTransModel[0][0]);
    }

    if (m_pModel->GetType() == ModelType::Animated)
    {
      glm::mat4 amat4JointTransforms[50];
      dynamic_cast<MageAnimatedModel*>(m_pModel)->GetJointTransformations(amat4JointTransforms);
      GLuint iJointTransforms = m_pShader->GetUniformLocation("mat4JointTransforms");
      m_pShader->SetUniform(iJointTransforms, 50, &amat4JointTransforms[0][0][0]);
    }

    // Bind Textures.
    if (m_pModel->GetType() == ModelType::Textured || m_pModel->GetType() == ModelType::Animated)
    {
      dynamic_cast<MageTexturedModel*>(m_pModel)->BindTextures();
      m_pShader->SetUniform(m_pShader->GetUniformLocation("texSampler"), 0); // 0 is the Texture Unit. Maybe change this when handling multiple Textures.
    }

    // Draw Geometry.
    if(m_pModel)
      m_pModel->Draw();

    // Release Shader.
    m_pShader->Release();
  }
}


NodeType MageSgRenderNode::GetType()
{
  return NodeType::Render;
}


bool MageSgRenderNode::IsTransparent()
{
  if (m_fAlpha < 1.0f)
    return true;

  return false;
}


void MageSgRenderNode::SetShader(MageShader* pShader)
{
  m_pShader = pShader;
}


void MageSgRenderNode::SetAlpha(float fAlpha)
{
  m_fAlpha = fAlpha;
}


void MageSgRenderNode::Update(MageTimer* pTimer)
{
  // Update if the Model is animated.
  if (m_pModel->GetType() == ModelType::Animated)
    dynamic_cast<MageAnimatedModel*>(m_pModel)->_Update(pTimer);
}


void MageSgRenderNode::InitCustomUniforms()
{
  // Implement in Sub-Classes.
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/