#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../../Core/Timer/MageTimer.h"

#include <vector>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum NodeType
{
  Root,
  Node,
  Transform,
  Render,
  Camera
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageSgNode
{
public:
  /*
  * Constructor.
  */
  MageSgNode();


  /*
  * Destructor.
  */
  virtual ~MageSgNode();


  /*
  * Adds an additional Child Node.
  * @param 'pChild' defines the new Child Node.
  */
  void AddChild(MageSgNode* pChild);


  /*
  * Returns a Vector storing all Child Nodes.
  * @return pointer to the vector of child-nodes
  */
  const std::vector<MageSgNode*>& GetChildren();


  /*
  * Returns the Number of Children.
  * @return the Number of Children.
  */
  unsigned int GetNumOfChildren();


  /*
  * Returns the Parent Node.
  * @return Shared Pointer to the Parent Node.
  */
  MageSgNode* GetParent();


  /*
  * Returns an individual Enumeration Entry defining the Type of the Node.
  * See 'NodeType' Enumeration for more Detail.
  * @return Enumeration Entry defining the Type of the Node.
  */
  virtual NodeType GetType();


  /*
  * Returns whether the Node is processed during the Traversal or not.
  * @return 'true' if the Node is processed during the Traversal.
  */
  bool IsActive();


  /*
  * Removes a specific Child Node.
  * @param 'pChild' defines the new Child Node.
  * @return 'true' if the Node could be deleted.
  */
  bool RemoveChild(MageSgNode* pChild);


  /*
  * Toggles the Activity of the Node.
  * @param 'bActive' Defines whether the Node is processed during the Traversal (true) or not (false).
  */
  void SetActive(bool bActive);


  /*
  * Performs a custom Update.
  * Needs to be implemented in Sub-Classes.
  * @param 'pTimer' Pointer to Geometry Thread Timer.
  */
  virtual void Update(MageTimer* pTimer);


private:
  // Pointer to the Parent Node.
  MageSgNode* m_pParent = nullptr;

  // Vector storing Pointers to the Children.
  std::vector<MageSgNode*> m_vecChildren;

  // Stores whether the not is processed during the Traversal or not.
  bool m_bActive = true;


  /*
  * Resets the Parent Node.
  * This should only be done implicit by setting it as a child
  * @param Shared Pointer to the new Parent Node.
  */
  void SetParent(MageSgNode* pParent);

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/