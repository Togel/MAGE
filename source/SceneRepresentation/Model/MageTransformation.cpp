/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTransformation.h"

#include "gtc/matrix_transform.hpp"
#include "gtx/compatibility.hpp"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageTransformation::MageTransformation(glm::vec3 v3Translation, glm::quat qRotation, glm::vec3 v3Scaling)
  : m_v3Translation(v3Translation)
  , m_qRotation(qRotation)
  , m_v3Scaling(v3Scaling)
{
}


MageTransformation::~MageTransformation()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

glm::quat MageTransformation::GetRotation()
{
  return m_qRotation;
}


glm::vec3 MageTransformation::GetScaling()
{
  return m_v3Scaling;
}


glm::mat4x4 MageTransformation::GetTransformation()
{
  // Apply first the Scaling and then the Rotation and Translation to a Identity-Matrix.
  return glm::translate(glm::mat4(1.0f), m_v3Translation) * glm::toMat4(m_qRotation) * glm::scale(glm::mat4(1.0f), m_v3Scaling);
}


glm::vec3 MageTransformation::GetTranslation()
{
  return m_v3Translation;
}


MageTransformation* MageTransformation::Interpolate(MageTransformation* pOtherTransform, double dProcess)
{
  // Error Prevention. Should usually never be needed.
  if (dProcess < 0.0f)
    dProcess = 0.0f;
  else if (dProcess > 1.0f)
    dProcess = 1.0f;

  // Interpolate Translation Vectors.
  glm::vec3 v3Pos = glm::lerp(this->m_v3Translation, pOtherTransform->GetTranslation(), static_cast<float>(dProcess));

  // Interpolate Rotations.
  glm::quat qRot = glm::fastMix(this->m_qRotation, pOtherTransform->GetRotation(), static_cast<float>(dProcess));

  // Interpolate Scalings.
  glm::vec3 v3Scale = glm::lerp(this->m_v3Scaling, pOtherTransform->GetTranslation(), static_cast<float>(dProcess));

  // Create the new Joint Transformation.
  return new MageTransformation(v3Pos, qRot, v3Scale);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/