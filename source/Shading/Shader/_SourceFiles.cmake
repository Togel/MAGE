set( RelativeDir "source/Shading/Shader" )
set( RelativeSourceGroup "SourceFiles\\Shading\\Shader" )

set(Directories

)

set(DirFiles
  AnimationShader.h
  NormalAsColorShader.h
  TextureShader.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source/Shading")