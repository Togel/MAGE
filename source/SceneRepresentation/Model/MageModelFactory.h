#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageModel.h"
#include "MageTexturedModel.h"
#include "MageAnimatedModel.h"
#include "MageAnimation.h"

#include "../../Core/MageSingleton.h"

#include "assimp/scene.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageModelFactory : public MageSingleton<MageModelFactory>
{
  friend class MageSingleton<MageModelFactory>;

public:
  /*
  * Destructor.
  */
  ~MageModelFactory();


  /*
  * Loads one or multiple Models from a File.
  * @param 'sPath' defines the Path to the Model File.
  * @return A Vector storing the loaded Models.
  */
  std::vector<MageModel*> LoadModel(std::string sPath);


private:
  /*
  * Extracts all Animations of the Scene and stores them in a Vector.
  * @param 'pScene' defines the Scene storing the Materials.
  * @param 'vecAnimations' Vector in which the Animations get stored.
  */
  void GetAnimations(const aiScene* pScene, std::vector<MageAnimation>& vecAnimations);


  /*
  * Extracts the Vertex Indices and Weights from an Input Map storing these Information.
  * @param 'mapVertexWeights' Input Map (<Vertex Index, <Joint Index, Weight>>) from which the Data is extracted.
  * @param 'vecIndices' Vector in which the Joint Indices get stored.
  * @param 'vecWeights' Vector in which the Joint Weights get stored.
  */
  void GetJointIndicesAndWeights(std::map<unsigned int, std::map<unsigned int, float>>& mapVertexWeights, std::vector<glm::ivec4>& vecIndices, std::vector<glm::vec4>& vecWeights);


  /*
  * Extracts the Keyframes from th Assimp Scene.
  * @param 'pNodeAnimation' defines the Root Node of the Animation.
  * @return A Map storing the Timestamps and Transformations for each Keyframe of the Animation.
  */
  std::map<double, MageTransformation*> GetKeyframes(aiNodeAnim* pNodeAnimation);


  /*
  * Extracts all Materials of the Scene and stores them in a Map
  * @param 'sPath' defines the Path of the File from which the Models and Materials are loaded.
  * @param 'pScene' defines the Scene storing the Materials.
  * @param 'mapMaterials' Map (<ID, Material>) in which extracted Materials get stored.
  */
  void GetMaterials(std::string sPath, const aiScene* pScene, std::map<unsigned int, MageTexture*>& mapMaterials);


  /*
  * Extracts the Mesh Information from the Mesh and stores them in Vectors.
  * @param 'pAiMesh' defines the Mesh from which the Mesh Information should be extracted.
  * @param 'vecVertices' Vector in which the Vertices get stored.
  * @param 'vecNormals' Vector in which the Normals get stored.
  * @param 'vecColors' Vector in which the Colors get stored.
  * @param 'vecUVs' Vector in which the UV Coordinates get stored.
  * @param 'vecFaceIndices' Vector in which the Face Indices get stored.
  * @param 'vecTangents' Vector in which the Tangents get stored.
  * @param 'vecBiTangents' Vector in which the Bi-Tangents get stored.
  */
  void GetMeshInformation(aiMesh* pAiMesh, 
    std::vector<glm::vec3>& vecVertices,
    std::vector<glm::vec3>& vecNormals,
    std::vector<glm::vec3>& vecColors,
    std::vector<glm::vec2>& vecUVs,
    std::vector<unsigned int>& vecFaceIndices,
    std::vector<glm::vec3>& vecTangents,
    std::vector<glm::vec3>& vecBiTangents);


  /*
  * Extracts a Skeleton from the Assimp Scene.
  * @param 'pNode' defines a Node which is checked to be a Skeleton Joint.
  * @param 'pParentJoint' defines the Node which is the Parent of the next inserted Joint.
  * @param 'vecJoints' contains all Joints. Gets cleared in the Process.
  */
  void GetSkeleton(aiNode* pNode, MageJoint* pParentJoint, std::vector<MageJoint*>& vecJoints);


  /*
  * Transforms a Vector of Joints into a Skeleton Hierarchy.
  * @param 'pNode' defines the Root Node of the Assimp Scene.
  * @param 'vecJoints' defines the Vector storing the Joints.
  */
  MageJoint* GetSkeletonHierarchy(aiNode* pNode, std::vector<MageJoint*>& vecJoints);


  /*
  * Extracts the Skeleton Information of a Mesh.
  * @param 'pAiMesh' defines the Mesh from which the Skeleton Information should be extracted.
  * @param 'vecJoints' Vector in which the Joints get stored.
  * @param 'mapVertexWeights' Map (<Vertex Index, <Joint Index, Weight>>) storing for each Vertex a Combination of a Joint and a Weight.
  */
  void GetSkeletonInformation(aiMesh* pAiMesh, std::vector<MageJoint*>& vecJoints, std::map<unsigned int, std::map<unsigned int, float>>& mapVertexWeights);


  /*
  * Gets the Root Node of the Skeleton within the Assimp Scene.
  * @param 'pNode' defines the Root Node of the Assimp Scene.
  * @param 'vecJoints' defines the Vector storing the Joints.
  * @return Pointer to the Assimp Node which is the Root Node of the Skeleton.
  */
  aiNode* GetSkeletonRoot(aiNode* pNode, std::vector<MageJoint*>& vecJoints);


protected:
  /*
  * Constructor.
  */
  MageModelFactory();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/