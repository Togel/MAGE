#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
template <class T>
class MageSingleton
{
public:
  /*
  * De-Constructor.
  */
  virtual ~MageSingleton()
  {
    m_pInstance = 0;
  }


  /*
  * Returns the Instance of the Singleton.
  * @return Pointer to the Singleton.
  */
  static T* GetInstance()
  {
    if (!m_pInstance)
      m_pInstance = new T();

    return m_pInstance;
  }


private:
  // Pointer to the Singleton Instance.
  static T* m_pInstance;


protected:
  /*
  * Constructor.
  */
  MageSingleton()
  { 
  }

};

template <class T> T* MageSingleton <T>::m_pInstance = 0;

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/