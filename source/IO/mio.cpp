/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "mio.h"

#include <algorithm>
#include <sstream>
#include <fstream>

#ifdef WIN32
#include <windows.h>
#endif

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/


/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

namespace mio
{
  // [Implementation of internal Functions].
  namespace internals
  {
    void SetConsoleColor(ConsoleColor eColor)
    {
#ifdef WIN32
      // Windows
      HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
      SetConsoleTextAttribute(hConsole, eColor.m_iWinColor);
#else
      // Linux
      std::cout << eColor.m_sLinColor;
#endif
    }


    void LogMessage(char* sPath, char* sMessage)
    {
      // Try to open the Log-File.
      FILE* pFile = fopen(sPath, "w");

      // Store Message if File could be opened.
      if (pFile != NULL)
      {
        fprintf(pFile, "%s\n", sMessage);
        fflush(pFile); // Flush the Keyboard-Buffer.
        fclose(pFile);
      }
    }
  }


  // [Implementation of Configuration].
  bool AddConfiguration(std::string sKey, std::string sValue)
  {
    if (internals::m_mapConfig.insert(std::make_pair(sKey, sValue)).second)
      return true;

    return false;
  }


  bool GetConfiguration(std::string sKey, std::string& sValue)
  {
    if (internals::m_mapConfig.find(sKey) == internals::m_mapConfig.end())
    {
      mio::err << "mio::GetConfiguration() -> There is no Configuration for the Key (" << sKey << ")." << mio::endl;
      return false;
    }

    sValue = internals::m_mapConfig[sKey];
    return true;
  }


  bool LoadConfigFile(std::string sPath)
  {
    // Open the Config File.
    std::ifstream fFile;
    fFile.open(sPath);

    if (!fFile)
    {
      mio::err << "mio::LoadConfigFile() -> Could not find File " << sPath << "." << mio::endl;
      return false;
    }

    // Read Line by Line.
    std::string sLine;
    unsigned int iLineNr = 0;

    while (std::getline(fFile, sLine))
    {
      // Store untouched Line 'sLine' in new Variable which can be processed.
      std::string sLineEdit = sLine;

      // Skip empty Lines.
      if (sLineEdit.empty())
        continue;

      // Remove Comments indicated by '#' or ';'.
      if (sLineEdit.find('#') != sLineEdit.npos)
        sLineEdit.erase(sLineEdit.find('#'));

      if (sLineEdit.find(';') != sLineEdit.npos)
        sLineEdit.erase(sLineEdit.find(';'));

      // Skip if there are only White-Spaces.
      if (sLineEdit.find_first_not_of(' ' == sLineEdit.npos))
        continue;

      // Skip the Line if there is no Separator '='.
      if (sLineEdit.find('=') == sLineEdit.npos)
        continue;

      // Check whether the Line is well formated. There needs to be some Key and some Value.
      bool bWellFormedKey = false;
      bool bWellFormedValue = false;

      // Key Side.
      for (size_t iPos = 0; iPos < sLineEdit.find('='); iPos++)
        if (sLineEdit[iPos] != ' ')
          bWellFormedKey = true;

      // Value Side.
      for (size_t iPos = sLineEdit.find('=') + 1; iPos < sLineEdit.length(); iPos++)
        if (sLineEdit[iPos] != ' ')
          bWellFormedValue = true;

      if (!bWellFormedKey || !bWellFormedValue)
      {
        mio::err << "mio::LoadConfigFile() -> Configuration File " << sPath << " is broken. The following Line is not well formated ('Key' = 'Value'):" << mio::endl << sLineEdit << mio::endl;
        return false;
      }

      // Parse the Line.
      size_t iSeparatorPos = sLineEdit.find("=");
      std::string sKey;
      std::string sValue;

      sKey = sLineEdit.substr(0, iSeparatorPos);
      sValue = sLineEdit.substr(iSeparatorPos + 1);

      if (!AddConfiguration(sKey, sValue))
      {
        mio::err << "mio::LoadConfigFile() -> Configuration File " << sPath << " tries to override already existing Key " << sKey << "." << mio::endl;
        return false;
      }

      // Jump to next Line.
      iLineNr++;
    }

    // Close the File
    fFile.close();
    return true;
  }


  // [Implementation of Console Output].
  internals::IOInfo    sInfo;
  internals::IOWarning sWarning;
  internals::IOError   sError;
  internals::IOCrash   sCrash;


  internals::IOInfo inf
  {
    return sInfo;
  };
  

  internals::IOWarning war
  {
    return (sWarning << "Warning: ");
  };
  

  internals::IOError err
  {
    return (sError << "Error: ");
  };


  internals::IOCrash crs
  {
    return (sCrash << "Crash: ");
  };
}


/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/