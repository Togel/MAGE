#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageKeyFrame.h"

#include <vector>
#include <algorithm>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

struct sortKeyframes
{
  inline bool operator() (const MageKeyframe& sKey1, const MageKeyframe& sKey2)
  {
    return (sKey1.m_dTimeStamp < sKey2.m_dTimeStamp);
  }
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

struct MageAnimation
{
  /*
  * Constructor.
  */
  MageAnimation(std::string sName, std::vector<MageKeyframe> vecKeyframes)
  {
    m_sName = sName;

    m_vecKeyframes = vecKeyframes;

    // Sort the Timestamp to avoid Errors. Avoids interpolating the Keyframe in wrong Order.
    std::sort(m_vecKeyframes.begin(), m_vecKeyframes.end(), sortKeyframes());

    // The Animation ends exactly with the last Keyframe. 
    if (m_vecKeyframes.size() > 0)
      m_dDuration = m_vecKeyframes.back().m_dTimeStamp;
  }

  // The Duration of the Animation.
  double m_dDuration = 0.0;

  // The Name of the Animation.
  std::string m_sName = "";

  // The Keyframes of the Animation.
  std::vector<MageKeyframe> m_vecKeyframes;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/