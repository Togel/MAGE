#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../../SceneRepresentation/MageTexture.h"

#include "GL/glew.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class OpenGlTexture : public MageTexture
{
public:
  /*
  * Constructor.
  */
  OpenGlTexture(std::string sPath, TextureType eType);


  /*
  * Destructor.
  */
  virtual ~OpenGlTexture();


  /*
  * Bind the Texture.
  */
  void Bind() override;


  /*
  * Remove the Texture from the Graphics Card.
  * @return true if the Texture has been removed from the Graphics Card.
  */
  bool FreeVRam() override;


  /*
  * Uploads the Texture to the Graphics Card.
  * @return true if the Texture has been uploaded successfully.
  */
  bool Upload() override;


protected:
  GLuint m_iID = 0;
  GLenum m_eType = GL_TEXTURE_2D;
  GLenum m_eUnit = GL_TEXTURE0;


  /*
  * Uploads a DDS-Texture to the Graphics Card.
  */
  void UploadDDS();
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/