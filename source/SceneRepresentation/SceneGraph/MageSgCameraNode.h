#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgNode.h"

#include "GL/glew.h"
#include "glm.hpp"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageSgCameraNode : public MageSgNode
{
public:
  /*
  * Constructor.
  */
  MageSgCameraNode();


  /*
  * Destructor.
  */
  virtual ~MageSgCameraNode();


  /*
  * Fill the Camera Uniform Buffer with the Projection and View Matrix.
  */
  void FillBuffer();


  /*
  * Returns an individual Enumeration Entry defining the Type of the Node.
  * See 'NodeType' Enumeration for more Detail.
  * @return Enumeration Entry defining the Type of the Node.
  */
  NodeType GetType() override;


  /*
  * Returns the Uniform Buffer ID.
  * @return The Uniform Buffer ID.
  */
  GLuint GetUniformBufferID();


  /*
  * Redefines the Projection Matrix.
  * @param 'fFov' defines the Field Of View.
  * @param 'fAspect' defines the Aspect Ratio.
  * @param 'fNearPlane' defines the Near Plane.
  * @param 'fFarPlane' defines the Far Plane.
  */
  void SetProjective(float fFov, float fAspect, float fNearPlane, float fFarPlane);


private:
  // Stores the Handle to the Camera Uniform Buffer.
  GLuint m_iUniformBufferID = 0;

  // Stores the Camera Transformation.
  glm::mat4 m_mat4CamTransform = glm::mat4(0.0f);
  // Stores the Projection Matrix.
  glm::mat4 m_mat4Projection   = glm::mat4(0.0f);


  /*
  * Returns the View Matrix.
  * The View Matrix is constructed by combining the Transformations of all Parent Nodes.
  * @return The View Matrix.
  */
  glm::mat4 GetViewMatrix();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/