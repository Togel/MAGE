/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTimerFactory.h"
#include "MageTimer.h"
#include "MageTimerWithUpdate.h"

#include "../MageSystem.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/
MageTimerFactory::MageTimerFactory()
{
}

MageTimerFactory::~MageTimerFactory()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

TimerPtr MageTimerFactory::CreateTimer()
{
  // Create a new Timer with full Functionality.
  FullTimerPtr pFullTimer = FullTimerPtr(new MageTimerWithUpdate());

  // Put the Timer in the System which in turn calls the Callback Functions
  MageSystem::GetSingleton()->AddTimer(pFullTimer);

  // Return the reduced Type such that the User can not use Callback Functions by Mistake.
  return std::dynamic_pointer_cast<MageTimer>(pFullTimer);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/