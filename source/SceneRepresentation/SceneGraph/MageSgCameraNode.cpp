/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgCameraNode.h"
#include "MageSgTransformNode.h"

#include "gtc/matrix_transform.hpp"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSgCameraNode::MageSgCameraNode()
{
  glGenBuffers(1, &m_iUniformBufferID);
}


MageSgCameraNode::~MageSgCameraNode()
{
  glDeleteBuffers(1, &m_iUniformBufferID);
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageSgCameraNode::FillBuffer()
{
  glm::mat4 mat4ViewMatrix = GetViewMatrix();
  glm::mat4 mat4InverseTransposedViewMatrix = glm::transpose(glm::inverse(mat4ViewMatrix));

  GLfloat aBuffer[48];

  memcpy(aBuffer, &mat4ViewMatrix[0][0], 16 * sizeof(float));
  memcpy(aBuffer + 16, &m_mat4Projection[0][0], 16 * sizeof(float));
  memcpy(aBuffer + 32, &mat4InverseTransposedViewMatrix[0][0], 16 * sizeof(float));

  glBindBuffer(GL_UNIFORM_BUFFER, m_iUniformBufferID);
  glBufferData(GL_UNIFORM_BUFFER, 48 * sizeof(float), aBuffer, GL_DYNAMIC_DRAW);
}


NodeType MageSgCameraNode::GetType()
{
  return NodeType::Camera;
}


GLuint MageSgCameraNode::GetUniformBufferID()
{
  return m_iUniformBufferID;
}


void MageSgCameraNode::SetProjective(float fFov, float fAspect, float fNearPlane, float fFarPlane)
{
  m_mat4Projection = glm::perspective(fFov, fAspect, fNearPlane, fFarPlane);
}


glm::mat4 MageSgCameraNode::GetViewMatrix()
{
  MageSgNode* pParent = GetParent();
  glm::mat4 mat4ModelMatrix = glm::mat4();

  // The Model Matrix of the Camera is the Conjunction of all Parent Transformations starting at the direct Parent.
  while (pParent)
  {
    if (pParent->GetType() == NodeType::Transform)
    {
      MageSgTransformNode* pTransParent = dynamic_cast<MageSgTransformNode*>(pParent);
      mat4ModelMatrix = mat4ModelMatrix * pTransParent->GetTransformation();
    }
    pParent = pParent->GetParent();
  }

  // Calculate Vectors needed for the Generation of the View Matrix (Translation Vector, View Vector, Up Vector).
  glm::vec4 v4Translation = mat4ModelMatrix[3];
  glm::vec4 v4ViewDir     = mat4ModelMatrix * glm::vec4(0.0, 0.0, -1.0, 0.0);
  glm::vec4 v4UpDir       = mat4ModelMatrix * glm::vec4(0.0, 1.0, 0.0, 0.0);

  glm::vec3 v3Translation(v4Translation[0], v4Translation[1], v4Translation[2]);
  glm::vec3 v3ViewDir(v4ViewDir[0], v4ViewDir[1], v4ViewDir[2]);
  glm::vec3 v3UpDir(v4UpDir[0], v4UpDir[1], v4UpDir[2]);

  // Create the View Matrix.
  glm::mat4 mat4ViewMatrix = glm::lookAt(v3Translation, v3Translation + v3ViewDir, v3UpDir);

  return mat4ViewMatrix;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/