/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageVAO.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageVAO::MageVAO(MageMesh* pMesh)
  : m_pMesh(pMesh)
{
  // Create a VAO.
  glGenVertexArrays(1, &m_iBufferID);
  glBindVertexArray(m_iBufferID);

  // Create the VBOs.
  if(m_pMesh->GetVertices().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::VertexBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  if (m_pMesh->GetNormals().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::NormalBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  if (m_pMesh->GetUVs().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::UvBuffer, new MageVBO(GL_ARRAY_BUFFER)));
  else if (m_pMesh->GetColors().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::ColorBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  if (m_pMesh->GetFaceIndices().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::FaceIndexBuffer, new MageVBO(GL_ELEMENT_ARRAY_BUFFER)));

  if (m_pMesh->GetJointIndices().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::JointIndexBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  if (m_pMesh->GetJointWeights().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::JointWeightBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  if (m_pMesh->GetTangents().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::TangentBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  if (m_pMesh->GetBiTangents().size() > 0)
    m_mapVBOs.insert(std::make_pair(VboType::BiTangentBuffer, new MageVBO(GL_ARRAY_BUFFER)));

  glBindVertexArray(0);
}


MageVAO::~MageVAO()
{
  // Delete VBOs
  std::map<VboType, MageVBO*>::iterator it_VBO;
  for (it_VBO = m_mapVBOs.begin(); it_VBO != m_mapVBOs.end(); ++ it_VBO)
  {
    delete it_VBO->second;
  }
  m_mapVBOs.clear();

  // Delete VAO.
  glDeleteVertexArrays(1, &m_iBufferID);
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool MageVAO::Draw()
{
  // The Data needs to be uploaded.
  if (m_eState != VaoState::Uploaded)
    return false;

  // Bind VAO.
  glBindVertexArray(m_iBufferID);

  // Draw the Mesh.
  if (m_pMesh->GetFaceIndices().size() <= 0)
    glDrawArrays(m_pMesh->GetMode(), 0, m_pMesh->GetSize());
  else
    glDrawElements(m_pMesh->GetMode(), m_pMesh->GetSize(), GL_UNSIGNED_INT, 0);

  // Release VAO.
  glBindVertexArray(0);

  return true;
}


bool MageVAO::Upload(GLenum eUsage)
{
  // The VBO is uploaded already.
  if (m_eState != VaoState::Free)
    return false;

  m_eState = VaoState::Uploading;

  // Bind the VAO.
  glBindVertexArray(m_iBufferID);


  // Get Mesh Data.
  std::vector<glm::vec3> vecVertices;
  std::vector<glm::vec3> vecNormals;
  std::vector<glm::vec3> vecColors;
  std::vector<glm::vec2> vecUVs;
  std::vector<unsigned int> vecFaceIndices;
  std::vector<glm::ivec4> vecJointIndices;
  std::vector<glm::vec4>  vecJointWeights;
  std::vector<glm::vec3> vecTangents;
  std::vector<glm::vec3> vecBiTangents;

  m_pMesh->GetData(vecVertices, vecNormals, vecColors, vecUVs, vecFaceIndices, vecTangents, vecBiTangents, vecJointIndices, vecJointWeights);


  // Upload Vertex Data.
  if (vecVertices.size() > 0)
    m_mapVBOs[VboType::VertexBuffer]->Upload(eUsage, vecVertices);

  // Bind Normal Buffer.
  if (vecNormals.size() > 0)
    m_mapVBOs[VboType::NormalBuffer]->Upload(eUsage, vecNormals);

  // Bind UV Buffer.
  if (vecUVs.size() > 0)
    m_mapVBOs[VboType::UvBuffer]->Upload(eUsage, vecUVs);
  else if (vecColors.size() > 0)
    m_mapVBOs[VboType::ColorBuffer]->Upload(eUsage, vecColors);

  // Bind Element Array Buffer.
  if (vecFaceIndices.size() > 0)
    m_mapVBOs[VboType::FaceIndexBuffer]->Upload(eUsage, vecFaceIndices);

  // Bind Joint Index Buffer.
  if (vecJointIndices.size() > 0)
    m_mapVBOs[VboType::JointIndexBuffer]->Upload(eUsage, vecJointIndices);

  // Bind Joint Weight Buffer.
  if (vecJointWeights.size() > 0)
    m_mapVBOs[VboType::JointWeightBuffer]->Upload(eUsage, vecJointWeights);

  // Bind Tangent Buffer.
  if (vecTangents.size() > 0)
    m_mapVBOs[VboType::TangentBuffer]->Upload(eUsage, vecTangents);

  // Bind Bi-Tangent Buffer.
  if (vecBiTangents.size() > 0)
    m_mapVBOs[VboType::BiTangentBuffer]->Upload(eUsage, vecBiTangents);


  // Upload Vertex Attribute Pointer Information.
  if (vecVertices.size() > 0)
  {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::VertexBuffer]->GetBufferID());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Upload Normal Attribute Pointer Information.
  if (vecNormals.size() > 0) 
  {
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::NormalBuffer]->GetBufferID());
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Upload UV Attribute Pointer Information, if there are UV Information.
  if (vecUVs.size() > 0) 
  {
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::UvBuffer]->GetBufferID());
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }
  // Upload Color Attribute Pointer Information otherwise.
  else if (vecColors.size() > 0)  
  {
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::ColorBuffer]->GetBufferID());
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Upload Joint Index Attribute Pointer Information.
  if (vecJointIndices.size() > 0)
  {
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::JointIndexBuffer]->GetBufferID());
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Upload Joint Weight Attribute Pointer Information.
  if (vecJointWeights.size() > 0)
  {
    glEnableVertexAttribArray(4);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::JointWeightBuffer]->GetBufferID());
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Upload Tangent Attribute Pointer Information.
  if (vecTangents.size() > 0) 
  {
    glEnableVertexAttribArray(5);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::TangentBuffer]->GetBufferID());
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Upload Bi-Tangent Attribute Pointer Information.
  if (vecBiTangents.size() > 0) 
  {
    glEnableVertexAttribArray(6);
    glBindBuffer(GL_ARRAY_BUFFER, m_mapVBOs[VboType::BiTangentBuffer]->GetBufferID());
    glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }


  // Release the VAO.
  glBindVertexArray(0);
  m_eState = VaoState::Uploaded;

  return true;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/