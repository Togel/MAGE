#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../Core/MageSingleton.h"
#include "../Core/MageDrawContext.h"

#include "glfw3.h"
#include "glm.hpp"

#include <map>

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/

enum KeyStatus
{
  KeyIdle,
  KeyPushed,
  KeyReleased
};

/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageInputDeviceHandler : public MageSingleton<MageInputDeviceHandler>
{
  friend class MageSingleton<MageInputDeviceHandler>;

public:
  /*
  * Destructor.
  */
  ~MageInputDeviceHandler();


  /*
  * Changes the Mouse Cursor Position to the Middle of the Window.
  */
  void CenterMouse();


  /*
  * Gets the current Status of a specific Key.
  * Valid Input is:
  * A-Z, 0-9, F1-F12
  * ",", ".", "-", "'", "`", "/", "\", ";", "=", "[", "]"
  * Escape, Return, Tab, Backspace
  * Insert, Delete, PageUp, PageDown, Home, End, Scroll, Print, Pause
  * Right, Left, Up, Down
  * LeftShift, RightShift, LeftCtrl, RightCtrl, LeftAlt, RightAlt, CapsLock
  * Numpad: NumLock, NP0-NP9, NP/, NP*, NP-, NP+, NPEnter, NP,
  * Mouse: MBT1-MBT8 (MBT1 = Left Mouse Button, MBT2 = Right Mouse Button, MBT3 = Middle Mouse Button)
  * @param 'sKey' defines the Key for which the Status should be returned.
  * @return Enumeration Entry defining the Key Status.
  */
  KeyStatus GetKeyStatus(std::string sKey);


  /*
  * Returns the current Mouse Distance from the Window Center.
  * @return Vector storing the Distance in x and y Coordinate from the Window Center.
  */
  glm::vec2 GetMouseDistanceFromCenter();


  /*
  * Returns the current Mouse Position inside the Window.
  * @return Vector storing the x and y Coordinate of the Mouse.
  */
  glm::vec2 GetMousePosition();


  /*
  * Switches the Draw Context.
  * The Handler needs a Pointer to the GLFW Window in order to operate.
  * @param 'pDrawContext' Pointer to the new Draw Context.
  */
  void SwitchDrawContext(MageDrawContext* pDrawContext);


  /*
  * Toggles the Mouse Cursor Visibility.
  * The Cursor can be shown or hidden.
  * @param 'bVisible' defines whether the Cursor should be visible (true) or not (false).
  */
  void ToggleMouseVisibility(bool bVisible);


  /*
  * Updates the Key Status for all Keyboard Keys.
  * This function is called automatically by the Draw Thread.
  */
  void _Update();


protected:
  /*
  * Constructor.
  */
  MageInputDeviceHandler();


private:
  // Maps a Keyboard Key to the GLFW Equivalent.
  std::map<std::string, int> m_mapKeyMappings;
  // Maps a Mousekey Key to the GLFW Equivalent.
  std::map<std::string, int> m_mapMouseMappings;
  // Stores the Status (Idle, Pushed, Released) of all Keys.
  std::map<std::string, KeyStatus> m_mapKeyStatus;

  // Pointer the the associated Draw Context.
  // The Handler needs to Reference to operate.
  MageDrawContext* m_pDrawContext = nullptr;


  /*
  * Create the Mapping between GLFW Keys and String.
  */
  void CreateMapping();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/