/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageAnimatedModel.h"

#include "../../IO/mio.h"

#include <algorithm>

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageAnimatedModel::MageAnimatedModel(MageVAO* pVAO, std::vector<MageTexture*> vecTextures, MageJoint* pSkeleton, std::vector<MageAnimation> vecAnimations)
  : MageTexturedModel(pVAO, vecTextures)
  , m_pSkeleton(pSkeleton)
  , m_vecAnimations(vecAnimations)
{
}


MageAnimatedModel::~MageAnimatedModel()
{
  m_vecAnimations.clear();
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageAnimatedModel::_Update(MageTimer* pTimer)
{
  // Abort if no Animation is active.
  if (m_iActiveAnimation == -1)
    return;

  // Update Animation Time.
  m_dAnimationTime = std::min(m_dAnimationTime + (pTimer->GetLoopTime() / 1000.0f), m_vecAnimations[m_iActiveAnimation].m_dDuration);

  // Check whether the next Keyframe has been reached.
  while (m_iActiveKeyframe < static_cast<int>(m_vecAnimations[m_iActiveAnimation].m_vecKeyframes.size()) - 1
    && m_dAnimationTime >= m_vecAnimations[m_iActiveAnimation].m_vecKeyframes[m_iActiveKeyframe + 1].m_dTimeStamp)
  {
    m_iActiveKeyframe++;
  }

  // Stores new Pose.
  std::map<std::string, glm::mat4> mapCurrentPose;

  // Interpolate between two Keyframes.
  if (m_dAnimationTime < m_vecAnimations[m_iActiveAnimation].m_dDuration)
  {
    // Determine the current and the next Keyframe.
    m_vecAnimations[m_iActiveAnimation].m_vecKeyframes;
    MageKeyframe sCurKeyframe  = m_vecAnimations[m_iActiveAnimation].m_vecKeyframes[m_iActiveKeyframe];
    MageKeyframe sNextKeyframe = m_vecAnimations[m_iActiveAnimation].m_vecKeyframes[m_iActiveKeyframe + 1];

    // Calculate the Process between the current and the next Keyframe. Process = Time inside Keyframe / Total Keyframe Time.
    double dProcess = (m_dAnimationTime - sCurKeyframe.m_dTimeStamp) / (sNextKeyframe.m_dTimeStamp - sCurKeyframe.m_dTimeStamp);

    // Interpolate the Transformations of the current and the next Keyframe. Loop over Joint IDs.
    for (std::map<std::string, MageTransformation*>::iterator it = sCurKeyframe.m_mapPose.begin(); it != sCurKeyframe.m_mapPose.end(); ++it)
    {
      MageTransformation* pInterpolatedTrans = sCurKeyframe.m_mapPose[it->first]->Interpolate(sNextKeyframe.m_mapPose[it->first], dProcess);
      mapCurrentPose[it->first] = pInterpolatedTrans->GetTransformation();
    }
  }  

  // Final Keyframe has been reached.
  else if (m_dAnimationTime == m_vecAnimations[m_iActiveAnimation].m_dDuration)
  {
    MageKeyframe sCurKeyframe = m_vecAnimations[m_iActiveAnimation].m_vecKeyframes[m_iActiveKeyframe];

    // Sets all Transformations according to the last Keyframe. Loop over Joint IDs.
    for (std::map<std::string, MageTransformation*>::iterator it = sCurKeyframe.m_mapPose.begin(); it != sCurKeyframe.m_mapPose.end(); ++it)
    {
      mapCurrentPose[it->first] = sCurKeyframe.m_mapPose[it->first]->GetTransformation();
    }

    // Disable the Animation.
    m_iActiveAnimation = -1;
    m_iActiveKeyframe = -1;
  }

  // Apply the interpolated Transformations from the Keyframes to the Skeleton.
  UpdateJointTransformations(mapCurrentPose, m_pSkeleton, glm::mat4(1.0f));
}


void MageAnimatedModel::GetJointTransformations(glm::mat4 amat4JointTransforms[])
{
  // Get the Joint Transformations.
  std::map<unsigned int, MageJoint*> mapJoints;
  m_pSkeleton->GetSortedJoints(mapJoints);

  // Get Size of the Array.
  //const int iMaxJoints = (sizeof(amat4JointTransforms) / sizeof(*amat4JointTransforms));
  const int iMaxJoints = 50;

  // Add the Joint Transformations with increasing Index to the Array.
  std::map<unsigned int, MageJoint*>::iterator it_Joints;
  for (it_Joints = mapJoints.begin(); it_Joints != mapJoints.end(); ++it_Joints)
  {
    // There can only be a limited Number of Joints
    if (it_Joints->first >= iMaxJoints)
    {
      mio::err << "MageAnimatedModel::GetJointTransformations(): Too many Joints." << mio::endl;
      break;
    }
     
    amat4JointTransforms[it_Joints->first] = it_Joints->second->GetCurrentTransformation();
  }
}


ModelType MageAnimatedModel::GetType()
{
  return ModelType::Animated;
}


void MageAnimatedModel::StartAnimation(unsigned int iID)
{
  // Only start a new Animation if no other Animation is active.
  if (m_iActiveAnimation != -1)
    return;

  // Reset Animation Time.
  m_dAnimationTime = 0.0;

  // Only start valid Animations.
  if (iID < m_vecAnimations.size())
  {
    m_iActiveAnimation = iID;
    m_iActiveKeyframe  = 0;
  }
}


void MageAnimatedModel::UpdateJointTransformations(std::map<std::string, glm::mat4> mapCurrentPose, MageJoint* pJoint, glm::mat4 m4ParentTransform)
{
  if (mapCurrentPose.find(pJoint->GetName()) == mapCurrentPose.end())
  {
    mio::err << "Animation Failure. Unknown Joint ID." << mio::endl;
    return;
  }

  // Receive the Transformation in Local and Model Space.
  glm::mat4 m4LocalTransform = mapCurrentPose[pJoint->GetName()];
  glm::mat4 m4ModelTransform = m4ParentTransform * m4LocalTransform;

  // Update the Joint Transformations of all Child Nodes.
  for (MageJoint* pChild : pJoint->GetChildren())
  {
    UpdateJointTransformations(mapCurrentPose, pChild, m4ModelTransform);
  }

  // Update the Joint Transformation.
  pJoint->SetCurrentTransformation(m4ModelTransform * pJoint->GetInverseBindTransform());
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/