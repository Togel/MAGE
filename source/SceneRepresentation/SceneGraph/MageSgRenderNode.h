#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgNode.h"
#include "glm.hpp"

#include "../Model/MageModel.h"

#include "../../Shading/MageShader.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageSgRenderNode : public MageSgNode
{
public:
  /*
  * Constructor.
  */
  MageSgRenderNode(MageShader* pShader, MageModel* pModel);


  /*
  * Destructor.
  */
  virtual ~MageSgRenderNode();


  /*
  * Draw the Model of the Node.
  * @param 'mat4Transformation' defines the Matrix storing all Transformations of its Parents.
  */
  virtual void Draw(glm::mat4 mat4Transformation);


  /*
  * Returns an individual Enumeration Entry defining the Type of the Node.
  * See 'NodeType' Enumeration for more Detail.
  * @return Enumeration Entry defining the Type of the Node.
  */
  NodeType GetType() override;


  /*
  * Returns whether the Node has transparent Parts.
  * @return 'true' if the Node has transparent Parts.
  */
  virtual bool IsTransparent();


  /*
  * Redefines which Shader should be used for drawing.
  * @param 'pShader' defines the Shader which is going to be used for drawing.
  */
  virtual void SetShader(MageShader* pShader);


  /*
  * Redefines the Value of the Alpha Canal.
  * @param 'fAlpha' defines the new Alpha Value.
  */
  virtual void SetAlpha(float fAlpha);


  /*
  * Updates the Model by calling its Update Function.
  * @param 'pTimer' defines Timer used by the Geometry Thread.
  */
  virtual void Update(MageTimer* pTimer) override;


protected:
  // Pointer to the Shader Program used.
  MageShader* m_pShader = nullptr;
  // Pointer to the Model.
  MageModel*  m_pModel = nullptr;

  // Alpha Channel which get uploaded as Uniform.
  float m_fAlpha = 1.0f;


  /*
  * Initializes additional Uniforms if needed.
  * Needs to be implemented in Sub-Class.
  */
  virtual void InitCustomUniforms();
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/