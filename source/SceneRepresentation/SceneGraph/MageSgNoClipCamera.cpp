/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgNoClipCamera.h"

#include "../../IO/MageInputDeviceHandler.h"

#include "../../IO/mio.h"

#include "gtx/rotate_vector.hpp"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSgNoClipCamera::MageSgNoClipCamera()
{
  MageInputDeviceHandler::GetInstance()->ToggleMouseVisibility(false);
  MageInputDeviceHandler::GetInstance()->CenterMouse();
}


MageSgNoClipCamera::~MageSgNoClipCamera()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool l_bFirstCall = true;

void MageSgNoClipCamera::Update(MageTimer* pTimer)
{
  // Skip the very first Input.
  // This prevents for example huge Camera Movements as the Mouse is not located at the Window Center in the Beginning.
  if (l_bFirstCall)
  {
    l_bFirstCall = false;
    return;
  }

  // Calculate Speed Modifier.
  float fTransSpeedMod = m_fTranslationSpeed * static_cast<float>(pTimer->GetLoopTime());
  float fRotSpeedMod   = m_fRotationSpeed    * static_cast<float>(pTimer->GetLoopTime());

  // Get Mouse Offset.
  MageInputDeviceHandler* pHandler = MageInputDeviceHandler::GetInstance();
  glm::vec2 v2MouseOffset = pHandler->GetMouseDistanceFromCenter();
  pHandler->CenterMouse();
    
  // Get last Camera Orientation.
  glm::mat4 m4LastTransform = GetTransformation();

  glm::vec4 v4Translation = m4LastTransform[3];
  glm::vec4 v4ViewDir = -m4LastTransform[2];
  glm::vec4 v4Up = m4LastTransform[1];
  glm::vec4 v4Right = m4LastTransform[0];

  glm::vec3 v3Translation(v4Translation[0], v4Translation[1], v4Translation[2]);
  glm::vec3 v3ViewDir(v4ViewDir[0], v4ViewDir[1], v4ViewDir[2]);
  glm::vec3 v3Up(v4Up[0], v4Up[1], v4Up[2]);
  glm::vec3 v3Right(v4Right[0], v4Right[1], v4Right[2]);
  glm::vec3 v3yAxis(0.0f, 1.0f, 0.0f);

  // Calculate horizontal Camera Rotation.
  v3ViewDir = glm::rotate(v3ViewDir, -fRotSpeedMod*v2MouseOffset.x, v3yAxis);
  v3Up      = glm::rotate(v3Up,      -fRotSpeedMod*v2MouseOffset.x, v3yAxis);
  v3Right   = glm::rotate(v3Right,   -fRotSpeedMod*v2MouseOffset.x, v3yAxis);

  // Calculate vertical Camera Rotation.
  v3ViewDir = glm::rotate(v3ViewDir, fRotSpeedMod*v2MouseOffset.y, v3Right);
  v3Up      = glm::rotate(v3Up, fRotSpeedMod*v2MouseOffset.y, v3Right);

  // Sprinting.
  float fSprint = 1.0f;
  if (pHandler->GetKeyStatus("LeftShift") == KeyStatus::KeyPushed)
    fSprint = 2.0f;

  // Translation into Viewing Direction.
  if (pHandler->GetKeyStatus("W") == KeyStatus::KeyPushed)
    v3Translation += fSprint*fTransSpeedMod*v3ViewDir;
  // Translation against Viewing Direction.
  if (pHandler->GetKeyStatus("S") == KeyStatus::KeyPushed)
    v3Translation -= fSprint*fTransSpeedMod*v3ViewDir;
  // Strafing to the Left.
  if (pHandler->GetKeyStatus("A") == KeyStatus::KeyPushed)
    v3Translation -= fSprint*fTransSpeedMod*v3Right;
  // Strafing to the Right.
  if (pHandler->GetKeyStatus("D") == KeyStatus::KeyPushed)
    v3Translation += fSprint*fTransSpeedMod*v3Right;
  // Upwards Translation.
  if (pHandler->GetKeyStatus("Space") == KeyStatus::KeyPushed)
    v3Translation += fSprint*fTransSpeedMod*v3yAxis;
  // Downwards Translation.
  if (pHandler->GetKeyStatus("LeftCtrl") == KeyStatus::KeyPushed)
    v3Translation -= fSprint*fTransSpeedMod*v3yAxis;

  // Apply Transformation.
  glm::mat4 mat4CurTransform(glm::vec4(v3Right, 0.0f), glm::vec4(v3Up, 0.0f), -glm::vec4(v3ViewDir, 0.0f), glm::vec4(v3Translation, 1.0f));
  SetTransformation(mat4CurTransform);
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/