set( RelativeDir "source/SceneRepresentation/Model" )
set( RelativeSourceGroup "SourceFiles\\SceneRepresentation\\Model" )

set(Directories

)

set(DirFiles
  _SourceFiles.cmake
  MageAnimatedModel.cpp
  MageAnimatedModel.h
  MageAnimation.h
  MageJoint.cpp
  MageJoint.h
  MageKeyFrame.h
  MageMesh.cpp
  MageMesh.h 
  MageModel.cpp
  MageModel.h
  MageModelFactory.cpp
  MageModelFactory.h
  MageTexture.cpp
  MageTexture.h
  MageTexturedModel.cpp
  MageTexturedModel.h
  MageTransformation.cpp
  MageTransformation.h
  MageVAO.cpp
  MageVAO.h
  MageVBO.cpp
  MageVBO.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source/SceneRepresentation")