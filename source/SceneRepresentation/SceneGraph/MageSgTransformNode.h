#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgNode.h"

#include "glm.hpp"

#include "gtx/quaternion.hpp"


/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageSgTransformNode : public MageSgNode
{
public:
  /*
  * Constructor.
  */
  MageSgTransformNode();


  /*
  * Constructor.
  */
  virtual ~MageSgTransformNode();


  /*
  * Returns the Rotation of the Transformation.
  * @return Quaternion storing the Rotation.
  */
  glm::quat GetRotation();


  /*
  * Returns the Transformation.
  * @return The Transformation Matrix.
  */
  glm::mat4 GetTransformation();


  /*
  * Returns the Translation.
  * @return the Translation Vector.
  */
  glm::vec3 GetTranslation();


  /*
  * Returns an individual Enumeration Entry defining the Type of the Node.
  * See 'NodeType' Enumeration for more Detail.
  * @return Enumeration Entry defining the Type of the Node.
  */
  NodeType GetType() override;


  /*
  * Adds a Rotation to the Transformation Matrix.
  * @param 'fAngle' defines the Angle by which the Node should be rotated.
  * @param 'v3Axis' defines the Axis around which the Node should be rotated.
  */
  void Rotate(float fAngle, glm::vec3 v3Axis);


  /*
  * Adds a Rotation to the Transformation Matrix.
  * @param 'qRotation' A Quaternion defining the Rotation which should be applied.
  */
  void Rotate(glm::quat qRotation);


  /*
  * Scales the Node by a given Factor in all Dimensions.
  * @param 'fScaling' defines the Factor by which the Node gets scaled.
  */
  void Scale(float fScaling);


  /*
  * Scales the Node by a given Factors.
  * @param 'v3Scaling' defines Vector of Scaling Factors.
  */
  void Scale(glm::vec3 v3Scaling);


  /*
  * Redefine the Transformation.
  * @param 'matTransformation' defines the new Transformation.
  */
  void SetTransformation(glm::mat4 mat4Transformation);


  /*
  * Redefines the Translation.
  * @param 'v3Translation' defines the new Translation Vector.
  */
  void SetTranslation(glm::vec3 v3Translation);


  /*
  * Adds a translation to the current translation
  * @param 'v3Translation' defines the Translation vector.
  */
  void Translate(glm::vec3 v3Translation);


  /*
  * Adds another Transformation to the current Transformation. 
  * @param 'mat4Transformation' defines the Transformation Matrix which should be applied to the current one.
  */
  void Transform(glm::mat4 mat4Transformation);


private:
  glm::mat4 m_mat4Transformation = glm::mat4();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/