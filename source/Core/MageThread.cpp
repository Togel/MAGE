/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageThread.h"

#include "../IO/mio.h"
#include "Timer/MageTimerFactory.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageThread::MageThread()
  : m_pThreadTimer(new MageTimerWithUpdate())
{
}


MageThread::~MageThread()
{
  // Delete all Callbacks.
  typedef std::map<unsigned int, MageCallback*>::iterator it_cb;
  for (it_cb it = m_mapCallbacks.begin(); it != m_mapCallbacks.end(); ++it)
  {
    delete it->second;
  }

  m_mapCallbacks.clear();

  // Delete the Timer.
  delete m_pThreadTimer;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

unsigned int MageThread::AddCallback(MageCallback* pCallback)
{
  // Include the Callback.
  m_mapCallbacks[m_iNextCallbackID++] = pCallback;
  
  // Next Timer ID is increased during Insertion. Therefore, subtract this Value by 1.
  return (m_iNextCallbackID - 1);
}


bool MageThread::DeleteCallback(unsigned int iID)
{
  // Check whether the ID is not valid anymore.
  if (m_mapCallbacks.find(iID) == m_mapCallbacks.end())
  {
    mio::err << "MageThread::DeleteCallback() -> Trying to delete Callback with ID " << iID << ". ID does not exist (anymore)." << mio::endl;
    return false;
  }
  // Remove the Callback.
  else
  {
    m_mapCallbacks.erase(m_mapCallbacks.find(iID));
    return true;
  }
}


MageTimer* MageThread::GetTimer()
{
  return dynamic_cast<MageTimer*>(m_pThreadTimer);
}


bool MageThread::IsOwnerOfCallback(unsigned int iID)
{
  if (m_mapCallbacks.find(iID) != m_mapCallbacks.end())
    return true;
  else
    return false;
}


void MageThread::SwitchScene(MageScene* pScene)
{
  m_pScene = pScene;
}


void MageThread::_UpdateTimer()
{
  m_pThreadTimer->_Update();
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/