/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageThreadGeometry.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageThreadGeometry::MageThreadGeometry()
{
}

MageThreadGeometry::~MageThreadGeometry()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void MageThreadGeometry::_Run()
{
  // Call Pre-Geometry Callbacks.
  for(it_CB it = m_mapCallbacks.begin(); it != m_mapCallbacks.end(); ++it)
  {
    if (it->second->GetType() == CallbackType::PreGeometry)
      it->second->Do(m_pThreadTimer);
  }

  // Update the Geometry of the Scene.
  if (m_pScene)
    m_pScene->Update(m_pThreadTimer);

  // Call Post-Geometry Callbacks.
  for (it_CB it = m_mapCallbacks.begin(); it != m_mapCallbacks.end(); ++it)
  {
    if (it->second->GetType() == CallbackType::PostGeometry)
      it->second->Do(m_pThreadTimer);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/