#pragma once
#include <string>


static const std::string ANIMATION_CAM_VS = R"(#version 330 core

const int iMaxJoints = 50;

// [Input Data].
layout(location = 0) in vec3 v3Vertex;
layout(location = 1) in vec3 v3Normal;
layout(location = 2) in vec2 v2UV;
layout(location = 3) in ivec4 v4JointIndices;
layout(location = 4) in vec4 v4JointWeights;

// [Output Data].
out vec2 v2TexUV;

// [Uniform Data].
uniform mat4 mat4Model;
uniform mat4 mat4InvTransModel;
uniform mat4 mat4JointTransforms[iMaxJoints];

layout (std140) uniform Camera
{
  mat4 mat4View;
  mat4 mat4Projection;
  mat4 mat4InvTransView;
};

// [Main Program].
void main()
{
  // Apply Joint Transformations.
  vec4 v4LocalPos = vec4(0.0);
  vec4 v4LocalNormal = vec4(0.0);
	
  for(int iIndex = 0; iIndex < 4; iIndex++)
  {
    mat4 mat4JointTransform = mat4JointTransforms[v4JointIndices[iIndex]];
    vec4 v4LocalPosQuarter = mat4JointTransform * vec4(v3Vertex, 1.0);
    v4LocalPos += v4LocalPosQuarter * v4JointWeights[iIndex];
		
    vec4 v4NormalQuarter = mat4JointTransform * vec4(v3Normal, 0.0);
    v4LocalNormal += v4NormalQuarter * v4JointWeights[iIndex];
  }

  // Output position of the vertex, in clip space : MVP * position
  gl_Position = mat4Projection * mat4View * mat4Model * v4LocalPos;

  // Process the UV Coordinate to the Fragment Shader.
  v2TexUV = v2UV;
})";


static const std::string ANIMATION_CAM_FS = R"(#version 330 core

// [Input Data].
in vec2 v2TexUV;

// [Output Data].
out vec3 v3Color;

// [Uniform Data].
uniform sampler2D texSampler;

// [Main Program].
void main()
{
  // Map the Texture to the Mesh.
  v3Color = texture(texSampler, v2TexUV).rgb;
})";