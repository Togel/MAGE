/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "OpenGLMesh.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

OpenGlMesh::OpenGlMesh(std::string sPath)
  : MageMesh(sPath)
{
}


OpenGlMesh::~OpenGlMesh()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

void OpenGlMesh::Draw()
{
  if (m_eState != MeshState::Uploaded)
    return;

  glBindVertexArray(m_iVAO); // Bind VAO

  if (m_vecFaceIndices.size() <= 0)
    glDrawArrays(GL_TRIANGLES, 0, m_iSize);
  else
    glDrawElements(GL_TRIANGLES, m_iSize, GL_UNSIGNED_INT, 0);

  glBindVertexArray(0); // Unbind VAO
}


bool OpenGlMesh::FreeVRam()
{
  if (m_eState != MeshState::Uploaded)
    return false;

  m_eState = MeshState::Loaded;

  glDeleteBuffers(1, &m_iVertexBuffer);
  glDeleteBuffers(1, &m_iNormalBuffer);
  glDeleteBuffers(1, &m_iUVBuffer);
  glDeleteBuffers(1, &m_iTangentBuffer);
  glDeleteBuffers(1, &m_iBiTangentBuffer);
  glDeleteBuffers(1, &m_iEABuffer);
  glDeleteVertexArrays(1, &m_iVAO);

  return true;
}


bool OpenGlMesh::Upload()
{
  if (m_eState != MeshState::Loaded)
    return false;

  m_eState = MeshState::Uploading;

  // [Create Buffer Objects]
  // Calculate the Number of drawable Elements.
  if (m_vecFaceIndices.size() > 0)
    m_iSize = static_cast<GLsizei>(m_vecFaceIndices.size()); // Indexed Faces
  else
    m_iSize = static_cast<GLsizei>(m_vecVertices.size()); // Vertices

  // Create Buffers.
  glGenVertexArrays(1, &m_iVAO);
  glBindVertexArray(m_iVAO);

  glGenBuffers(1, &m_iVertexBuffer);
  glGenBuffers(1, &m_iNormalBuffer);
  glGenBuffers(1, &m_iUVBuffer);
  glGenBuffers(1, &m_iEABuffer);
  glGenBuffers(1, &m_iTangentBuffer);
  glGenBuffers(1, &m_iBiTangentBuffer);


  // [Upload Data]
  // Bind Vertex Buffer.
  if (m_vecVertices.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecVertices.size(), &m_vecVertices[0], GL_STATIC_DRAW);
  }

  // Bind Normal Buffer.
  if (m_vecNormals.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecNormals.size(), &m_vecNormals[0], GL_STATIC_DRAW);
  }

  // Bind UV Buffer.
  if (m_vecUVs.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iUVBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*m_vecUVs.size(), &m_vecUVs[0], GL_STATIC_DRAW);
  }

  // Bind Element Array Buffer.
  if (m_vecFaceIndices.size() > 0)
  {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iEABuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*m_vecFaceIndices.size(), &m_vecFaceIndices[0], GL_STATIC_DRAW);
  }

  // Bind Tangent Buffer.
  if (m_vecTangents.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iTangentBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecTangents.size(), &m_vecTangents[0], GL_STATIC_DRAW);
  }

  // Bind Bi-Tangent Buffer.
  if (m_vecBitangents.size() > 0)
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_iBiTangentBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*m_vecBitangents.size(), &m_vecBitangents[0], GL_STATIC_DRAW);
  }

  // Unbind Buffer.
  glBindBuffer(GL_ARRAY_BUFFER, 0);


  // [Setup Vertex Array Object]
  // Enable Vertex Attribute Pointer.
  if (m_vecVertices.size() > 0)
  {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_iVertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Enable Normal Attribute Pointer.
  if (m_vecNormals.size() > 0)
  {
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_iNormalBuffer);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Enable UV Attribute Pointer.
  if (m_vecUVs.size() > 0)
  {
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_iUVBuffer);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Enable Tangent Attribute Pointer.
  if (m_vecTangents.size() > 0)
  {
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Enable Bi-Tangent Attribute Pointer.
  if (m_vecBitangents.size() > 0)
  {
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  }

  // Unbind VAO
  glBindVertexArray(0);

  m_eState = MeshState::Uploaded;
  return true;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/