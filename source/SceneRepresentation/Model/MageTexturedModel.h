#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageModel.h"
#include "MageTexture.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageTexturedModel : public MageModel
{
public:
  /*
  * Constructor.
  */
  MageTexturedModel(MageVAO* pVAO, std::vector<MageTexture*> vecTextures);


  /*
  * Destructor.
  */
  virtual ~MageTexturedModel();


  /*
  * Binds the Textures of the Model.
  */
  void BindTextures();


  /*
  * Binds the Textures.
  * Binds the Vertex Buffer Objects and draws its Contents.
  * @return true if the Drawing Process has been successful.
  */
  virtual bool Draw() override;


  /*
  * Returns the Type of the Model.
  * @return Enum Entry defining the Type of the Model.
  */
  virtual ModelType GetType() override;


protected:
  std::vector<MageTexture*> m_vecTextures;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/