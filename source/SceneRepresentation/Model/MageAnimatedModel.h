#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTexturedModel.h"
#include "MageJoint.h"
#include "MageAnimation.h"

#include "../../Core/Timer/MageTimer.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/

class MageAnimatedModel : public MageTexturedModel
{
public:
  /*
  * Constructor.
  */
  MageAnimatedModel(MageVAO* pVAO, std::vector<MageTexture*> vecTextures, MageJoint* pSkeleton, std::vector<MageAnimation> vecAnimations);


  /*
  * Destructor.
  */
  virtual ~MageAnimatedModel();


  /*
  * Interpolates between the current and the next Keyframe.
  * Applies the resulting Transformations to the Skeleton of the Model.
  * @param 'pTimer' Pointer to the Timer of the Geometry Thread.
  */
  void _Update(MageTimer* pTimer);


  /*
  * Returns an Array containing the Transformations of all Joints of the Skeleton.
  * @param 'amat4JointTransforms' Array containing Matrices with the Transformation Information.
  */
  void GetJointTransformations(glm::mat4 amat4JointTransforms[]);


  /*
  * Returns the Type of the Model.
  * @return Enum Entry defining the Type of the Model.
  */
  ModelType GetType() override;


  /*
  * Kicks of a specific Animation.
  * An Animation can only be started if no other is active.
  * @param 'iID' defines the ID of the Animation.
  */
  void StartAnimation(unsigned int iID);


private:
  // Root of the Skeleton Hierarchy.
  MageJoint* m_pSkeleton = nullptr;

  // Vector storing all Animations of the Model.
  std::vector<MageAnimation> m_vecAnimations;

  // Stores the currently activated Animation.
  // Equals -1 if no Animation is active.
  int m_iActiveAnimation = -1;

  // Stores the currently activated Keyframe.
  // Equals -1 if no Animation is active.
  int m_iActiveKeyframe  = -1;

  // Stores the Time the Animation is already active.
  double m_dAnimationTime = 0.0;


  /*
  * Update the Joint Transformations of the Skeleton.
  * Takes the interpolated Transformation from the Keyframe Animation and applies them to the Joints.
  * @param 'mapCurrentPose' storing the interpolated Transformations.
  * @param 'pJoint' defines the Joint whose Transformation is changed.
  * @param 'm4ParentTransform' stores the model-space Transformation of the Parent-Joint.
  */
  void UpdateJointTransformations(std::map<std::string, glm::mat4> mapCurrentPose, MageJoint* pJoint, glm::mat4 m4ParentTransform);
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/