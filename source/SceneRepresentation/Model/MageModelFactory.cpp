/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageModelFactory.h"
#include "MageMesh.h"

#include "SOIL.h"

#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/mesh.h"

#include "../../IO/mio.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageModelFactory::MageModelFactory()
{
}


MageModelFactory::~MageModelFactory()
{
}


struct sortDoubles
{
  inline bool operator() (const double& dValue1, const double& dValue2)
  {
    return (dValue1 < dValue2);
  }
};


/******************************************************************************/
/*                              INLINE FUNCTIONS                              */
/******************************************************************************/

inline glm::vec3 glm_cast(aiVector3D& v3Vector)
{
  return glm::vec3(v3Vector.x, v3Vector.y, v3Vector.z);
}


inline glm::vec3 glm_cast(aiColor4D& v4Color)
{
  return glm::vec3(v4Color.r, v4Color.g, v4Color.b);
}


inline glm::vec2 glm_vec3_to_vec2_cast(aiVector3D& v3Vector)
{
  return glm::vec2(v3Vector.x, v3Vector.y);
}


inline glm::quat glm_cast(aiQuaternion& qQuat)
{
  return glm::quat(qQuat.w, qQuat.x, qQuat.y, qQuat.z);
}


//inline glm::mat4 glm_cast(aiMatrix4x4& mat4Matrix)
//{
//  return glm::mat4(
//    mat4Matrix.a1, mat4Matrix.a2, mat4Matrix.a3, mat4Matrix.a4,
//    mat4Matrix.b1, mat4Matrix.b2, mat4Matrix.b3, mat4Matrix.b4,
//    mat4Matrix.c1, mat4Matrix.c2, mat4Matrix.c3, mat4Matrix.c4,
//    mat4Matrix.d1, mat4Matrix.d2, mat4Matrix.d3, mat4Matrix.d4);
//}

inline glm::mat4 glm_cast(aiMatrix4x4& mat4Matrix)
{
  glm::mat4 mat4Glm;


  mat4Glm[0][0] = mat4Matrix.a1; mat4Glm[0][1] = mat4Matrix.b1;  mat4Glm[0][2] = mat4Matrix.c1; mat4Glm[0][3] = mat4Matrix.d1;
  mat4Glm[1][0] = mat4Matrix.a2; mat4Glm[1][1] = mat4Matrix.b2;  mat4Glm[1][2] = mat4Matrix.c2; mat4Glm[1][3] = mat4Matrix.d2;
  mat4Glm[2][0] = mat4Matrix.a3; mat4Glm[2][1] = mat4Matrix.b3;  mat4Glm[2][2] = mat4Matrix.c3; mat4Glm[2][3] = mat4Matrix.d3;
  mat4Glm[3][0] = mat4Matrix.a4; mat4Glm[3][1] = mat4Matrix.b4;  mat4Glm[3][2] = mat4Matrix.c4; mat4Glm[3][3] = mat4Matrix.d4;

  return mat4Glm;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

std::vector<MageModel*> MageModelFactory::LoadModel(std::string sPath)
{
  // [Load the File].
  // Change all Primitives to Triangles.
  // Set a limit of 4 Bones Weights per Vertex.
  Assimp::Importer oModelLoader;
  const aiScene* pScene = oModelLoader.ReadFile(sPath,
    aiProcess_Triangulate |
    aiProcess_LimitBoneWeights);

  // Store all Meshes included in the File.
  std::vector<MageModel*> vecModels;


  // [Check whether the File could be loaded].
  if (!pScene)
  {
    mio::err << "Could not load the Scene" << mio::endl;
    return vecModels;
  }


  // [Extract Materials].
  std::map<unsigned int, MageTexture*> mapMaterials;
  GetMaterials(sPath, pScene, mapMaterials);


  // [Extract Mesh Information].
  for (unsigned int iMesh = 0; iMesh < pScene->mNumMeshes; ++iMesh)
  {
    // Initialize Mesh.
    aiMesh* pAiMesh = pScene->mMeshes[iMesh];

    // [Get the Mesh Information]
    std::vector<glm::vec3> vecVertices;
    std::vector<glm::vec3> vecNormals;
    std::vector<glm::vec3> vecColors;
    std::vector<glm::vec2> vecUVs;
    std::vector<unsigned int> vecFaceIndices;
    std::vector<glm::vec3> vecTangents;
    std::vector<glm::vec3> vecBiTangents;
 
    GetMeshInformation(pAiMesh, vecVertices, vecNormals, vecColors, vecUVs, vecFaceIndices, vecTangents, vecBiTangents);


    // [Get Texture Information].
    std::vector<MageTexture*> vecTextures;

    // Store the Texture of the Mesh, if the Mesh has a Texture.
    if (pAiMesh->HasTextureCoords(0))
      if (mapMaterials.find(pAiMesh->mMaterialIndex) != mapMaterials.end())
        vecTextures.push_back(mapMaterials[pAiMesh->mMaterialIndex]);

    
    // [Get Bone Information].
    std::vector<MageJoint*> vecJoints;
    std::map<unsigned int, std::map<unsigned int, float>> mapVertexWeights;
    std::vector<glm::ivec4> vecJointIndices;
    std::vector<glm::vec4> vecJointWeights;

    // Get the Joints and their Weights from the Skeleton.
    GetSkeletonInformation(pAiMesh, vecJoints, mapVertexWeights);
    
    // Put the Bones into the Hierarchy.
    MageJoint* pRoot = GetSkeletonHierarchy(pScene->mRootNode, vecJoints);
    pRoot->CalculateInverseBindTransform(glm::mat4(1.0f));
  
    // Get the Joint Indices and Joint Weights for each Vertex.
    GetJointIndicesAndWeights(mapVertexWeights, vecJointIndices, vecJointWeights);
    

    // [Get Animations].
    std::vector<MageAnimation> vecAnimations;
    GetAnimations(pScene, vecAnimations);


    // [Model Creation].
    // Create and store a new Mesh based on the gathered Data.
    MageMesh* pMesh = new MageMesh(vecVertices, vecNormals, vecColors, vecUVs, vecFaceIndices, vecTangents, vecBiTangents, vecJointIndices, vecJointWeights);
    MageVAO*  pVAO  = new MageVAO(pMesh);

    // Create a Model based on the gathered Data.
    MageModel* pModel;
    
    // Colorized Model.
    if (vecTextures.empty() && !vecColors.empty())
      pModel = new MageModel(pVAO);
    // Textured Model.
    else if (!pRoot || vecAnimations.empty())
      pModel = new MageTexturedModel(pVAO, vecTextures);
    // Animated Model.
    else if (pRoot && !vecAnimations.empty())
      pModel = new MageAnimatedModel(pVAO, vecTextures, pRoot, vecAnimations);

    vecModels.push_back(pModel);
  }

  return vecModels;
}


void MageModelFactory::GetAnimations(const aiScene* pScene, std::vector<MageAnimation>& vecAnimations)
{
  // Get all Animations.
  for (unsigned int iAnimation = 0; iAnimation < pScene->mNumAnimations; ++iAnimation)
  {
    aiAnimation* pAnimation = pScene->mAnimations[iAnimation];

    // Store the Keyframes in a Map.
    std::map<double, std::map<std::string, MageTransformation*>> mapKeyframes;

    // Get the Animation for each Joint.
    for (unsigned int iNodeAnimation = 0; iNodeAnimation < pAnimation->mNumChannels; ++iNodeAnimation)
    {
      aiNodeAnim* pNodeAnimation = pAnimation->mChannels[iNodeAnimation];

      // Get all Transformations at every Timestamp for the current Joint (Animation Node).
      std::map<double, MageTransformation*> mapJointTransformations = GetKeyframes(pAnimation->mChannels[iNodeAnimation]);

      // Store the Transformation and Timestamp Information in the 'mapKeyframes' Map.
      // Basically the Loop transforms 'std::map<std::string, std::map<double, TransformationPtr>>' into 'std::map<double, std::map<std::string, TransformationPtr>>'.
      for (std::map<double, MageTransformation*>::iterator it = mapJointTransformations.begin(); it != mapJointTransformations.end(); ++it)
      {
        mapKeyframes[it->first][pNodeAnimation->mNodeName.data] = it->second;
      }
    }

    // Create the Keyframes.
    std::vector<MageKeyframe> vecKeyframes;

    for (std::map<double, std::map<std::string, MageTransformation*>>::iterator it = mapKeyframes.begin(); it != mapKeyframes.end(); ++it)
    {
      vecKeyframes.push_back(MageKeyframe(it->first, it->second));
    }

    // Create the Animation.
    vecAnimations.push_back(MageAnimation(pAnimation->mName.data, vecKeyframes));
  }
}


void MageModelFactory::GetJointIndicesAndWeights(std::map<unsigned int, std::map<unsigned int, float>>& mapVertexWeights, std::vector<glm::ivec4>& vecIndices, std::vector<glm::vec4>& vecWeights)
{
  // Nothing to do if there are not Indices and Weights.
  if (mapVertexWeights.empty())
    return;

  // Put the Contents of the 'mapVertexWeights' Map into the 'vecIndices' and 'vecWeights' Vectors.
  typedef std::map<unsigned int, std::map<unsigned int, float>>::iterator it_VertexWeights;
  typedef std::map<unsigned int, float>::iterator it_Joints;

  // Loop over Vertex Indices.
  for (it_VertexWeights itVertex = mapVertexWeights.begin(); itVertex != mapVertexWeights.end(); ++itVertex)
  {
    // Store Indices and Weights of the current Vertex in Vectors.
    glm::ivec4 iv4JointIndices = glm::ivec4(0, 0, 0, 0);
    glm::vec4  v4JointWeights = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

    // Track the Position inside the Vectors.
    unsigned int iPos = 0;

    // Loop over Joint Indices.
    for (it_Joints itJoint = itVertex->second.begin(); itJoint != itVertex->second.end(); ++itJoint)
    {
      // There should only be 4 Weights as it is defined this Way using Assimp Library.
      if (iPos > 3)
      {
        mio::err << "There are too many Weights for the Model. This could cause broken Animations." << mio::endl;
        break;
      }

      // Store the Index and Weight Information.
      iv4JointIndices[iPos] = itJoint->first;
      v4JointWeights[iPos] = itJoint->second;

      // Update the Position Counter.
      iPos++;
    }

    // Store Index and Weight Information in the Output Vectors.
    vecIndices.push_back(iv4JointIndices);
    vecWeights.push_back(v4JointWeights);
  }
}


std::map<double, MageTransformation*> MageModelFactory::GetKeyframes(aiNodeAnim* pNodeAnimation)
{
  // Load Position Changes.
  std::map<double, glm::vec3> mapTranslations;

  for (unsigned int iKeyframe = 0; iKeyframe < pNodeAnimation->mNumPositionKeys; ++iKeyframe)
  {
    // Store the Position Data of the current Joint (Animation Node) with its Timestamp in the Map.
    mapTranslations[pNodeAnimation->mPositionKeys[iKeyframe].mTime] = glm_cast(pNodeAnimation->mPositionKeys[iKeyframe].mValue);
  }

  // Load Rotation Changes.
  std::map<double, glm::quat> mapRotations;

  for (unsigned int iKeyframe = 0; iKeyframe < pNodeAnimation->mNumRotationKeys; ++iKeyframe)
  {
    // Store the Rotation Data of the current Joint (Animation Node) with its Timestamp in the Map.
    mapRotations[pNodeAnimation->mRotationKeys[iKeyframe].mTime] = glm_cast(pNodeAnimation->mRotationKeys[iKeyframe].mValue);
  }

  // Load Position Changes.
  std::map<double, glm::vec3> mapScalings;

  for (unsigned int iKeyframe = 0; iKeyframe < pNodeAnimation->mNumScalingKeys; ++iKeyframe)
  {
    // Store the Scaling Data of the current Joint (Animation Node) with its Timestamp in the Map.
    mapScalings[pNodeAnimation->mScalingKeys[iKeyframe].mTime] = glm_cast(pNodeAnimation->mScalingKeys[iKeyframe].mValue);
  }

  // Get all Timestamps. Some Keyframes might only affect for example the Scaling and not the Translation and Rotation.
  // Thus, the Keys of the three Maps 'mapTranslations', 'mapRotations' and 'mapScalings' might not be the same.
  std::vector<double> vecTimestamps;

  for (std::map<double, glm::vec3>::iterator it = mapTranslations.begin(); it != mapTranslations.end(); ++it)
  {
    // Add the Timestamp if it has not already been added.
    if (std::find(vecTimestamps.begin(), vecTimestamps.end(), it->first) == vecTimestamps.end())
      vecTimestamps.push_back(it->first);
  }

  for (std::map<double, glm::quat>::iterator it = mapRotations.begin(); it != mapRotations.end(); ++it)
  {
    // Add the Timestamp if it has not already been added.
    if (std::find(vecTimestamps.begin(), vecTimestamps.end(), it->first) == vecTimestamps.end())
      vecTimestamps.push_back(it->first);
  }

  for (std::map<double, glm::vec3>::iterator it = mapScalings.begin(); it != mapScalings.end(); ++it)
  {
    // Add the Timestamp if it has not already been added.
    if (std::find(vecTimestamps.begin(), vecTimestamps.end(), it->first) == vecTimestamps.end())
      vecTimestamps.push_back(it->first);
  }

  // Make sure the Timestamps are sorted.
  std::sort(vecTimestamps.begin(), vecTimestamps.end(), sortDoubles());

  // Combine Translation, Rotation and Scaling at each Timestamp in a Transformation.
  std::map<double, MageTransformation*> mapTransformation;

  glm::vec3 v3LastTranslation = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::quat qLastRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
  glm::vec3 v3LastScaling = glm::vec3(1.0f, 1.0f, 1.0f);

  for (double dTimestamp : vecTimestamps)
  {
    glm::vec3 v3Translation;
    glm::quat qRotation;
    glm::vec3 v3Scaling;

    // Check whether there is Translation Data for the current Timestamp.
    if (mapTranslations.find(dTimestamp) != mapTranslations.end())
      v3Translation = mapTranslations[dTimestamp];
    // If there is no Translation Data use the latest Translation Data.
    else
      v3Translation = v3LastTranslation;

    // Check whether there is Rotation Data for the current Timestamp.
    if (mapRotations.find(dTimestamp) != mapRotations.end())
      qRotation = mapRotations[dTimestamp];
    // If there is no Rotation Data use the latest Translation Data.
    else
      qRotation = qLastRotation;

    // Check whether there is Scaling Data for the current Timestamp.
    if (mapScalings.find(dTimestamp) != mapScalings.end())
      v3Scaling = mapScalings[dTimestamp];
    // If there is no Scaling Data use the latest Translation Data.
    else
      v3Scaling = v3LastScaling;

    // Combine Translation, Rotation and Scaling in one Transformation.
    mapTransformation[dTimestamp] = new MageTransformation(v3Translation, qRotation, v3Scaling);

    // Update 'Last Variables'.
    v3LastTranslation = v3Translation;
    qLastRotation = qRotation;
    v3LastScaling = v3Scaling;
  }

  return mapTransformation;
}


void MageModelFactory::GetMaterials(std::string sPath, const aiScene* pScene, std::map<unsigned int, MageTexture*>& mapMaterials)
{
  for (unsigned int iMaterial = 0; iMaterial < pScene->mNumMaterials; ++iMaterial)
  {
    const aiMaterial* pMaterial = pScene->mMaterials[iMaterial];

    aiString aisPath;
    int iIndex = 0;

    // Get the Path to the Texture.
    if (pMaterial->GetTexture(aiTextureType_DIFFUSE, iIndex, &aisPath) == AI_SUCCESS)
    {
      // Convert the Path to a String.
      std::string sTexture = aisPath.data;

      // Get File Path.
      std::string sFilePath = "";
      std::string::size_type iLastSlash = sPath.rfind('/');

      if (iLastSlash != std::string::npos)
        sFilePath = sPath.substr(0, iLastSlash + 1);
      else
      {
        iLastSlash = sPath.rfind('\\');

        if (iLastSlash != std::string::npos)
          sFilePath = sPath.substr(0, iLastSlash + 1);
      }

      // Get File Extension.
      std::string sFileExtension = "";
      std::string::size_type iExtPos = sTexture.rfind('.');

      if (iExtPos != std::string::npos)
        sFileExtension = sTexture.substr(iExtPos + 1);

      std::transform(sFileExtension.begin(), sFileExtension.end(), sFileExtension.begin(), ::toupper);

      // Load and create the Texture.
      GLuint iTextureID;

      // BMP, PNG, JPG/JPEG, TGA, PSD, HDR.
      if (sFileExtension == "BMP" || sFileExtension == "PNG" || sFileExtension == "JPG" || sFileExtension == "JPEG"
        || sFileExtension == "TGA" || sFileExtension == "PSD" || sFileExtension == "HDR")
        iTextureID = SOIL_load_OGL_texture
        (
          sFilePath.append(sTexture).c_str(),
          SOIL_LOAD_AUTO,
          SOIL_CREATE_NEW_ID,
          SOIL_FLAG_MIPMAPS | SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_INVERT_Y
        );

      // DDS.
      else if (sFileExtension == "DDS")
        iTextureID = SOIL_load_OGL_texture
        (
          sFilePath.append(sTexture).c_str(),
          SOIL_LOAD_AUTO,
          SOIL_CREATE_NEW_ID,
          SOIL_FLAG_DDS_LOAD_DIRECT
        );

      // Store the Texture with its Index in the Map.
      mapMaterials[iMaterial] = new MageTexture(iTextureID);
    }
  }
}


void MageModelFactory::GetMeshInformation(aiMesh* pAiMesh,
  std::vector<glm::vec3>& vecVertices,
  std::vector<glm::vec3>& vecNormals,
  std::vector<glm::vec3>& vecColors,
  std::vector<glm::vec2>& vecUVs,
  std::vector<unsigned int>& vecFaceIndices,
  std::vector<glm::vec3>& vecTangents,
  std::vector<glm::vec3>& vecBiTangents)
{
  // Reserve enough Space for the Vectors.    
  vecVertices.reserve(pAiMesh->mNumVertices);
  vecNormals.reserve(pAiMesh->mNumVertices);
  vecColors.reserve(pAiMesh->mNumVertices);
  vecUVs.reserve(pAiMesh->mNumVertices);
  vecTangents.reserve(pAiMesh->mNumVertices);
  vecBiTangents.reserve(pAiMesh->mNumVertices);
  vecFaceIndices.reserve(3 * pAiMesh->mNumFaces);

  // Get the Vertices, Normals, Colors, UVs, Tangents and Bi-Tangents.
  for (unsigned int uiIndex = 0; uiIndex < pAiMesh->mNumVertices; ++uiIndex)
  {
    // Vertices.
    if (pAiMesh->HasPositions())
      vecVertices.push_back(glm_cast(pAiMesh->mVertices[uiIndex]));

    // Normals.
    if (pAiMesh->HasNormals())
      vecNormals.push_back(glm_cast(pAiMesh->mNormals[uiIndex]));

    // UV Coordinates.
    if (pAiMesh->HasTextureCoords(0)) // Assimp supports 8 Sets of UV Coordinates. We use one one (0).
      vecUVs.push_back(glm_vec3_to_vec2_cast(pAiMesh->mTextureCoords[0][uiIndex]));
    // Colors.
    else if (pAiMesh->HasVertexColors(0)) // Assimp supports 8 Sets of Colors. We use one one (0).
      vecColors.push_back(glm_cast(pAiMesh->mColors[0][uiIndex]));
    else
      vecColors.push_back(glm::vec3(0.0f, 1.0f, 0.0f));

    // Tangents and Bi-Tangents.
    if (pAiMesh->HasTangentsAndBitangents())
    {
      vecTangents.push_back(glm_cast(pAiMesh->mTangents[uiIndex]));
      vecBiTangents.push_back(glm_cast(pAiMesh->mBitangents[uiIndex]));
    }
  }

  // Get Face Indices.
  for (unsigned int uiFaceIndex = 0; uiFaceIndex < pAiMesh->mNumFaces; ++uiFaceIndex)
  {
    vecFaceIndices.push_back(pAiMesh->mFaces[uiFaceIndex].mIndices[0]);
    vecFaceIndices.push_back(pAiMesh->mFaces[uiFaceIndex].mIndices[1]);
    vecFaceIndices.push_back(pAiMesh->mFaces[uiFaceIndex].mIndices[2]);
  }
}


void MageModelFactory::GetSkeleton(aiNode* pNode, MageJoint* pParentJoint, std::vector<MageJoint*>& vecJoints)
{
  // Check whether the current Node 'pNode' is a Bone Node.
  for (MageJoint* pJoint : vecJoints)
  {
    if (pJoint->GetName() == pNode->mName.data)
    {
      // A Child has been found.
      if (pParentJoint) // Not called if the current Node is the Bone Root.
        pParentJoint->AddChild(pJoint);

      // Remove the Joint from the Vector as it can not be another Element of the Skeleton. This improves Performance a bit.
      std::vector<MageJoint*>::iterator it_Pos = std::find(vecJoints.begin(), vecJoints.end(), pJoint);
      vecJoints.erase(it_Pos);

      // Find further Joint in the Children of the current Node 'pNode'.
      for (unsigned int iChild = 0; iChild < pNode->mNumChildren; ++iChild)
      {
        GetSkeleton(pNode->mChildren[iChild], pJoint, vecJoints);
      }

      // The aiNode has already been matched with a Joint. There can at most be one match, thus we can stop here.
      return;
    }
  }
}


MageJoint* MageModelFactory::GetSkeletonHierarchy(aiNode* pNode, std::vector<MageJoint*>& vecJoints)
{
  // Stores the Root Joint Node.
  aiNode* pRoot = GetSkeletonRoot(pNode, vecJoints);
  MageJoint* pRootJoint;

  for (MageJoint* pJoint : vecJoints)
  {
    // The Root Joint Node has been found.
    if (pJoint->GetName() == pRoot->mName.data)
      pRootJoint = pJoint;
  }

  // Error Checking
  if (pRoot == nullptr)
  {
    mio::err << "No Joint Root has been found." << mio::endl;
    return nullptr;
  }

  // Find Children and complete construction of Skeleton.
  GetSkeleton(pRoot, nullptr, vecJoints);

  return pRootJoint;
}


void MageModelFactory::GetSkeletonInformation(aiMesh* pAiMesh, std::vector<MageJoint*>& vecJoints, std::map<unsigned int, std::map<unsigned int, float>>& mapVertexWeights)
{
  // Get all Bones of the Mesh.
  for (unsigned int iJoint = 0; iJoint < pAiMesh->mNumBones; ++iJoint)
  {
    aiBone* pBone = pAiMesh->mBones[iJoint];
    vecJoints.push_back(new MageJoint(iJoint, pBone->mName.data, glm_cast(pBone->mOffsetMatrix)));

    // Extract Vertex Weights.
    std::map<unsigned int, float> mapBoneWeights;

    for (unsigned int iVertexWeight = 0; iVertexWeight < pBone->mNumWeights; ++iVertexWeight)
    {
      aiVertexWeight sVertexWeight = pBone->mWeights[iVertexWeight];
      mapBoneWeights[sVertexWeight.mVertexId] = sVertexWeight.mWeight;
    }

    // The Loop transforms 'std::map<unsigned int (joint index), std::map<unsigned int (vertex index), float (weight)>>'
    // into 'std::map<unsigned int (vertex index), std::map<unsigned int (joint index), float (weight)>>'.
    for (std::map<unsigned int, float>::iterator it = mapBoneWeights.begin(); it != mapBoneWeights.end(); ++it)
    {
      mapVertexWeights[it->first][iJoint] = it->second;
    }
  }
}


aiNode* MageModelFactory::GetSkeletonRoot(aiNode* pNode, std::vector<MageJoint*>& vecJoints)
{
  // Check whether the current Node 'pNode' is a Bone Node.
  for (MageJoint* pJoint : vecJoints)
  {
    // The Root Joint Node has been found.
    if (pJoint->GetName() == pNode->mName.data)
      return pNode;
  }

  // Find the Root Joint in the Children of the current Node 'pNode'.
  for (unsigned int iChild = 0; iChild < pNode->mNumChildren; ++iChild)
  {
    aiNode* pRoot = GetSkeletonRoot(pNode->mChildren[iChild], vecJoints);

    // The Root Joint Node has been found.
    if (pRoot != nullptr)
      return pRoot;
  }

  return nullptr;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/