/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageModel.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageModel::MageModel(MageVAO* pVAO)
  : m_pVAO(pVAO)
{
}


MageModel::~MageModel()
{
  delete m_pVAO;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

bool MageModel::Draw()
{
  // Draw the Model.
  if (m_pVAO)
    return m_pVAO->Draw();

  return false;
}


ModelType MageModel::GetType()
{
  return ModelType::Colorized;
}


bool MageModel::Upload(GLenum eUsage)
{
  // Upload the VAO/VBOs.
  if (m_pVAO)
    return m_pVAO->Upload(eUsage);

  return false;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/