set( RelativeDir "source/Shading" )
set( RelativeSourceGroup "SourceFiles\\Shading" )

set(Directories
  Shader
)

set(DirFiles
  MageCameraShader.cpp
  MageCameraShader.h
  MageShader.cpp
  MageShader.h
  MageShaderManager.cpp
  MageShaderManager.h
  _SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")