set( RelativeDir "source/CollisionDetection" )
set( RelativeSourceGroup "SourceFiles\\CollisionDetection" )

set(Directories
  
)

set(DirFiles
  _SourceFiles.cmake
  MageBoundingBox.cpp
  MageBoundingBox.h
  MageBoundingSphere.cpp
  MageBoundingSphere.h
  MageBoundingVolume.cpp
  MageBoundingVolume.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${RelativeSourceGroup} FILES ${LocalSourceGroupFiles} )

foreach( subdir ${Directories})
	include("${RelativeDir}/${subdir}/_SourceFiles.cmake")
endforeach()

set( RelativeDir "source")