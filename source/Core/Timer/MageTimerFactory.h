#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageTimer.h"

#include "../MageSingleton.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class MageTimerFactory : public MageSingleton<MageTimerFactory> // THIS SHOULD BE A SNGLETON AND FUNCTION SHOULD BE STATIC
{
  friend class MageSingleton<MageTimerFactory>;

public:
  /*
  * Destructor.
  */
  ~MageTimerFactory();


  /*
  * Creates a new Timer and puts it into the System, which updates it.
  * @return The Super-Type of the Timer, which does not have any Update Function.
  */
  static MageTimer* CreateTimer();


protected:
  /*
  * Constructor.
  */
  MageTimerFactory();

};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/