/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSgTransformNode.h"

#include "gtc/matrix_transform.hpp"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSgTransformNode::MageSgTransformNode()
{
}


MageSgTransformNode::~MageSgTransformNode()
{
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

glm::quat MageSgTransformNode::GetRotation()
{
  return glm::quat_cast(m_mat4Transformation);
}


glm::mat4 MageSgTransformNode::GetTransformation()
{
  return m_mat4Transformation;
}


glm::vec3 MageSgTransformNode::GetTranslation()
{
  return glm::vec3(m_mat4Transformation[3].x, m_mat4Transformation[3].y, m_mat4Transformation[3].z);
}


NodeType MageSgTransformNode::GetType()
{
  return NodeType::Transform;
}


void MageSgTransformNode::Rotate(float fAngle, glm::vec3 v3Axis)
{
  m_mat4Transformation = glm::rotate(m_mat4Transformation, fAngle, v3Axis);
}


void MageSgTransformNode::Rotate(glm::quat qRotation)
{
  Transform(glm::toMat4(qRotation));
}


void MageSgTransformNode::Scale(float fScaling)
{
  m_mat4Transformation[0] *= fScaling;
  m_mat4Transformation[1] *= fScaling;
  m_mat4Transformation[2] *= fScaling;
}


void MageSgTransformNode::Scale(glm::vec3 v3Scaling)
{
  m_mat4Transformation[0] *= v3Scaling[0];
  m_mat4Transformation[1] *= v3Scaling[1];
  m_mat4Transformation[2] *= v3Scaling[2];
}


void MageSgTransformNode::SetTransformation(glm::mat4 mat4Transformation)
{
  m_mat4Transformation = mat4Transformation;
}


void MageSgTransformNode::SetTranslation(glm::vec3 v3Translation)
{
  m_mat4Transformation[3] = glm::vec4(v3Translation, m_mat4Transformation[3][3]);
}


void MageSgTransformNode::Translate(glm::vec3 v3Translation)
{
  m_mat4Transformation[3] += glm::vec4(v3Translation, 0.0f);
}


void MageSgTransformNode::Transform(glm::mat4 mat4Transformation)
{
  m_mat4Transformation = mat4Transformation * m_mat4Transformation;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/