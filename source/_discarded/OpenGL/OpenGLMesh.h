#pragma once

/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "../../SceneRepresentation/MageMesh.h"

#include "GL/glew.h"

/******************************************************************************/
/*                                   DEFINE                                   */
/******************************************************************************/


/******************************************************************************/
/*                            Forward Deceleration                            */
/******************************************************************************/


/******************************************************************************/
/*                              CLASS DEFINITION                              */
/******************************************************************************/
class OpenGlMesh : public MageMesh
{
public:
  /*
  * Constructor.
  */
  OpenGlMesh(std::string sPath);


  /*
  * Destructor.
  */
  virtual ~OpenGlMesh();


  /*
  * Draws the Mesh.
  */
  void Draw() override;


  /*
  * Remove the Mesh from the Graphics Card.
  * @return true if the Mesh has been removed from the Graphics Card.
  */
  bool FreeVRam() override;


  /*
  * Uploads the Mesh to the Graphics Card.
  * @return true if the Mesh has been uploaded to the Graphics Card.
  */
  bool Upload() override;


protected:
  GLuint m_iVAO = 0; // VAO
  GLuint m_iVertexBuffer = 0; // VBO
  GLuint m_iNormalBuffer = 0; // VBO
  GLuint m_iUVBuffer = 0; // VBO
  GLuint m_iTangentBuffer = 0; // VBO
  GLuint m_iBiTangentBuffer = 0; // VBO
  GLuint m_iEABuffer = 0; // Element Array Buffer

  GLsizei m_iSize = 0;
};

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/