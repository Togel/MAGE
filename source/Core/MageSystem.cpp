/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageSystem.h"

#include "../IO/mio.h"

/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageSystem::MageSystem()
  : m_pDrawThread(new MageThreadDraw())
  , m_pGeoThread(new MageThreadGeometry())
{
}


MageSystem::~MageSystem()
{
  m_bRun = false;

  delete m_pGeoThread;
  delete m_pDrawThread;
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

MageThreadDraw* MageSystem::GetDrawThread()
{
  return m_pDrawThread;
}


MageThreadGeometry* MageSystem::GetGeometryThread()
{
  return m_pGeoThread;
}


void MageSystem::Run()
{
  // Allow the Threads to roll.
  m_bRun = true;

  // Create a Function for the Geometry Thread which is executed by an std::thread.
  auto RunGeometryThread = [this]()
  {
    while (m_bRun)
    {
      m_pGeoThread->_UpdateTimer();
      m_pGeoThread->_Run();
    }
  };


  // Kick-off Threads.
  m_vecThreads.emplace_back(std::thread(RunGeometryThread));


  // Kick-off Draw-Thread.
  while (m_bRun)
  {
    // Draw Context.
    m_pDrawThread->_UpdateTimer();
    m_pDrawThread->_Run();
  }


  // System has been terminated. Synchronize Threads and clean-up.
  for (unsigned int iPos =  0; iPos<m_vecThreads.size(); ++iPos)
  {
    m_vecThreads.at(iPos).join();
  }
  m_vecThreads.clear();
}


void MageSystem::SwitchScene(MageScene* pScene)
{
  m_pDrawThread->SwitchScene(pScene);
  m_pGeoThread->SwitchScene(pScene);
}


void MageSystem::Terminate()
{
  m_bRun = false;
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/