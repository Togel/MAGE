/******************************************************************************/
/*                                  INCLUDES                                  */
/******************************************************************************/
#include "MageVBO.h"


/******************************************************************************/
/*                          CONSTRUCTOR / DESTRUCTOR                          */
/******************************************************************************/

MageVBO::MageVBO(GLenum eType)
  : m_eType(eType)
{
  glGenBuffers(1, &m_iBufferID);
}


MageVBO::~MageVBO()
{
  glDeleteBuffers(1, &m_iBufferID);
}

/******************************************************************************/
/*                               IMPLEMENTATION                               */
/******************************************************************************/

GLuint MageVBO::GetBufferID()
{
  return m_iBufferID;
}


void MageVBO::Upload(GLenum eUsage, std::vector<float>& vecData)
{
  if (vecData.size() > 0)
  {
    glBindBuffer(m_eType, m_iBufferID);
    glBufferData(m_eType, sizeof(float)*vecData.size(), &vecData[0], eUsage);
    glBindBuffer(m_eType, 0);
  }
}


void MageVBO::Upload(GLenum eUsage, std::vector<glm::vec2>& vecData)
{
  if (vecData.size() > 0)
  {
    glBindBuffer(m_eType, m_iBufferID);
    glBufferData(m_eType, sizeof(glm::vec2)*vecData.size(), &vecData[0], eUsage);
    glBindBuffer(m_eType, 0);
  }
}


void MageVBO::Upload(GLenum eUsage, std::vector<glm::vec3>& vecData)
{
  if (vecData.size() > 0)
  {
    glBindBuffer(m_eType, m_iBufferID);
    glBufferData(m_eType, sizeof(glm::vec3)*vecData.size(), &vecData[0], eUsage);
    glBindBuffer(m_eType, 0);
  }
}


void MageVBO::Upload(GLenum eUsage, std::vector<unsigned int>& vecData)
{
  if (vecData.size() > 0)
  {
    glBindBuffer(m_eType, m_iBufferID);
    glBufferData(m_eType, sizeof(unsigned int)*vecData.size(), &vecData[0], eUsage);

    // The Face Index Buffer must not be released.
    if(m_eType != GL_ELEMENT_ARRAY_BUFFER)
      glBindBuffer(m_eType, 0);
  }
}


void MageVBO::Upload(GLenum eUsage, std::vector<glm::vec4>& vecData)
{
  if (vecData.size() > 0)
  {
    glBindBuffer(m_eType, m_iBufferID);
    glBufferData(m_eType, sizeof(glm::vec4)*vecData.size(), &vecData[0], eUsage);
    glBindBuffer(m_eType, 0);
  }
}


void MageVBO::Upload(GLenum eUsage, std::vector<glm::ivec4>& vecData)
{
  if (vecData.size() > 0)
  {
    glBindBuffer(m_eType, m_iBufferID);
    glBufferData(m_eType, sizeof(glm::ivec4)*vecData.size(), &vecData[0], eUsage);
    glBindBuffer(m_eType, 0);
  }
}

/******************************************************************************/
/*                                END OF FILE                                 */
/******************************************************************************/